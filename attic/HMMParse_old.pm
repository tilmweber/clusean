#
#  CLUSEAN Module to parse HMMer output files
#
#  Author: Tilmann Weber

=head1 NAME

CLUSEAN::HMMParse - Analysed HMMPfam and HMMSearch outputfiles

=head1 SYNOPSIS


$HMMRes=CLUSEAN::HMMPfam->new ('-file' => "/home/weber/abdb/hmmmod/t/test2.hmm");

@Results=$HMMRes->getResultSet;

=head1 DESCRIPTION

This modules opens and analyzed HMMPfam or HMMSearch outputfiles and
parses the included information and returns an Array of Hashes including the
following information:



     $aHMMResultSet = { 'HMMName'      => Name of Pfam-Domain Hit,
                        'Domain_start' => aa pos of domain start,
                        'Domain_end'   => aa pos of domain end,
                        'Evalue'       => E-value of domain hit,
                        'Score'        => Score of domain hit,
                        'HMMAcc'       => AccNo of HMM-Hit,
                        'GeneID'       => GeneID ($sGeneID),
			'QuerySeq'     => ID of query sequence,
		        'HMMDesc'      => PlainText description of PfamHit}

     $HMMRes->getResultSet returns an Array of $aHMMResultSet arrays

=head1 INCLUSIONS

HMMParse includes following 3rd Party modules:

Bio::Tools:HMMER::Results


=head1 EXAMPLES

    use HMMParse;

    my $HMMRes=CLUSEAN::HMMParse->new ('-file' => "HMMPfamOut.hmm",
			      '-type' => "hmmpfam");
    my @Results=$HMMRes->getResultSet;


    foreach my $hHMMHit (@Results) {
       print $hHMMHit->{"GeneID"},"\t",$hHMMHit->{"HMMName"},"\t";
       print $hHMMHit->{"Domain_start"},"\t\t",$hHMMHit->{"Domain_end"},"\t\t";
       print $hHMMHit->{"Score"},"\t",$hHMMHit->{"Evalue"},"\n";
}


=cut

package CLUSEAN::HMMParse;

=head1 Methods

=cut

use strict;
use base ("Bio::Tools::HMMER::Results");        # HMMPFAM Parser

=head2 new

 Title      :  new

 Usage      :  my $HMMRes=CLUSEAN::HMMParse->new ('-file'    =>  'HMMSearchOut.hmm',
					 '-type'    =>  'hmmpfam')

               where -type is either hmmpfam or hmmsearch, depending on the
               results-file

 Function   :  Reads and parses a HMMPfam or HMMSearch outputfile

 Returns    :  A Bio::Tools::HMMER::Results object


=cut

my $sCLUSEANROOT = $ENV{CLUSEANROOT} || "/usr/local/CLUSEAN";
# my $sCLUSEANROOT = "/home/weber/abdb";

sub new {



# Set up Method
    my $invocant  = shift;
    my $class     = ref($invocant)  ||  $invocant;


# Get parameters
    my $aParams   =  {
	"-type"     =>  "hmmpfam",
	"-file"     =>  undef,
	@_ };


    my $self = Bio::Tools::HMMER::Results->new (
					     '-file'   => $aParams->{"-file"},
					     '-type'   => $aParams->{"-type"});




    bless ($self, $class);
    return $self;
}






=head2 getResultSet

 Title      :  getResultSet

 Usage      :  @aResults=$oHMMParseObject->getResultSet ('-evalue'  => 2,
							 '-gene_id' => test.01
                                                         '-score'   => 1);

 Function   :  Parses the HMMer results and Returns HMM-Hits as @aResults if
               the hit's Evalue is < '-evalue' and the score is > '-score'
               Defaults:  E-value < 2, Score > 1

 Returns    :  Array of $aHMMResultSet
               where $aHMMResultSet is defined as
               $aHMMResultSet = { 'HMMName'      => Name of Pfam-Domain Hit
				  'Domain_start' => aa pos of domain start
				  'Domain_end'   => aa pos of domain end
                                  'Evalue'       => E-value of domain hit
                                  'Score'        => Score of domain hit
                                  'HMMAcc'       => AccNo of HMM-Hit
                                  'GeneID'       => GeneID
				  'QuerySeq'     => ID of query sequence
                                  'HMMDesc'      => PlainText description of PfamHit}

=cut

sub getResultSet {

    my $oSeqSet;
    my $oDomain;
    my $aHMMResultSet;
    my @aResults;
    my %hDescTable;

# Set up Method
    my $invocant  = shift;
    my $class     = ref($invocant)  ||  $invocant;

# Get parameters
    my $aParams   =  {
	"-evalue"   =>  10,
	"-score"    =>  1,
	"-gene_id"  =>  'noID',
	"-descfile" =>  "$sCLUSEANROOT/tables/pfamdesc.tab",
	@_ };


# Parse Acc-Desc ass-file

    open DescFile,  "$aParams->{'-descfile'}";

    while (my $sLine = <DescFile>) {

        $sLine =~ /\w*:([\w\d-]*)[\t\s]*(.*$)/;
	$hDescTable{$1}=$2;
    }
    close (DescFile);

    foreach $oSeqSet ( $invocant->each_Set) {
	foreach $oDomain ($oSeqSet->each_Domain ) {
	   next if ($oDomain->evalue > $aParams->{'-evalue'});
	   next if ($oDomain->bits < $aParams->{'-score'});
           $aHMMResultSet = { 'HMMName'         =>  $oDomain->hmmname,
			      'Domain_start'    =>  $oDomain->start,
			      'Domain_end'      =>  $oDomain->end,
			      'Evalue'          =>  $oDomain->evalue,
			      'Score'           =>  $oDomain->bits,
			      'GeneID'          =>  $aParams->{'-gene_id'},
			      'QuerySeq'        =>  $oSeqSet->name,
			      'HMMDesc'         =>  $hDescTable{$oDomain->hmmname}};
	   if ($oDomain->has_tag("hmmacc")) {
	       $aHMMResultSet->{"HMMAcc"} =  $oDomain->hmmacc;
	   }
	   push @aResults, $aHMMResultSet;
       }
    }
    return @aResults;
}


1;
