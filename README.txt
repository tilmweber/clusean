README

CLUSEAN CLuster SEquence ANalyzer

Welcome to CLUSEAN

CLUSEAN is a collection of programs to analyze secondary metabolite biosynthesis genes

For hardware requirements, software installation and program documentations see

./doc/CLUSEAN.doc (MS Word) or ./doc/CLUSEAN.pdf