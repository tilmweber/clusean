#!/usr/bin/perl
#
#  $Id: assignFunction.pl,v 1.3 2008/02/07 16:21:06 weber Exp $
#
#

=head1 Name

assignFunction.pl

=head1 SYNOPTSIS

assignFunction.pl --input <EMBL-File> [--output <Filename> --onlyblast --onlyhmmer
--verbose --help]

=head1 DESCRIPTION

assignFunction.pm analyzes BLASTP and HMMer references included in (user generated)
EMBL-Files. Using the AssignFunction module based on these analyses a putative
function of every orf is predicted.
This module is still in experimental stage, so all assignments should be handled with care.


=head1 OPTIONS

=head2 required parameter:

  --input <EMBL-file>    EMBL-File to be processed. If '--output' parameter is
                         omitted, a new file with additional ".ass" is generated

=head2 optional parameters:

  --ECpred				 Include EC-number prediction and annotate /EC_number tags
  --output <filename>    Output EMBL-File, that includes assignments
  --onlyblast            Only process BLASTP-reports for assignment
  --onlyhmmer            Only process HMMer-reports for assignment
  --verbose              Verbose output
  --help                 Display help

=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 University of Tuebingen
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version
# so we switch to bioperl live in my home directory
#use lib "/home/weber/perllib";
use strict;
use Bio::SeqIO;
use Bio::Annotation::Comment;
use CLUSEAN::AssignFunction;
use Getopt::Long;
use File::Temp qw(tempdir);
use File::Basename;

# Variables
my $oSeqIOIn;
my $oSeqIOOut;
my %hParams;
my $bVerbose = 0;
my $print_progress = 0;
$hParams{verbose}       = \$bVerbose;
$hParams{'BlastCutOff'} = 1e-2;
$hParams{'HMMerCutOff'} = 1e-2;
$hParams{'progress'} = \$print_progress;
my $sTempDirBase = $ENV{TEMP} || '/tmp';
my $iCleanTemp = 1;
GetOptions(
    \%hParams,    'input|i=s',
    'output|o=s', 'onlyhmmer',
    'onlyblast',,
    'ECpred',        'BLASTTab=s',
    'HMMerTab=s',    'BlastCutOff=f',
    'HMMerCutOff=f', 'help',
    'verbose', 'progress'
);

if ( $hParams{help} ) {
    print "\n\n";
    eval { require Pod::Text };
    if (@$) {
        print
        "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
        "To view help, please install Pod::Text module or look directly into\n";
        print "the perl script $0\n\n";
    }
    else {
        my $oPodParser = Pod::Text->new();
        $oPodParser->parse_from_file($0);
    }
    exit 0;
}

if ( !defined $hParams{input} ) {
    print "Parameter --input required\n";
    exit 1;
}

if ( !-e $hParams{input} ) {
    print "EMBL Inputfile $hParams{input} not found\n";
    exit 1;
}

my ( undef, $sInputDirName, undef ) = fileparse( $hParams{input} );

if ($bVerbose) {
    print "Inputfile basedir is $sInputDirName \n";
}

if ( !defined $hParams{output} ) {
    $hParams{output} = $hParams{input};
    $hParams{output} =~ s/\.([^\.]*)$/.ass.$1/;
}

if ( defined $hParams{BLASTTab} ) {
    if ( !-e $hParams{BLASTTab} ) {
        print $hParams{BLASTTab}, "\n";
        die "Blast Table $hParams{BLASTTab} not found \n";
    }
}

if ( defined $hParams{'HMMerTab'} ) {
    if ( !-e $hParams{'HMMerTab'} ) {
        die "HMMer Table $hParams{'HMMerTab'} not found \n";
    }
}

if ($bVerbose) {
    print
    "Processing inputfile $hParams{input} and outputfile $hParams{output}\n";
}

# to help debugging: Lets start the tempfile with date/time
my ( $sec, $min, $hour, $day, $mon, $year, $wday, $yday, $isdst ) = localtime;
$year += 1900;
$mon++;

# Create temporary directory below $sTempDirBase
# Cleanup after finishing
my $sTempDir = tempdir(
    "Assign_" . $year . $mon . $day . $hour . $min . $sec . "XXXX",
    DIR     => $sTempDirBase,
    CLEANUP => $iCleanTemp
);
$oSeqIOIn = Bio::SeqIO->new( "-file"   => $hParams{input},
    "-format" => "EMBL" );
$oSeqIOOut = Bio::SeqIO->new( "-file"   => ">" . $hParams{output},
    "-format" => "EMBL" );
while ( my $oSeq = $oSeqIOIn->next_seq ) {
    if ($bVerbose) {
        print "Processing ", $oSeq->id, "\n";
    }
    my @features = grep ( $_->primary_tag eq 'CDS', $oSeq->top_SeqFeatures );
    my $feature_count = 1;
    foreach my $oFeature ( @features )
    {
        my @aPredSource;
        my $oAssignment;
        my @AoAAssignments;
        my $done_percentage = $feature_count / (scalar @features ) * 100;
        if ($bVerbose) {
            print "processing CDS ", $oFeature->start, "..", $oFeature->end,
            "\n";
        }


        # Check wheter BLASTP-processing is required
        if ( ( $oFeature->has_tag('blastp_file') ) && ( !$hParams{onlyhmmer} ) )
        {
            my $sBlastFile;
            ( my $sGzBlastFile ) = $oFeature->each_tag_value('blastp_file');
            $sGzBlastFile = $sInputDirName . $sGzBlastFile;
            if ( -e $sGzBlastFile . ".gz" ) {
                $sBlastFile = "$sTempDir/blastrep.bls";
                system("gunzip -c $sGzBlastFile > $sBlastFile");
                my $exit_value = $? >> 8;
                if ( !$exit_value == 0 ) {
                    die "Error Uncompressing $sBlastFile";
                }
            }

            # Otherwise use file directly
            elsif ( -e $sGzBlastFile ) {
                $sBlastFile = $sGzBlastFile;
            }
            else {
                die "Couldn't find Blastfile: $sGzBlastFile\n";
            }
            if ($print_progress) {
                printf("XXX\n%d\nProcessing $sBlastFile (%.2f %%)\nXXX\n", ($done_percentage, $done_percentage));
            }

            push( @aPredSource, "BLASTP" );

            $oAssignment =
            CLUSEAN::AssignFunction->new(
                '-reportfile' => $sBlastFile,
                '-table'  => $hParams{'BLASTTab'},
                '-CutOff' => $hParams{'BlastCutOff'}
            );
            @AoAAssignments = $oAssignment->doAssignment;
            if ($bVerbose) {
                print "Assignment after BLAST processing: ",
                $oAssignment->getAssignmentName, " (";
                print sprintf "%.3f) \n", $oAssignment->getAssignmentScore;
            }
        }

        # Check wheter HMMer-Processing is required
        if ( ( $oFeature->has_tag('HMMer_file') ) && ( !$hParams{onlyblast} ) )
        {
            ( my $sHMMerFile ) = $oFeature->each_tag_value('HMMer_file');
            $sHMMerFile = $sInputDirName . $sHMMerFile;
            if ( !-e $sHMMerFile ) {
                die "Couldn't find HMMer-File: $sHMMerFile\n";
            }

            if ($print_progress) {
                printf("XXX\n%d\nProcessing $sHMMerFile (%.2f %%)\nXXX\n", ($done_percentage, $done_percentage));
            }

            push( @aPredSource, 'HMMER' );
            $oAssignment =
            CLUSEAN::AssignFunction->new(
                '-reportfile' => $sHMMerFile,
                '-method'     => 'HMMTab',
                '-mergeAss'   => \@AoAAssignments,
                '-table'  => $hParams{'HMMerTab'},
                '-CutOff' => $hParams{'HMMerCutOff'}
            );
            @AoAAssignments = $oAssignment->doAssignment;
            if ($bVerbose) {
                print "Assignment after HMMer processing: ",
                $oAssignment->getAssignmentName, " (";
                print sprintf "%.3f)\n",$oAssignment->getAssignmentScore;
            }
        }

        # Add label tag with assignment
        next if ( scalar @AoAAssignments eq 0 );
        $oFeature->add_tag_value( 'label', $oAssignment->getAssignmentName );

        # set up note tag containig the complete prediction table
        my $sNote = "Assignments (";
        $sNote .= join( ", ", @aPredSource );
        $sNote .= "):";
        my @AssLine;
        foreach my $AssLine (@AoAAssignments) {
            $sNote .= sprintf( " $AssLine->[0] (%.3f),", $AssLine->[1] );
        }
        $sNote =~ s/,$//;
        $oFeature->add_tag_value( 'note', $sNote );

        # Add EC-Annotation

        if ( $hParams{ECpred} ) {
            if ( ( $oFeature->has_tag('ECpred_file') ) && ( $hParams{ECpred} ) )
            {
                ( my $sgzECpredFile ) =
                $oFeature->each_tag_value('ECpred_file');
                $sgzECpredFile = $sInputDirName . $sgzECpredFile;
                my $sECpredFile;
                if ( -e $sgzECpredFile . ".gz" ) {
                    $sECpredFile = "$sTempDir/blastrep.bls";
                    system("gunzip -c $sgzECpredFile > $sECpredFile");
                    my $exit_value = $? >> 8;
                    if ( !$exit_value == 0 ) {
                        die "Error Uncompressing $sECpredFile";
                    }
                }

                # Otherwise use file directly
                elsif ( -e $sgzECpredFile ) {
                    $sECpredFile = $sgzECpredFile;
                }
                else {
                    die "Couldn't find Blastfile: $sgzECpredFile\n";
                }
                if ($print_progress) {
                    printf("XXX\n%d\nProcessing $sECpredFile (%.2f %%)\nXXX\n", ($done_percentage, $done_percentage));
                }
                $oAssignment =
                CLUSEAN::AssignFunction->new( '-reportfile' => $sECpredFile,
                    '-method'     => 'ECpred' );
                @AoAAssignments = $oAssignment->doAssignment();

                next if (! $oAssignment->getAssignmentName);

                if ($bVerbose) {
                    print sprintf "EC-Prediction: ".$oAssignment->getAssignmentName." (Score: %.3f)\n",
                    $oAssignment->getAssignmentScore;
                }

                # Add EC tag with assignment
                next if (( scalar @AoAAssignments eq 0 ) ||
                    ( $oAssignment->getAssignmentName eq "unassigned"));
                $oFeature->add_tag_value( 'EC_number',
                    $oAssignment->getAssignmentName );

                # set up note tag containig the complete prediction table
                my $sNote = "EC predicition:";
                my @AssLine;
                foreach my $AssLine (@AoAAssignments) {
                    $sNote .=
                    sprintf( " $AssLine->[0] (score: %.3f),", $AssLine->[1] );
                }
                $sNote =~ s/,$//;
                $oFeature->add_tag_value( 'note', $sNote );
            }
        }
        $feature_count++;
    }

    # Add some additional general annotation
    $oSeq->add_date(
        sprintf( "%04u-%02u-%02u  (added autoannotaion)", $year, $mon, $day ) );
    my $sCCText = "This sequence has been processed with the AssignFunction ";
    $sCCText .=   "library written by T. Weber. The function prediction is based ";
    $sCCText .=   "on the evaluation of BLASTP and HMMer reports. It is still in ";
    $sCCText .=   "experimental stage, so the assignments should be handled with ";
    $sCCText .=   "care. If you encounter wrong assignments, please contact T. ";
    $sCCText .=   "Weber, email: tilmann.weber\@biotech.uni-tuebingen.de";
    my $bIsAlreadyAnnotated;
    my $oAnnotationColl = $oSeq->annotation;
    my @aoComments      = $oAnnotationColl->get_Annotations('comment');

    foreach my $sComment (@aoComments) {
        if ( $sComment->as_text eq $sCCText ) {
            $bIsAlreadyAnnotated = 'TRUE';
        }
    }
    if ( !$bIsAlreadyAnnotated ) {
        $oAnnotationColl->add_Annotation( 'comment',
            Bio::Annotation::Comment->new( '-text' => $sCCText ) );
    }
    $oSeqIOOut->write_seq($oSeq);
}
