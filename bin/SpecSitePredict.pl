#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program runs analyses based on the extraction of specific amino acids
#
# InputFiles: EMBL-Format-File, processed with CLUSEAN pipeline (evaluateDomains.pl)
#
# Output: EMBL-File with blastp_file tags
#         HMMer-Reports of identified genes
#
#

=head1 NAME

SpecSitePredict.pl

=head1 SYNOPSIS

SpecSitePredict.pl  --input <input-EMBL-file> --output <output-EMBL-file> [--onlynew --csv <CSV-file>
--noCSVheader --verbose --help]

=head1 DESCRIPTION

	Identifies and annotates conserved amino acids within specific protein domains
	Configuration: $CLUSEANROOT/etc/SignatureResources.xml

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --onlynew                         :       Only process CDS features with no
                                             previous results
   --csv <filename>					  :		   In addition to EMBL export, also export as CSV
   --noCSVheader					     :		   Omit CSV headerline (useful for automated processing of
   											 the CSV line	                                             
                                             
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use CLUSEAN::AligAnal;
use CLUSEAN::Generic;
use CLUSEAN::GetDomains;
use Bio::SeqIO;
use Getopt::Long;
use XML::Simple;
use FindBin qw ($Bin);
use Config::General;
use File::Basename;
use Data::Dumper;

my $sInputSeqFn;     # Filename for InputFile
my $sOutputSeqFn;    # Filename for Outputfile
my $sCSVOutFn;       # Filename for annotation output file
my $bVerbose;        # Verbose flag
my $bHelp;           # Help flag
my $bNoCSVHead;      # Don't print CSV-Headerline of field descriptions
my $oSeqIOIn;        # Input SeqIO-Object
my $oSeqIOOut;       # Output SeqIO-Object
my $oSeq;            # Bio::Seq main object

#Import Configuration data
my $CLUSEANROOT;
if ( defined $ENV{CLUSEANROOT} ) {
    $CLUSEANROOT = $ENV{CLUSEANROOT};
}
else {
    $CLUSEANROOT = $Bin;
    $CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
}

# Parse Config file
my $oConfig =
  Config::General->new( '-ConfigFile' => "$CLUSEANROOT/etc/CLUSEAN.cfg" );
my %hConfig = $oConfig->getall;

my $sSeparator = $hConfig{Formatting}->{Separator}
  || "|";    # Character for joining entries;
my $sXMLFileName = $hConfig{SignatureResources}->{SignatureResourceFile};
my $sProfileDir = $hConfig{SignatureResources}->{DBDir};

if ( !-e "$CLUSEANROOT/$sXMLFileName" ) {
    die "Can't find Singnature XML file at $CLUSEANROOT/$sXMLFileName\n";
}

# Parse XML-File and assign hashref
my $xml = XML::Simple->new(
    'ForceArray' => [ ( 'choice', 'scaffold' ) ],
    'SuppressEmpty' => 1
);
my $xmlHash = $xml->XMLin("$CLUSEANROOT/$sXMLFileName");

# Evaluate Cmdline and assign variables
GetOptions(
    'input|i=s'     => \$sInputSeqFn,
    'output|o=s'    => \$sOutputSeqFn,
    'csv|c=s'       => \$sCSVOutFn,
    'noCSVHeader|n' => \$bNoCSVHead,
    'verbose|v'     => \$bVerbose,
    'help|h'        => \$bHelp
);

# Check parameters
if ($bHelp) {
    print "\n\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
"To view help, please install Pod::Text module or look directly into\n";
        print "the perl script $0\n\n";
    }
    else {
        my $oPodParser = Pod::Text->new();
        $oPodParser->parse_from_file($0);
    }
    exit 0;
}
if ( !-e $sInputSeqFn ) {
    die "EMBL Inputfile $sInputSeqFn not found";
}

my ( undef, $sInputDirName, undef ) = fileparse($sInputSeqFn);

# First we have to load the annotated EMBL-file
$oSeqIOIn = Bio::SeqIO->new(
    '-file'   => $sInputSeqFn,
    '-format' => 'EMBL'
);

# create output object
$oSeqIOOut = Bio::SeqIO->new(
    '-file'   => ">$sOutputSeqFn",
    '-format' => 'EMBL'
);

if ( defined $sCSVOutFn ) {

    # open Filehandle for CSV output
    open( CSVOUT, ">$sCSVOutFn" )
      || die "Cannot open $sCSVOutFn for writing";

    if ( !defined $bNoCSVHead ) {
        my @aHeader = (
            "Accession number",
            "Analysis name",
            "Analysis description",
            "GeneName",
            "CDSstart",
            "CDSend",
            "FeatureLabel",
            "FeatureStart",
            "FeatureEnd",
            "QueryScaffold",
            "HitScaffold",
            "Scaff. binary comp.",
            "Extracted residues signature",
            "Corresponding signature of hit",
            "Reference signature to compare with",
            "Binary comparison",
            "Result Name",
            "Description of Result"
        );
        print CSVOUT join( $sSeparator, @aHeader ), "\n";
    }
}

# For every Sequence in File...
while ( $oSeq = $oSeqIOIn->next_seq ) {

    # get sequence id
    my $sSeqID = $oSeq->id;

    # if ID is unknown check accession...
    if ( $sSeqID =~ /.*unknown.*/ ) {
        $sSeqID = $oSeq->accession;
    }
    $sSeqID =~ s/\s/_/g;
    $sSeqID =~ s/\;$//;
    my @aAnalyses = keys( %{$xmlHash} );

    # For each analysis defined in SignatureResources.xml file
    foreach my $sAnalysis (@aAnalyses) {

        # Skip xmlns data...
        next if ( $sAnalysis =~ /xmlns.*/ );

        # only run on prediction type analyses

        next if ( $xmlHash->{$sAnalysis}->{type} !~ /prediction/ );

        if ($bVerbose) {
            print "---------\n";
            print "Analysis: $sAnalysis\n";
            print "---------\n";
        }

        # Now cylce through CDS features
        foreach my $oCDSFeature (
            grep ( ( $_->primary_tag eq 'CDS' ), $oSeq->top_SeqFeatures ) )
        {
            my $oOverlappingFeatures = CLUSEAN::GetDomains->new(
                '-featureObj' => \$oCDSFeature,
                '-seqObj'     => $oSeq
            );
            my @aOverlappingFeatures = $oOverlappingFeatures->getSubfeatures;

            # skip CDS if there are no overlapping features for this CDS
            next if ( !defined $aOverlappingFeatures[0] );
            my @aFeatures = grep ( (
                    (
                        $_->primary_tag eq
                          $xmlHash->{$sAnalysis}->{Prerequisite}
                          ->{primary_tag_type}
                    )
                      && (
                        $_->has_tag(
                            $xmlHash->{$sAnalysis}->{Prerequisite}->{tag}
                        )
                      )
                ),
                @aOverlappingFeatures );

            foreach my $oFeature (@aFeatures) {

                # Skip CDS if it is not annotated as NRPS or PKSI
                my ($sLabel) =
                  $oFeature->get_tag_values(
                    $xmlHash->{$sAnalysis}->{Prerequisite}->{tag} );
                chomp($sLabel);
                if ( $sLabel ne
                    $xmlHash->{$sAnalysis}->{Prerequisite}->{tag_value})
                {
                    next;
                }
                if ($bVerbose) {
                    print "Analyzing CDS ",
                      $oCDSFeature->start, "..",
                      $oCDSFeature->end,   " ";
                    if ( $oCDSFeature->has_tag('gene') ) {
                        my ($sGene) = $oCDSFeature->get_tag_values('gene');
                        print "Gene: $sGene\n";
                    }
                    print "\n$sLabel at ",
                      $oFeature->start, "..",
                      $oFeature->end,   "\n";
                }

# Now we should only have $oFeatures that match the prerequisites defined in xml file

                # Attach sequence data
                $oFeature->attach_seq($oSeq);

                # Get gene name
                my $sGene;
                if ( $oCDSFeature->has_tag('gene') ) {
                    ($sGene) = $oCDSFeature->get_tag_values('gene');
                }
                else {
                    $sGene = "noGeneName";
                }

                # Generate AligAnal object
                my $oAligAnal = CLUSEAN::AligAnal->new(
                    '-xml_Hash' => $xmlHash,
                    '-oFeature' => $oFeature,
                    '-program'  => $sAnalysis,
                    '-profiledir'   => $CLUSEANROOT."/".$sProfileDir
                );

                if ($bVerbose) {
                    print "************\n";
                    print "Running analysis: $sAnalysis \n";

                    my @aChoices = @{ $oAligAnal->get_choiceArray };

                    print "Available Choices: ";
                    if ( scalar @aChoices == 0 ) {
                        print "none \n";
                    }
                    else {
                        print
                          join( ", ", @aChoices ),
                          "\n";
                    }
                    print "************\n";
                }

                # Retrieve Scaffold information
                my @aScaffoldHit = @{
                    $oAligAnal->ExtractResidues(
                        @{ $oAligAnal->get_aScaffoldOffset }
                      )->get_aHit
                  };
                my @aScaffoldQuery = @{
                    $oAligAnal->ExtractResidues(
                        @{ $oAligAnal->get_aScaffoldOffset }
                      )->get_aQuery
                  };
                my @aScaffoldEmission = @{ $oAligAnal->get_aScaffoldEmission };

           # If the hit has no sufficient quality to return amino acids, skip it
           # OK this isn't the best solution but it has to work for now

                if ( $aScaffoldHit[0] eq "" ) {
                    print "Low quality match, no evaluation possible \n\n";
                    next;
                }

                # Check whether scaffolds match
                my $oScaffCompArray = CLUSEAN::Generic->new;

                my @ScaffCompArray = $oScaffCompArray->compareArrayRegex(
                    '-Array1' => \@aScaffoldHit,
                    '-Array2' => \@aScaffoldQuery
                );

                my $sScaffCompString = join( "", @ScaffCompArray )
                  ; # @CompArray does contain 1 for every match and 0 for mismatch

                # Output Scaffold information

                if ($bVerbose) {
                    print "###########################\n";
                    print "QueryCoordinates       : ",
                      $oAligAnal->get_HSP_startQ, "..",
                      $oAligAnal->get_HSP_endQ,   "\n";
                    print "Gene                   : $sGene\n";

                    print "H-Scaffold             : ", @aScaffoldHit,   "\n";
                    print "Q-Scaffold             : ", @aScaffoldQuery, "\n";
                    print "MatchArray             : $sScaffCompString\n";
                    print "Offsets to HMM profile : ",
                      join( ", ", @{ $oAligAnal->get_aScaffoldOffset } ),
                      "\n";
                    print "ScaffoldEmission       : ",
                      join( ", ", @aScaffoldEmission ),
                      "\n";

                    if ( $sScaffCompString =~ /0/ ) {
                        print
                          "CAUTION: Scaffold sequence does not match 100% \n";
                    }
                    else {
                        print "Scaffold sequences match :-)\n";
                    }
                    print "###########################\n";

                }

                # Now cycle through available comparisonsi
                while ( my $oComp = $oAligAnal->next_comparisonData ) {

                    {
                        my $ExtrResidues = $oAligAnal->ExtractResidues(
                            @{ $oComp->get_aOffset } );

                        my $oCompArray = CLUSEAN::Generic->new;
                        my @CompArray  = $oCompArray->compareArrayRegex(
                            '-Array1' => $ExtrResidues->get_aQuery,
                            '-Array2' =>
                              $oComp->get_aValue
                        );

                        my $sNote = "Checking for " . $sAnalysis . ". ";
                        if ( $sScaffCompString =~ /0/ ) {
                            $sNote .=
"Match against scaffold residues NOT OK. Please Check! ";
                        }
                        else {
                            $sNote .= "Match against scaffold OK. ";
                        }

                        if ( join( "", @CompArray ) =~ /0/ ) {
                            $sNote .= "NO PERFECT MATCH AGAINST "
                              . $oComp->get_result . ".";
                        }
                        else {
                            $sNote .=
                                "Query: "
                              . join( "", @{ $ExtrResidues->get_aQuery } )
                              . "; ";
                            $sNote .= "Hit against HMM: "
                              . join( "", @{ $ExtrResidues->get_aHit } ) . "; ";
                            $sNote .= "Compared against: "
                              . join( "", @{ $oComp->get_aValue } ) . "; ";
                            $sNote .=
                              "Perfect match for " . $oComp->get_result . ".";
                        }

                        $oFeature->add_tag_value( 'note', $sNote );

                        if ($bVerbose) {

                            print "\n\n---------\n";
                            print
                              "Testing choice   : ",
                              $oComp->get_result,
                              "\n";
                            print "\n";

                            print
                              "H-ExtrRes        : ",
                              @{ $ExtrResidues->get_aHit }, "\n";
                            print
                              "Q-ExtrRes        : ",
                              @{ $ExtrResidues->get_aQuery }, "\n";
                            print
                              "Offset to HMMprof: ",
                              $oComp->get_sOffset,
                              "\n";

                            print
                              "Compared against : ",
                              @{ $oComp->get_aValue }, "\n";
                            print
                              "ResultA          : ",
                              @CompArray,
                              "\n";
                            print
                              "Emission         : ",
                              join( ", ", @{ $oComp->get_aValueEmission } ),
                              "\n";

                            if ( join( "", @CompArray ) =~ /0/ ) {
                                print
                                  "NO MATCH AGAINST ",
                                  $oComp->get_result,
                                  "\n";
                            }
                            else {
                                print
                                  "Perfect match for ",
                                  $oComp->get_result,
                                  "\n";
                            }

                            print "------------\n\n";
                        }

                        if ($sCSVOutFn) {
                            my $sSeqID = $oSeq->accession;
                            $sSeqID =~ s/;$//;

                            # Assemble output line
                            # Accession number |
                            my $sLine = $sSeqID . $sSeparator;

                            # AnalysisName |
                            $sLine .=
                                $sAnalysis
                              . $sSeparator
                              . $xmlHash->{$sAnalysis}->{ 'description' }
                              . $sSeparator;

                            # GeneName | CDSstart | CDSend
                            $sLine .=
                                $sGene
                              . $sSeparator
                              . $oCDSFeature->start
                              . $sSeparator;
                            $sLine .= $oCDSFeature->end . $sSeparator;

                            # FeatureLabel | FeatureStart | FeatureEnd
                            $sLine .=
                                $sLabel
                              . $sSeparator
                              . $oFeature->start
                              . $sSeparator;
                            $sLine .= $oFeature->end . $sSeparator;

                            #Scaffold information
                            $sLine .= join(
                                "",
                                @{
                                    $oAligAnal->ExtractResidues(
                                        @{ $oAligAnal->get_aScaffoldOffset }
                                      )->get_aQuery
                                  }
                            ) . $sSeparator;
                            $sLine .= join(
                                "",
                                @{
                                    $oAligAnal->ExtractResidues(
                                        @{ $oAligAnal->get_aScaffoldOffset }
                                      )->get_aHit
                                  }
                            ) . $sSeparator;

                            $sLine .= $sScaffCompString . $sSeparator;

                 # Extracted Residues Signature | Corresponding signature of hit
                            $sLine .=
                              join( "", @{ $ExtrResidues->get_aQuery } )
                              . $sSeparator;
                            $sLine .=
                              join( "", @{ $ExtrResidues->get_aHit } )
                              . $sSeparator;

                            # Reference signature to compare with
                            $sLine .=
                              join( "", @{ $oComp->get_aValue } ) . $sSeparator;

            # Binary comparison; 1 means identity at this position 0 means other
                            $sLine .= join( "", @CompArray ) . $sSeparator;

                            # Result Name | description of Result
                            $sLine .=
                                $oComp->get_result
                              . $sSeparator
                              . $oComp->get_comment;

                            # And print...
                            print
                              CSVOUT $sLine,
                              "\n";
                        }
                    }
                }
            }
        }
        if ($bVerbose) {
            print "//\n\n";
        }
    }

    # Write results to output EMBL file
    $oSeqIOOut->write_seq($oSeq);
}
