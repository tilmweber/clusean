#!/usr/bin/perl


=head1 NAME

identifyClusters.pl

=head1 SYNOPSIS

identifyClusters.pl <CSV-Filename> [--CalibFileName <CalibTableFile> --useScore --mode <1/2> --verbose --help]

=head1 DESCRIPTION

identifyClusters.pl ealuates output of the Assignment tools on genome scale identifying
putative secondary metabolite gene clusters

(Not yet fully implemented)

=head1 OPTIONS

=head2 required parameter

   <CSV-Filename>          : Filename of CSV-File generated with EMBLtoCSV.pl -a -t --noHeader
   
=head2 optional parametes

   --CalibrationTable      : Filename of the calibration table,
   							 defaults to $CLUSEANROOT/lib/IdClust.statistics.txt
   							 
   --WindowSize            : Size of analysis window
   							 default: 40
   							 
   --mode				   : Analysis mode
   								1: (default) Analyze every gene as single entity
   								2: Use WindowSize to analyze genes in chunks

   --useScore              : Take Assignment scores into account   								
   							 
   --verbose               : Verbose output
   
   --help                  : Help text
   
=head1 AUTHOR

 Dr. Tilmann Weber
 Microbiology/Biotechnology
 University of Tuebingen
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut	
						 
use warnings;
use Getopt::Long;
use strict;


# define CmdLine Vars

my $sCalibFileName  = $ENV{'CLUSEANROOT'}.'/lib/IdClust.statistics.txt';
my $iWindowsSize    = 40;
my $iMode           = 1;

my	($bVerbose,	$bHelp, $buseScore);
	
# Get CmdLineOptions
GetOptions ('CalibFileName|c=s'  =>  \$sCalibFileName,
			'WindowSize|w=i'	 =>  \$iWindowsSize,
			'mode|m=i'			 =>  \$iMode,
			'useScore|s'         =>  \$buseScore,
			'verbose|v'		     =>  \$bVerbose,
			'help|h'		     =>  \$bHelp);
			
			
if ($bHelp) {
  print "\n\n";
  eval {require Pod::Text};
  if ( @$ ) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }
  exit 0;
}

die "Error: No filename given! See --help for information.\n" if (! defined $ARGV[0] );

#Define global variables

my %CalTable;    # Hash containig Assigment-calibrations
my @Assignment;
my @AssScore;


# process calibration table
open (CALIB, $sCalibFileName) || die "Can't open calibration file $sCalibFileName!\n";

while (my $readln = <CALIB>){
	chomp ($readln);
	my ($Assignment, undef, undef, $meanScore) = split (/,/,$readln);
	$CalTable{$Assignment} = $meanScore;
}
close (CALIB);

			
# open EMBL2CSV file
open (INFILE, $ARGV[0]) || die "Couldn't open $ARGV[0]!";

# 


# now cycle through CSV file
my $orfNo = 1;

while (my $readln=<INFILE>) {
	
	# Skip header line
	next if ($readln =~ /^Filename/);
	
	chomp ($readln);
	
	# Get Assignment and Assignment Score
	my @fields = split (/\|/, $readln);
	# $EMBLid = $fields[1]; # maybe we expand this script some time to process multiple files
	$Assignment[$orfNo] = $fields[9];
	$AssScore[$orfNo]   = $fields[10];
	
	# If AssignFunction didn't result in a hit, set Score to 0
	if (($Assignment[$orfNo] eq "unclassified") || 
		($Assignment[$orfNo] eq "unassigned") ||
		($Assignment[$orfNo] eq "hyp")) {
		$AssScore[$orfNo] = 0;
	}
	
	# Normalize Assignment score
	# print $orfNo, "\t", $Assignment[$orfNo],"\n";
	if ((! defined $CalTable{$Assignment[$orfNo]}) ||
		($CalTable{$Assignment[$orfNo]} == 0)) {
		$AssScore[$orfNo]   = 0;
	}
	else {
		if (defined $buseScore) {
			$AssScore[$orfNo]   = $AssScore[$orfNo] / $CalTable{$Assignment[$orfNo]};
		}
		else {
			$AssScore[$orfNo]   = $CalTable{$Assignment[$orfNo]};
		}
	}
	
	# If AssignFunction didn't result in a hit, set Score to 0
	if (($Assignment[$orfNo] eq "unclassified") || 
		($Assignment[$orfNo] eq "unassigned") ||
		($Assignment[$orfNo] eq "hyp")) {
		$AssScore[$orfNo] = 0;
	}
	
	$orfNo++;
}

# Assign number of ORFs
my $TotalOrfs = $orfNo-1;

# Select analysis mode
if ($iMode eq "1") {
	
	# print Assignment score for every CDS feature
	for (my $i=1; $i <= $TotalOrfs; $i++) {
		print $i,",orf",$i,",",$AssScore[$i],",",$Assignment[$i],"\n";
	}
}	
elsif ($iMode eq "2") {
	
	# We need WindowsSize/2 (modulo)
	my $halveIterator = sprintf ('%u', $iWindowsSize / 2)+ $iWindowsSize % 2;
	
	my @WindowScore;
	
	# now cycle through windows
	for (my $i=$halveIterator + 1 ; $i <= $TotalOrfs - $halveIterator; $i++) {
		$WindowScore[$i]=0;
		# determine score sum for every window
		for (my $j=$i-$halveIterator; $j <= $i+$halveIterator; $j++) {
			$WindowScore[$i] += $AssScore[$j];
		}
		
		# normalize score on complete window
		$WindowScore[$i] /= $iWindowsSize;
		
		# and print it...
		print $i-$halveIterator,",orfwindow",$i-$halveIterator,"-",$i+$halveIterator,",",$WindowScore[$i],",","\n";
	}
}
