#!/usr/bin/perl

=head1 NAME

NRPSpredWrap.pl

=head1 SYNOPSIS

evaluateDomains.pl --input <FastA-Filename> 

=head1 DESCRIPTION

Wrapper script that executed the NRPSpredictor and returns the analysis results in STDOUT

=head2 PARAMETERS
    --input <filename>         Name of FastA file used for input
    
    
=head2 OPTIONAL PARAMETERS
    
    --verbose                  Print diagnostics information
    --help                     Show this documentation
   

=head3 AUTHORS

 NRPSPredictor:
 Christian Rausch
 Algorithms in Bioinformatics
 Center for Bioinformatics T�bingen (ZBiT)
 Sand 14
 72074 Tuebingen
 
 Wrapper script:
 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# So let the code begin...

use strict;                   # To be strict is always good ;-)
use File::Temp qw(tempdir);   # To get an unique Tempdir
use Getopt::Long;             # To process CmdLine
#use IO::CaptureOutput qw (capture_exec);        # A convenient way to handle STDOUT and STDERR
use File::Copy;                                 # A convenient way to copy files
use File::Basename;                             # A convenient way to get filename
use Config::General;
use FindBin qw ($Bin);

# Global variables declaration


#Import Configuration data

	my $sCLUSEANROOT;
	if (defined $ENV{CLUSEANROOT}) {
		$sCLUSEANROOT = $ENV{CLUSEANROOT};
	}
	else {
		$sCLUSEANROOT = $Bin;
		$sCLUSEANROOT =~ s/(.*)\/.*?$/$1/;
	
	}

# Parse Config file
	my $oConfig = Config::General->new ('-ConfigFile'     =>  "$sCLUSEANROOT/etc/CLUSEAN.cfg");
	my %hConfig = $oConfig->getall;

my $sNRPSPredCMD  = $ENV{NRPSPredCMD}   || $hConfig{NRPSPredictor}->{Exe} || "$sCLUSEANROOT/bin/NRPSPredWrap.pl";

# YOU MAY HAVE TO CHANGE THIS LINES according to your systems configuration!!
my $NRPSbinDir     = $hConfig{NRPSPredictor}->{NRPSBinDir};
my $NRPSbinDirExe  = $hConfig{NRPSPredictor}->{NRPSBinDir}; # For %&/($ MSWindows we have to change / to \ here
my $NRPSbin        = $hConfig{NRPSPredictor}->{NRPSBin};         # NRPSPredictor script


my $rSTDERR;     # STDERR


my $sInputSeqFn ='';               # Filename for InputFile

my $bVerbose;                      # Verbose flag
my $bHelp;                         # Help flag

my $sTempDirBase = $ENV{TEMP} || '/tmp';  # Temp dir, use /tmp if no TEMP environment variable is set
my $iCleanTemp   = 1;                     # Delete directory after execution of the script

my $sCmdLine;        # For assembling the NRPSpredictor cmdline
my $OS = $^O;        # Get OS string!
my $sDirectorySeparator;    # / for Linux, \ for Win

if ($OS =~ /MSWin.*/) {
	$sDirectorySeparator = "\\";
}
else {
	$sDirectorySeparator = "/";
}
# Evaluate Cmdline and assign variables

GetOptions ( 'input|i=s'               =>     \$sInputSeqFn,
		     'verbose|v'               =>     \$bVerbose,
             'help|h'                  =>     \$bHelp);


# Check parameters

if (($bHelp) || ($sInputSeqFn eq "")) {
  print "\n\n";
  eval {require Pod::Text};
  if (@$) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }
#  print `pod2text $0`;
  exit 0;
}

if (! -e $sInputSeqFn) {
  die "Inputfile $sInputSeqFn not found";
}

if (! -e "$NRPSbinDir/$NRPSbin") {
	die "Could not find NRPSPredictor script $NRPSbin in $NRPSbinDir"
}


# to help debugging: Lets start the tempfile with date/time

my ($sec, $min, $hour, $day, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
$mon++;

# Create temporary directory below $sTempDirBase

my $sTempDir = tempdir ("NRPSpredWrap_".$year.$mon.$day.$hour.$min.$sec."XXXX",
			DIR      => $sTempDirBase,
			CLEANUP  => $iCleanTemp);
			
# We need to get the filename without path

my $sFileName = fileparse ($sInputSeqFn);

# We have to convert some Linux/perl "/" into "\" for Windows and vice versa
if ($OS =~ /MSWin.*/) {
	$NRPSbinDirExe =~ s/\//\\/g;
	$sTempDir =~ s/\\/\//g;
}

# now we can assemble the NRPSpredictor Cmdline

$sCmdLine = $NRPSbinDirExe.$sDirectorySeparator.$NRPSbin." ".$sFileName." ".$sTempDir." ".$NRPSbinDir;

if ($bVerbose) {
	print "VERBOSE: Starting script with the following CmdLine:\n";
	print $sCmdLine,"\n";
}

# OK, now do it

# first we have to copy the Inputfile to $sTempDir
# Herefore, we need File::Copy...

copy ($sInputSeqFn, $sTempDir) || die "Error, can't copy $sInputSeqFn to $sTempDir";


# Now execute and collect STDOUT and STDERR
# Herefore we need IO::CaptureOutput
# OK, Backticks would have done the job, too, but this way it's nicer...

#($rSTDOUT, $rSTDERR) = capture_exec($sCmdLine);
# $rSTDOUT = `$sCmdLine`;
system ($sCmdLine);

# Now output the data
open (NRPSPOUT, "$sTempDir/NRPSPredictor.out") || die "Can't find NRPSPredictor outputfile";

while (my $output=<NRPSPOUT>) {;
	print $output;
}
			
