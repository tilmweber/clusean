#!/usr/bin/perl
#
# Author: Tilmann Weber
# Last change: 2003/12/16
#
# This program combines assembled contigs (by consed) and critica
# gene-finding results.
#
# InputFiles: Contigs as multi-FASTA
#             Critica-Output: .cds file of last iteration
#
# Output: EMBL-Files
#         BLASTP-Reports of identified genes
#
# Designed for internal use only!!!!

=head1 NAME

AnalyzeCritica.pl

=head1 DESCRITPTION

Automatic conversion of fasta DNA-sequence file and 
CRITICA orf-finding output to EMBL format file. Automatic blastp analysis
can be included by cmd-line parameter.

=head1 USAGE

B<AnalyzeCritica.pl>  --input <fasta-filename> --critica <critica-filename>
[--blastpdir <directory-name> --blastparms <parameters>
--runblast --verbose --help]

The script generates separate EMBL-files, with are named after their
fasta header, eg. a Contig with fasta description
">Contig1     testcluster.ace.1"
is converted to Contig1.embl

=head2 PARAMETER description

    --input <fasta-filename>       : Name of the (multi)fasta-file
    --critica <critica-filename>   : Name of the critica cds 
                                           output file

=head3 Optional parameters

    --verbose                      : verbose output
    --help                         : print help information

=head3 (Optional) Environment Variables

    B<Name>                  B<defaults to>

    CLUSEANROOT                 /usr/local/abdb
 

=head1 REQUIREMENTS

This script requires the following perl-modules installed on your system:

 Bioperl (Bio::SeqIO, Bio::SeqFeature::Generic)
 Getopt::Long
 Data::Dumper

For inline-blasting a working ncbi-blast package is required

=head1 AUTHOR

Tilmann Weber

Microbiology/Biotechnology

Auf der Morgenstelle28

72076 Tuebingen

Germany

Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version


use strict;
use Bio::SeqIO;
use Bio::SeqFeature::Generic;
use Getopt::Long;
#use Data::Dumper;


# Global variables

my $BLASTCMD   = $ENV{BLASTCMD} || "nice -n 20 blastall";
my $BSEARCHDB  = $ENV{BSEARCHDB}|| "swall";
my $sBlastParms= $ENV{BPARMS}   || "-a 2 -FF";

my $sInputSeqFn;                 # Filename for InputFile
my $sInputCriticaFn;             # Filename for CriticaFile
my $sOutputFilename;             # Filename for OutputFile



my $bVerbose;                    # Verbose flag
my $bHelp;                       # Help flag

my $oSeqIOIn;                    # Input SeqIO-Object
my $oSeqIOOut;                   # Output SeqIO-Object
my $oSeq;                        # Bio::Seq main object
my %hCriticaData;                # Hash containing a AoAoA with critica results


# Evaluate Cmdline and assign variables

GetOptions ( 'input|i=s'               =>     \$sInputSeqFn,
	     'critica|c=s'             =>     \$sInputCriticaFn,
	     'blastparms=s'            =>     \$sBlastParms,
	     'verbose|v'               =>     \$bVerbose,
             'help|h'                  =>     \$bHelp);

# Check parameters


if ($bHelp) {
  print "\n\n";
  eval {require Pod::Text};
  if (@$) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }
#  print `pod2text $0`;
  exit 0;
}
if (! -e $sInputSeqFn) {
    die "EMBL Inputfile $sInputSeqFn not found";
}


if (! -e $sInputCriticaFn) {
    die "Critica Inputfile $sInputCriticaFn not found";
}




# First we have to load the assembled Contigs
# ("Save all contigs" outputfile from consed)

$oSeqIOIn  =   Bio::SeqIO->new ('-file'    =>  $sInputSeqFn,
				'-format'  =>  'fasta');

# Open Critica Resultfile

open (CRITFILE, $sInputCriticaFn) or die "Can't open CriticaFile \n";


# For every line of Critica output
while (my $readln =<CRITFILE>) {

    # trim leading blanks
    $readln =~ s/^\s+//;

    # First let's get a line of CRITICA output and assign to array
    my ($contig, @CriticaLine) = split (/\s+/,$readln);

    # retrieve previous orfs for actual contig
    my $aTmp=$hCriticaData{$contig};

    # add new orf to array and to hash
    push (@{$aTmp}, [@CriticaLine] );
    $hCriticaData{$contig}=$aTmp;
  }

# Now we have all Critica-Orfs saved in %hCriticaData
# Let's assign them...

# For every Contig...
while ($oSeq=$oSeqIOIn->next_seq) {

  # local variables
  my $iGeneCount=1;

  # get sequence identifier 
  my $sSeqID=$oSeq->id." ".$oSeq->description;
  $sSeqID =~ s/[\s_]+$//g;
  $sSeqID =~ s/\s/_/g;

  # assign output filename
  my ($sOutputSeqName) = $sSeqID =~ /([a-zA-Z\d]+)/;
  if ($bVerbose) {print "processing $sOutputSeqName.embl\n";}

  # bring sequence identifier to consed style


  # create output object
  $oSeqIOOut=Bio::SeqIO->new ('-file'   => ">$sOutputSeqName.embl",
			      '-format' => 'EMBL');
  
  #if ($bVerbose) {
  #  print "Identified following critica elements:\n";
  #  print Dumper(%hCriticaData),"\n";
  #}

  # Get Arrayref of Critica-Results-Array from %hCriticaData-Hash
  my $aCrit=$hCriticaData{$sSeqID};

  # now cycle though every identified orf..
  foreach my $aCritLine (@{$aCrit}) {
    my $strand;
    my ($startbp,
	$stopbp,
	$pval,
	$matrix,
	$comp,
	$dicod,
	$initiator_score,
	$initiator_codon,
	$sd_score,
	$sd_pos,
	$sd_seq)  = @{$aCritLine};

    # Critica includes the stop codon so we have to remove it
    #$startbp > $stopbp?  ($stopbp += 3)   :   ($stopbp -= 3);

    # If $startbp > $stopbp exchange the position and set -1 strand instead
    if ($startbp > $stopbp) {
      my $helper=$startbp;
      $startbp=$stopbp;
      $stopbp=$helper;
      $strand=-1
    }
    else {
	  $strand=1;
	}

    # now we have all to setup the EMBL-CDS Feature with critica information
    my $oFeature=Bio::SeqFeature::Generic->new(  '-start'       =>  $startbp,
						 '-end'         =>  $stopbp,
						 '-strand'      =>  $strand,
						 '-primary_tag' =>  'CDS');
    my $NoteText  = "CRITICA ORF, Pval=$pval, Initiaton at $initiator_codon";
    $NoteText    .= " with Score $initiator_score, Putative RBS $sd_seq, ";
    $NoteText    .= "Score=$sd_score, Pos=$sd_pos";

    $oFeature->add_tag_value('note',$NoteText);

    # If blast-Processing is specified on CmdLine include this information
    
    # Now we have to add the filled Up Feature to the original Seq-object
    $oSeq->add_SeqFeature($oFeature);
    $iGeneCount++;
  }

  # Include all Features to output file
  $oSeqIOOut->write_seq($oSeq);
}
