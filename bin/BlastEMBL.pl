#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program runs blastp agains all CDS-Features found in EMBL-File
#
# InputFiles: EMBL-Format-File
#
# Output: EMBL-File with blastp_file tags
#         BLASTP-Reports of identified genes
#
#

=head1 NAME

BlastEMBL.pl

=head1 DESCRIPTION

Script based blasting of all CDS features in EMBL file

=head1 USAGE

B<BlastEMBL.pl> --input <input-EMBL-file> --output <output-EMBL-file> 
--blastpdir <directoryname> [--blastpdb <database name> 
--bLastparms <blastall parameters> --blastptag <tag> --run --verbose --help
--singleFASTA <filename>]

=head2 PARAMETER description

    --input <filename>       : Name of the embl-file to process
    --output <filename>      : Name of the embl-output file
    --blastpdir <dirname>    : Directory to store blastfiles
    

=head3 Optional parameters

    --blastpdb <database>    : Name of the BLAST database to use
                               default: nr
    --blastparms <parameter> : Parameters for blastall execution
                               default: -a2 -FF
    --blastptag <tag>		 : Tag for blastp_file reference
    						   default: blastp_file
    --singleFasta <filename> : In addtion to the fasta files in the --blastpdir, also
                               generate a multi-Fastafile <filename>                          
    --run                    : Run blast analysis on local machine
    --verbose                : Verbose output
    --help                   : print help information

=head3 (Optional) Environment Variables

   B<Name>                  B<defaults to>
   CLUSEANROOT                 /usr/local/abdb
   BLASTCMD                 nice -n 20 blastall
   BSEARCHDB                nr
   BPARMS                   -a2 -FF

=head1 REQUIREMENTS

This script requires the following perl-modules installed on your system:

 Bioperl (Bio::SeqIO, Bio::SeqFeature::Generic)
 Getopt::Long

For inline-blasting a working ncbi-blast package is required.

=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle28
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version# so we switch to bioperl live in my home directory

#use lib "/home/weber/perllib";

use strict;
use Bio::SeqIO;
use Bio::SeqFeature::Generic;
use Getopt::Long;
use Config::General;
use FindBin qw ($Bin);
use File::Basename;




my $sInputSeqFn;                 # Filename for InputFile
my $sBlastOutputFilename;        # Filename for BlastOutputFiles
my $sOutputSeqFn;                # Filename for Outputfile
my $ssingleFASTA;                # singleFASTA file
my $bRunBlast;                   # Run Blastp from script?

my $bOnlyNew;                    # process only new CDS with no
                                 # previous results
my $bVerbose;                    # Verbose flag
my $bHelp;                       # Help flag

my $oSeqIOIn;                    # Input SeqIO-Object
my $oSeqIOOut;                   # Output SeqIO-Object
my $oSeqIOSingleFasta;           # Output SeqIO-Object for singleFASTA
my $oSeq;                        # Bio::Seq main object

#Import Configuration data

	my $sCLUSEANROOT;
	if (defined $ENV{CLUSEANROOT}) {
		$sCLUSEANROOT = $ENV{CLUSEANROOT};
	}
	else {
		$sCLUSEANROOT = $Bin;
		$sCLUSEANROOT =~ s/(.*)\/.*?$/$1/;
	
	}

# Parse Config file
	my $oConfig = Config::General->new ('-ConfigFile'     =>  "$sCLUSEANROOT/etc/CLUSEAN.cfg");
	my %hConfig = $oConfig->getall;

my $BLASTCMD   = $ENV{BLASTCMD} || $hConfig{'Blast'}->{'Exe'};
my $BSEARCHDB  = $ENV{BSEARCHDB}|| $hConfig{'Blast'}->{'DB'};
my $sBlastParms= $ENV{BPARMS}   || $hConfig{'Blast'}->{'Params'};
my $sBlastPDir = $hConfig{'Blast'}->{'Dir'};
my $sTag       = $hConfig{'Blast'}->{'Tag'};

# Evaluate Cmdline and assign variables

GetOptions ( 'input|i=s'               =>     \$sInputSeqFn,
	     'output|o=s'              =>     \$sOutputSeqFn,
	     'blastpdb|b=s'            =>     \$BSEARCHDB,
	     'blastpdir|d=s'           =>     \$sBlastPDir,
	     'blastptag|t=s'           =>     \$sTag,
	     'singleFASTA=s'           =>     \$ssingleFASTA,
	     'run|r'                   =>     \$bRunBlast,
         'onlynew|n'               =>     \$bOnlyNew,
	     'blastparms|p=s'          =>     \$sBlastParms,
	     'verbose|v'               =>     \$bVerbose,
         'help|h'                  =>     \$bHelp);

# Check parameters


if ($bHelp) {
     print "\n\n";
     eval {require Pod::Text};
     if ( @$ ) {
	 print "Error formatting help content! Perl-Module Pod::Text required!\n";
	 print "To view help, please install Pod::Text module or look directly into\n";
	 print "the perl script $0\n\n";
     }
     else {
	 my $oPodParser = Pod::Text->new ();
	 $oPodParser->parse_from_file($0);
     }
     exit 0;
    }
if (! -e $sInputSeqFn) {
    die "EMBL Inputfile $sInputSeqFn not found";
}
my (undef, $sOutputDirName, undef) = fileparse($sOutputSeqFn);
if ($bVerbose) {
	print "Outputfile basedir is $sOutputDirName \n";
}
	
if (! -e $sOutputDirName.$sBlastPDir) {
  mkdir $sOutputDirName.$sBlastPDir || die "BlastP-Dir: $sBlastPDir cannot be created";
}


# First we have to load the assembled Contigs
# ("Save all contigs" outputfile from consed)

$oSeqIOIn  =   Bio::SeqIO->new ('-file'    =>  $sInputSeqFn,
								'-format'  =>  'EMBL');

# create output object

$oSeqIOOut=Bio::SeqIO->new ('-file'   => ">$sOutputSeqFn",
			   			    '-format' => 'EMBL');
			    
#If requested generate single FASTA output file
if (defined $ssingleFASTA) {
	$oSeqIOSingleFasta = Bio::SeqIO->new ('-file'          => ">$ssingleFASTA",
										  '-format'        => "FASTA");
}

# For every Sequence in File...
while ($oSeq=$oSeqIOIn->next_seq) {

  # local variables
  my $iGeneCount=1;

  if ($bOnlyNew) {
    $iGeneCount = scalar (grep ( (($_->primary_tag eq 'CDS') &&
				  ($_->has_tag($sTag))),
				 $oSeq->top_SeqFeatures));

    $iGeneCount++; # We have to do this, as array numbering starts with 0
                   # and our gene numbering starts with 1
  }

  # get sequence id
  my $sSeqID=$oSeq->id;
  
  # if ID is unknown check accession...
  
  if ($sSeqID=~/.*unknown.*/) {
  	$sSeqID = $oSeq->accession;
  }
  $sSeqID =~ s/\s/_/g;
  $sSeqID =~ s/\;$//;
  
  # now cycle though every identified orf..
  foreach my $oFeature (grep ( (($_->primary_tag eq 'CDS') &&
  								(! $_->has_tag("pseudo"))),
			       $oSeq->top_SeqFeatures)) {

      # to allow subsequence export we have to link our feature
      # with the sequence
      $oFeature->attach_seq($oSeq);

      # Set filnames
      my $sBlastOutputFileName = "$sBlastPDir/$sSeqID.orf$iGeneCount";

      if ($oFeature->has_tag($sTag)) {

	# If only new files are to be processed goto next CDS-fetature,
	# as existing $sTag indicates that this feature has already
	# been processed
	if ($bOnlyNew) {
	  next;
	}
	else {

	  # If already a $sTag-tag is present remove it
	  $oFeature->remove_tag($sTag);
	}
      }

      # Set acutal file reference
      $oFeature->add_tag_value($sTag,$sBlastOutputFileName.".blastp");

      # Get DNA-sequence of feature
      my $oOrfDNASeq=$oFeature->spliced_seq;
      if ($oFeature->strand eq -1) {
	$oOrfDNASeq->revcom;
      }

      # Translate to protein sequence and add translation tag
      if ($oFeature->has_tag('translation')) {
	$oFeature->remove_tag('translation');
      }
      my $oOrfProteinSeq=$oOrfDNASeq->translate(undef,undef,undef,11);
      $oFeature->add_tag_value('translation',$oOrfProteinSeq->seq);

      # Write ORF-Faasta protein sequence
      $oOrfProteinSeq->id($sBlastOutputFileName);
      $oOrfProteinSeq->description(undef);
      my $oOrfProtIO=Bio::SeqIO->new('-file'    =>   ">$sOutputDirName$sBlastOutputFileName.fasta",
				     '-format'  =>   'FASTA');
      $oOrfProtIO->write_seq($oOrfProteinSeq);
      if (defined $ssingleFASTA) {
      	$oSeqIOSingleFasta->write_seq($oOrfProteinSeq);
      }

      # if blast execution is required by cmdline do it...
      if (defined $bRunBlast) {
		my $sBlastCmdLine = "-pblastp -d $BSEARCHDB -i $sOutputDirName$sBlastOutputFileName.fasta";
		$sBlastCmdLine   .= " -o $sOutputDirName$sBlastOutputFileName.blastp $sBlastParms";
		if ($bVerbose) {print "running blast on $sBlastOutputFileName\n" };
		`$BLASTCMD $sBlastCmdLine`;
		if ($^O ne "MSWin32") {
			`gzip -f $sBlastOutputFileName.blastp`;
		}
      }

      # Now we have to add the filled Up Feature to the original Seq-object
      $iGeneCount++;
    }

  # Include all Features to output file
  $oSeqIOOut->write_seq($oSeq);
}
