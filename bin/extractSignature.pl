#!/usr/bin/perl -w
#
use strict;
use Bio::SearchIO;
use CLUSEAN::ExtractSignature;

my $obj = Bio::SearchIO->new(-file => $ARGV[0],
                             -format => "hmmer2");

my @module_seqs = getModuleSequences($obj);

my @eightA_sigs = extract8Angstrom(@module_seqs);
#my @eightA_sigs = extractStachelhaus(@module_seqs);

open (OUT, ">", $ARGV[1]) || die "failed to open $ARGV[1]: $?";

foreach my $sig_ref (@eightA_sigs) {
    next if(length($$sig_ref[0]) > 34);
    print OUT $$sig_ref[0] . "\t" . $$sig_ref[1] . "\n";
}

close(OUT);
