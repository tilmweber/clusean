#!/usr/bin/perl
#
# Author: Tilmann Weber
# $Id: transferAnnotation.pl,v 1.2 2008/02/07 16:21:06 weber Exp $
#
#
#

=head1 NAME

transferAnnotation.pl

=head1 SYNOPSIS

transferAnnotation.pl --olddir <FullPathname> --newdir <Pathname> [--prefix <prefix> --verbose --debug --help]

=head1 DESCRIPTION

transferAnnotation.pl transfers annotation between different assembly-
versions. Herefor a temporary blast database is generated from old 
ORF protein sequences. All new ORFs are blasted against this database.
If a significant hit is observed, the annotation (currently /gene and 
/label tag) are transferred to the new sequence.
Important: Old Contig files need to match *.hmmann.ass.embl
           Each CDS feature of the old sequence has to have a /blastp_file
tag!

CAVEAT: This script is still experimental! A manual check of the results
is ESSENTIAL!!

=head1 OPTIONS

=head2 REQUIRED PARAMETERS:

  --olddir <FullPathname>    Directory containing the old annoation files
                             Full pathname requred (starting with / )

  --newdir <Pathname>        Directory containing the new Contigs

=head2 OPTIONAL PARAMETERS:

  --prefix <text>            Prefix to use for new filenames.
                             DEFAULT: tann

  --verbose                  Print status messages

  --debug                    Print even more messages

  --help                     Show this text


=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut


# Used libraries

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version# so we switch to bioperl live in my home directory

# use lib "/home/weber/perllib";


use strict;
use warnings;
use Bio::SeqIO;
use Bio::SearchIO;
use Getopt::Long;
use File::Temp qw(tempdir);
use Data::Dumper;


# Global Variables

my $BLASTCMD   = $ENV{BLASTCMD} || " nice -n 20 blastall";
my $BSEARCHDB  = $ENV{BSEARCHDB}|| "oldorfs";
# my $sBlastParms= $ENV{BPARMS}   || "-a2 -FF -v 2 -b 2"; # for 2 processor usage
my $sBlastParms= $ENV{BPARMS}   || "-FF -v 2 -b 2"; # for 1 processor usage

my $iMinID = 0.97;               # Identity threshold for assigning


my $iCleanTemp = 1;              # Flag to clean Tempdir 
                                 # after programm completion
my $fVerbose;                    # verbose flag
my $fHelp;                       # help flag
my $fDebug;                      # Debug flag

my $sNewEMBLdir;                 # Dir with new EMBL-files
my $sOldEMBLdir;                 # Dir with old EMBL-files
my $sAnnPrefix="tann";           # Filename prefix;
my $sTempDir;                    # TempDir

my %HashOfFeatures;              # Hash holding old Feature references

my @transferTags=qw(label gene); # Tags to be transferred to new sequence
# Subroutines

sub prepare;                     # generates&prepares Blast-database
sub evaluate;                    # check for identity in old and new Seqs


sub prepare 
  {
  open (SCRIPT, ">$sTempDir/makeblastdb");
  print SCRIPT "#!/bin/sh\n";
  print SCRIPT "cd $sTempDir\n";
  print SCRIPT "cat $sOldEMBLdir/blastp/*.fasta>oldorfs\n";
  print SCRIPT "formatdb -i oldorfs -pT -oF\n";
  close SCRIPT;
  chmod 0755, "$sTempDir/makeblastdb";
  system "$sTempDir/makeblastdb" || 
    die "Error generating blast database!";
  
  # Read old EMBL files
  opendir(OLDDIR, $sOldEMBLdir) || 
      die "Can't open $sOldEMBLdir for reading!\n";
  my @OldFiles = grep { /.*hmmann\.ass\.embl/ && -f "$sOldEMBLdir/$_" } readdir(OLDDIR);
  closedir OLDDIR;
  if ($fDebug) {
    print "Found files:",join (", ", @OldFiles),".\n";
  }
 
  foreach my $sOldfile (@OldFiles) {
    my $oSeqIO=Bio::SeqIO->new("-file"       => "$sOldEMBLdir/$sOldfile",
			       "-format"     => 'EMBL');

    while (my $oSeq=$oSeqIO->next_seq) {

      foreach my $oFeature (grep ( $_->primary_tag eq 'CDS',
				   $oSeq->top_SeqFeatures)) {
	if ($oFeature->has_tag('blastp_file'))
	  {
	    (my $sOrfName) = $oFeature->each_tag_value('blastp_file');
	    $sOrfName =~ s/\.*blastp\/*//g;
	    if ($fDebug) {
	      print "found Feature: $sOrfName.\n";
	    }
	    $HashOfFeatures{$sOrfName}=$oFeature;
	  }
      }
    }
  }

}


sub evaluate
  {
    # Open all EMBL-Files in directory and put the data into
    # the %newSeq hash structure with $oSeq->id as hash reference

    opendir(NEWDIR, $sNewEMBLdir) || 
      die "Can't open $sNewEMBLdir for reading!\n";
    my @NewFiles = grep { /.*\.embl/ && -f "$sNewEMBLdir/$_" } readdir(NEWDIR);
    closedir NEWDIR;

    if ($fDebug) {
      print "Found files:",join (", ", @NewFiles),".\n";
    }

    foreach my $sNewfile (@NewFiles) {
      my $oSeqIO=Bio::SeqIO->new("-file"       => "$sNewEMBLdir/$sNewfile",
			         "-format"     => 'EMBL');

      # prepare outputfiles
      my $sNewFileName = $sNewfile;
      $sNewFileName    =~ s/embl/$sAnnPrefix.embl/;
      my $oWriteSeq=Bio::SeqIO->new("-file"    => ">$sNewEMBLdir/$sNewFileName",
				    "-format"  => 'EMBL');

      while (my $oSeq=$oSeqIO->next_seq) {

       if ($fDebug) {
	 print "Identified Contig: ",$oSeq->id;
	 print " containing ",$oSeq->length," bp.\n";
       }

       my $iGeneCount=0;

       # get sequence id
       my $sSeqID=$oSeq->id;
       $sSeqID =~ s/\s/_/g;

       # now cycle though every identified orf..
       foreach my $oFeature (grep ( $_->primary_tag eq 'CDS',
			       $oSeq->top_SeqFeatures)) {

	 # Now we hae to increase the counter for the next gene
	 $iGeneCount++;
	 
	 # to allow subsequence export we have to link our feature
	 # with the sequence
	 $oFeature->attach_seq($oSeq);

	 # Set filnames
	 my $sBlastOutputFileName = "$sTempDir/$sSeqID.orf$iGeneCount";

	 # Get DNA-sequence of feature
	 my $oOrfDNASeq=$oFeature->seq;
	 if ($oFeature->strand eq -1) {
	   $oOrfDNASeq->revcom;
	 }

	 # Translate to protein sequence and add translation tag
	 if ($oFeature->has_tag('translation')) {
	   $oFeature->remove_tag('translation');
	 }
	 my $oOrfProteinSeq=$oOrfDNASeq->translate(undef,undef,undef,11);
	 $oFeature->add_tag_value('translation',$oOrfProteinSeq->seq);

	 # Write ORF-Faasta protein sequence
	 $oOrfProteinSeq->id($sBlastOutputFileName);
	 $oOrfProteinSeq->description(undef);
	 my $oOrfProtIO=Bio::SeqIO->new('-file'    =>
					">$sBlastOutputFileName.fasta",
					'-format'  =>   'FASTA');
	 $oOrfProtIO->write_seq($oOrfProteinSeq);

	 my $sBlastCmdLine = "-pblastp -d $sTempDir/oldorfs";
         $sBlastCmdLine   .= " -i $sBlastOutputFileName.fasta";
	 $sBlastCmdLine   .= " -o $sBlastOutputFileName.blastp $sBlastParms";
	 if ($fDebug) {
	   print "Running blast on $sBlastOutputFileName.\n";
	 }
	 `$BLASTCMD $sBlastCmdLine`;


	 # OK, now we have blasted our new gene against all "old" genes
	 # from the previous analysis
	 #
	 # Now we have to find out, whether the gene was present
	 # in the old sequences

	 # So first we have to load our blastreport

	 my $oBlastRepIO;               # Bio::SearchIO - Object
	 my $oBlastResult;              # Bio::Search::Result;

	 $oBlastRepIO = Bio::SearchIO->new("-format"  => "blast",
					  "-file"    => 
					   $sBlastOutputFileName.".blastp");
	 $oBlastResult = $oBlastRepIO->next_result();

	 # To see wheter gene is present we only have to look at the first hit

	 my $oBlastHit = $oBlastResult->next_hit;

	 # Bail out if no hit found
	 next if (! defined $oBlastHit);

	 my $oBlastHSP = $oBlastHit->next_hsp;

	 # Preliminary we only do a test on overall identity
	 # if old and new protein are >$iMinID % identical over HSP
         # both are regarded identical
	 
	 my $sHitName = $oBlastHit->name;
	 $sHitName =~ s#blastp/##;
	 $oBlastHit->name($sHitName);

	 if ($oBlastHSP->frac_identical > $iMinID) {
	   if ($fDebug) {
	     print "Found Hit with Id>$iMinID for $sSeqID.orf$iGeneCount:";
	     print "identical to ",$sHitName,".\n";

	   }
	   # Get old annotation
	   my $oOldFeature=$HashOfFeatures{$sHitName};

	   if (not defined $oOldFeature){
	     if ($fVerbose) {
	       print "No information available for $sHitName \n";
	       next;
	     }
	   }

#	   if ($fDebug) {
#	     print "Old Feature data structure:\n";
#	     print Dumper($oOldFeature);
#	   }

	   # transfer tags
	   foreach my $sTag (@transferTags) {
	     if ($oOldFeature->has_tag($sTag)) {
	       $oFeature->add_tag_value($sTag, 
			    join (", ",$oOldFeature->each_tag_value($sTag)));
	       if ($fVerbose) {
		 print "Transferring tag: $sTag with value: ";
		 print join (", ",$oOldFeature->each_tag_value($sTag));
		 print " from old $sHitName to new ",$oSeq->id, " ";
		 print $oFeature->start,"..",$oFeature->end," strand ";
		 print $oFeature->strand,".\n";
	       }
	     }
	   }
	   # add tag_transfer notice
	   $oFeature->add_tag_value('note',
			    "inherited tags: ".join (", ",@transferTags));
	 }
       }
       # write new sequence
       $oWriteSeq->write_seq($oSeq);
     }
    }
  }

    #if ($fDebug) {
    #  print Dumper(%
# MAIN
###############################################

# Get CmdLineArgs
GetOptions ('olddir|s=s'                   => \$sOldEMBLdir,
	    'newdir|n=s'                   => \$sNewEMBLdir,
	    'prefix|p=s'                   => \$sAnnPrefix,
	    'verbose|v'                    => \$fVerbose,
	    'debug|d'                      => \$fDebug,
	    'help|h'                       => \$fHelp);


if ($fHelp) {
  print "\n\n";
  eval {require Pod::Text};
  if ( @$ ) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }
  exit 0;
}

if ( !defined $sOldEMBLdir) {
  die "Error: Parameter -olddir must be present!\n";
}

if (! defined $sNewEMBLdir) {
  die "Error: Parameter -newdir must be present!\n";
}

if (! -e $sOldEMBLdir) {
  die "Error: OldEmbl directory $sOldEMBLdir not found!\n"
}

if (! -e $sNewEMBLdir) {
  die "Error: NewEmbl directory $sNewEMBLdir not found!\n"
}

if ($sOldEMBLdir eq $sNewEMBLdir) {
  die "Error: NewEmbl directory must be different from OldEmbl directory!\n"
}

#Create Tempdir

# to help debugging: Lets start the tempfile with date/time

my ($sek, $min, $std, $tag, $mon, $jahr, $wtag, $ytag, $isdst) = localtime;
$jahr += 1900;
$mon++;

# Create temporary directory below $sTempDirBase
# Cleanup after finishing
$sTempDir = tempdir ("transferEMBL_".$jahr.$mon.$tag.$std.$min.$sek."XXXX",
		     DIR      => "/tmp",
		     CLEANUP  => $iCleanTemp);



prepare;
evaluate;
#transfer;
