#!/usr/bin/perl
#

=head1 NAME

annotateNRPSPred.pl

=head1 SYNOPSIS

annotateNRPSPred.pl --input <input-EMBL-file> --output <output-EMBL-file> [--tag NRPSPred_file
--feattag CDS_motif  --csvout <CSV-filename> --score 0.1 --verbose --help]

=head1 DESCRIPTION

Enters data contained in  CDS associated HMMer result files into EMBL-Style annotation.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --tag <tag>                       :       Tag used for HMM-result-reference
                                             default: NRPSPred_file
   --feattag <tag>                   :       Tag used for feature annotation
                                             default: misc_feature
   --csvout			     :       Filename of CSV-outputfile
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# So let the code begin...

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version# so we switch to bioperl live in my home directory

# use lib "/home/weber/perllib";

use strict;
use Bio::SeqIO;
use Bio::SeqFeature::Generic;
use Getopt::Long;
use CLUSEAN::NRPSPredIO;
use File::Basename;
use Config::General;
use FindBin qw ($Bin);

# Global variables declaration

my $sInputSeqFn;     # Filename for InputFile
my $sOutputSeqFn;    # Filename for Outputfile

my $bVerbose;        # Verbose flag
my $bHelp;           # Help flag
my $sCSVFilename;    # Filename for CSV-Outputfile

my $oSeqIOIn;        # Input SeqIO-Object
my $oSeqIOOut;       # Output SeqIO-Object
my $oSeq;            # Bio::Seq main object

my $CLUSEANROOT;
if ( defined $ENV{CLUSEANROOT} ) {
    $CLUSEANROOT = $ENV{CLUSEANROOT};
}
else {
    $CLUSEANROOT = $Bin;
    $CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
}

# Parse Config file
my $oConfig =
  Config::General->new( '-ConfigFile' => "$CLUSEANROOT/etc/CLUSEAN.cfg" );
my %hConfig = $oConfig->getall;

my $sNRPSPredDir = $hConfig{NRPSPredictor}->{Dir};
my $sNRPSPredTag = $hConfig{NRPSPredictor}->{Tag};
my $sFeatureTag  = $hConfig{NRPSPredictor}->{NRPSPredQual} || "misc_feature";

# Evaluate Cmdline and assign variables

GetOptions(
    'input|i=s'   => \$sInputSeqFn,
    'output|o=s'  => \$sOutputSeqFn,
    'tag|h=s'     => \$sNRPSPredTag,
    'feattag|t=s' => \$sFeatureTag,
    'csvout|c=s'  => \$sCSVFilename,
    'verbose|v'   => \$bVerbose,
    'help|h'      => \$bHelp
);

# Check parameters

if ($bHelp) {
    print "\n\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
          "To view help, please install Pod::Text module or look directly\n";
        print "into the perl script $0\n\n";
    }
    else {
        my $oPodParser = Pod::Text->new();
        $oPodParser->parse_from_file($0);
    }

    #  print `pod2text $0`;
    exit 0;
}

if ( !-e $sInputSeqFn ) {
    die "EMBL Inputfile $sInputSeqFn not found";
}

my ( undef, $sInputDirName, undef ) = fileparse($sInputSeqFn);
if ($bVerbose) {
    print "Inputfile basedir is $sInputDirName \n";
}

$oSeqIOIn = Bio::SeqIO->new(
    '-file'   => $sInputSeqFn,
    '-format' => 'EMBL'
);

# create output object

$oSeqIOOut = Bio::SeqIO->new(
    '-file'   => ">$sOutputSeqFn",
    '-format' => 'EMBL'
);

if ( defined $sCSVFilename ) {
    open( CSVFILE, ">$sCSVFilename" )
      || die "Can't open CSV file $sCSVFilename for writing!\n";
}

# For every Sequence in File (normally only one sequence per file...)
while ( $oSeq = $oSeqIOIn->next_seq ) {

    # now cycle though every identified orf..
    foreach
      my $oFeature ( grep ( $_->primary_tag eq 'CDS', $oSeq->top_SeqFeatures ) )
    {

        # If it has an associated HMMer-reference use it...
        if ( $oFeature->has_tag($sNRPSPredTag) ) {
            ( my $sNRPSPredFilename ) =
              $oFeature->get_tag_values($sNRPSPredTag);
            $sNRPSPredFilename = $sInputDirName . $sNRPSPredFilename;

            # If associated file cannot be found - bail out...
            if ( !-e $sNRPSPredFilename ) {
                die
"Error: NRPSPredictor report: $sNRPSPredFilename not found! Bailing out...\n";
            }

            # Output filename on verbose
            if ($bVerbose) {
                print "processing $sNRPSPredFilename\n";
            }

            # Open NRPSPred-Report
            my $oNRPSPredReport =
              CLUSEAN::NRPSPredIO->new( '-file' => $sNRPSPredFilename );

            # Cycle through results
            while ( my $oNRPSPredResult = $oNRPSPredReport->next_result ) {

                if ($bVerbose) {
                    print "processing $sNRPSPredFilename\n";
                }

                next if ( !$oNRPSPredResult->has_Hit );

                my $sCSVLine;

                # If a CSV file has to be generated assemble output

                # Get gene name or alternatively locus name
                my $sGeneName;
                if ( $oFeature->has_tag("gene") ) {
                    ($sGeneName) = $oFeature->get_tag_values("gene");
                }
                elsif ( $oFeature->has_tag('locus_tag') ) {
                    ($sGeneName) = $oFeature->get_tag_values('locus_tag');
                }
                else {
                    $sGeneName = "NoGeneName";
                }

                $sCSVLine = $sGeneName . "," . $sNRPSPredFilename . ",";
                $sCSVLine .=
                    $oFeature->start . ","
                  . $oFeature->end . ","
                  . $oFeature->strand . ",";

                # calculate start/stop nucleotides out of protein coordinates
                my ( $iFtStart, $iFtEnd );
                if ( $oFeature->strand eq 1 ) {
                    $iFtStart = $oFeature->start +
                      ( 3 * $oNRPSPredResult->get_start ) - 3;
                    $iFtEnd =
                      $oFeature->start + ( 3 * $oNRPSPredResult->get_end ) - 1;
                }
                else {
                    $iFtEnd =
                      $oFeature->end - ( 3 * $oNRPSPredResult->get_start ) + 3;
                    $iFtStart =
                      $oFeature->end - ( 3 * $oNRPSPredResult->get_end ) + 1;
                }

                # CSV -> add protein coordinates to output

                $sCSVLine .= $iFtStart . "," . $iFtEnd . ",";

                # Setup new sequence-feature
                my $oNRPSPredFeature = Bio::SeqFeature::Generic->new(
                    '-primary_tag' => $sFeatureTag,
                    '-strand'      => $oFeature->strand,
                    '-start'       => $iFtStart,
                    '-end'         => $iFtEnd
                );

                my $sNRPSPredDesc =
                  "NRPSPredictor results for A-domain core spanning aa ";
                $sNRPSPredDesc .= $oNRPSPredResult->get_start;
                $sNRPSPredDesc .= "..";
                $sNRPSPredDesc .= $oNRPSPredResult->get_end;
                $oNRPSPredFeature->add_tag_value( 'note', $sNRPSPredDesc );

                $sNRPSPredDesc =
                  "Amino acid signature in 8A distance around active site: ";
                $sNRPSPredDesc .= $oNRPSPredResult->get_eightAA;
                $oNRPSPredFeature->add_tag_value( 'note', $sNRPSPredDesc );

                $sNRPSPredDesc = "Amino acid signature of Stachelhaus code: ";
                $sNRPSPredDesc .= $oNRPSPredResult->get_StachAA;
                $oNRPSPredFeature->add_tag_value( 'note', $sNRPSPredDesc );

                $sCSVLine .= $oNRPSPredResult->get_start . ","
                  . $oNRPSPredResult->get_end . ",";
                $sCSVLine .= $oNRPSPredResult->get_eightAA . ","
                  . $oNRPSPredResult->get_StachAA . ",";

                my @aLCHits;    # Help variable to assemble LC-Hit-String

                for (
                    my $i ;
                    $i < scalar @{ $oNRPSPredResult->get_large_clustArray } ;
                    $i++
                  )
                {
                    my $sLCHit;    # Help variable to assemble LC-Hit-String
                    $sNRPSPredDesc = "Hit against large clusters: ";
                    if ( $oNRPSPredResult->get_large_clustArray->[$i] =~
                        /No predictions.*/ )
                    {
                        $oNRPSPredFeature->add_tag_value( 'note',
                            $sNRPSPredDesc . 'None.' );
                        $sLCHit = "no LargeCluster Hit";
                    }
                    else {
                        $sNRPSPredDesc .=
                          $oNRPSPredResult->get_large_clustArray->[$i];
                        $sLCHit =
                          $oNRPSPredResult->get_large_clustArray->[$i] . ";"
                          . "Score: ";
                        $sNRPSPredDesc .= "; Score: ";
                        $sNRPSPredDesc .= sprintf( '%.3f',
                            $oNRPSPredResult->get_lc_scoreArray->[$i] );
                        $sLCHit .= sprintf( '%.3f',
                            $oNRPSPredResult->get_lc_scoreArray->[$i] );
                        $oNRPSPredFeature->add_tag_value( 'note',
                            $sNRPSPredDesc );
                    }
                    push( @aLCHits, $sLCHit );
                }
                $sCSVLine .= join( "/", @aLCHits ) . ",";

                my @aSCHits;    # Help variable to assemble CSV line
                for (
                    my $i ;
                    $i < scalar @{ $oNRPSPredResult->get_small_clustArray } ;
                    $i++
                  )
                {
                    my $sSCHit;
                    $sNRPSPredDesc = "Hit against small clusters: ";
                    if ( $oNRPSPredResult->get_small_clustArray->[$i] =~
                        /No predictions.*/ )
                    {
                        $oNRPSPredFeature->add_tag_value( 'note',
                            $sNRPSPredDesc . 'None.' );
                        $sSCHit = "no SmallCluster Hit";
                    }
                    else {
                        $sNRPSPredDesc .=
                          $oNRPSPredResult->get_small_clustArray->[$i];
                        $sSCHit =
                          $oNRPSPredResult->get_small_clustArray->[$i] . ";"
                          . "Score: ";
                        $sNRPSPredDesc .= "; Score: ";
                        $sNRPSPredDesc .= sprintf( '%.3f',
                            $oNRPSPredResult->get_sc_scoreArray->[$i] );
                        $sSCHit .= sprintf( '%.3f',
                            $oNRPSPredResult->get_sc_scoreArray->[$i] );
                        $oNRPSPredFeature->add_tag_value( 'note',
                            $sNRPSPredDesc );
                    }
                    push( @aSCHits, $sSCHit );
                }
                $sCSVLine .= join( "/", @aSCHits ) . ",";

                if ( $oNRPSPredResult->get_StachPred ) {
                    $sNRPSPredDesc = 'Hits against Stachelhaus signatures: ';

                    # Add statistics if there is a score...
                    if ( $oNRPSPredResult->get_StachPred =~ /^No matches.*/ ) {
                        $sNRPSPredDesc .= "None.";
                        $sCSVLine .= "No match against Stachelhaus signatures";
                    }
                    else {
                        $sNRPSPredDesc .= $oNRPSPredResult->get_StachPred;
                        $sNRPSPredDesc .= "; ";
                        $sNRPSPredDesc .= $oNRPSPredResult->get_Stach_ident;
                        $sNRPSPredDesc .= "% identity; score: ";
                        $sNRPSPredDesc .=
                          $oNRPSPredResult->get_Stach_score . ".";
                        $sCSVLine .= $oNRPSPredResult->get_StachPred . " Id: "
                          . $oNRPSPredResult->get_Stach_ident;
                        $sCSVLine .=
                          "% Score: " . $oNRPSPredResult->get_Stach_score;
                    }
                    $oNRPSPredFeature->add_tag_value( 'note', $sNRPSPredDesc );
                }
                else {
                    $sCSVLine .= "No match against Stachelhaus signatures";
                }

                my $cds_tag_name = "";
                my @tags = $oFeature->get_tag_values('locus_tag');
                $cds_tag_name .= $tags[0];
                $oNRPSPredFeature->add_tag_value( 'locus_tag', "$cds_tag_name" );

                # ...and add it to sequence object
                $oSeq->add_SeqFeature($oNRPSPredFeature);
                if ( defined $sCSVFilename ) {
                    print CSVFILE $sCSVLine, "\n";
                }
            }
        }
    }

    # finally write sequence
    $oSeqIOOut->write_seq($oSeq);
}
