#!/usr/bin/perl
#
# Based on original script getFeatSubset v 1.2 (2004)
#
# This script extracts features and Offsets of features from EMBL-Files
# and returns subsequences in FASTA format
#
#
# Usage:
#  getFeatSubset.pl --input Contig21.bls.hmmann.embl --tagvalue "Ketoacyl-synt_C"
#    --prefix "KS" --offset "10..100"
#
# In this example, Sequences that are assigned a CDS-motif tag and an a motif spanning aa 10 to 100
# of the feature
#
# options for offset:
#

=head1 NAME

getFeatures.pl

=head1 DESCRIPTION

Extract protein-sequence of specific feature (tag) of EMBL-File

=head1 USAGE

getFeatures.pl --input <input-EMBL-file> --tagvalue <string to be scanned for>
[--feature <feature name to be scanned for> --tag <tag name to be scanned for> 
--offset [+-]xx..yy --prefix <prefix-string> --help]

=head2 PARAMETER description

    --input <filename>       : Name of the embl-file to process
	
    --tagvalue <string>
    --feature <string>
    --tag <string>           : Extract features of type "--feature" which have a qualifier "--tag"
                               which has the value "--tagvalue"
 						   
      Defaults: --feature    : CDS_motif
 		         --tag        : label
 							            
	--minaa					 : Minimal size of feature to extract
    --offset xx..yy          : Offset of amino acids to extract within features
 	
 		no parameter specified --> extract whole feature
 		1..100                 --> extract absolute position 1..100 from feature
		
    --aa-before xx           : include xxx amino acids before feature
    --aa-behind xx           : include xxx amino acids behind feature			   	 
    --prefix <string>        : String to prepend on fasta output header
    
    --help

=head1 REQUIREMENTS

This script requires the following perl-modules installed on your system:

 Bioperl (Bio::SeqIO, Bio::SeqFeature::Generic)
 Getopt::Long


=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle28
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use Bio::SeqIO;
use Bio::SeqFeature::Generic;
use Getopt::Long;

my %hParams;
$hParams{tag}     = "label";
$hParams{feature} = "CDS_motif";

GetOptions(
	\%hParams,       'input|i=s',   'tag|n=s',    'tagvalue|v=s',
	'prefix|d=s',    'minaa|m=s',   'offset|o=s', 'aa-before|b=s',
	'aa-behind|a=s', 'feature|f=s', 'help|h'
);

if ( $hParams{help} ) {
	print "\n\n";
	eval { require Pod::Text };
	if (@$) {
		print
		  "Error formatting help content! Perl-Module Pod::Text required!\n";
		print
"To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else {
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}
	exit 0;
}

if ( !-e $hParams{input} ) {
	print STDERR "Inputfile $hParams{input} not found\n\n";
	exit 1;
}

my $oSeqIO = Bio::SeqIO->new( '-file' => $hParams{input} );

my $oSeqIOout = Bio::SeqIO->new( '-format' => 'FASTA' );

while ( my $oSeq = $oSeqIO->next_seq ) {
	my $i = 0;

	foreach
	  my $oCDSFeat ( grep( $_->primary_tag eq 'CDS', $oSeq->top_SeqFeatures ) )
	{
		$i++;
		my $j = 0;
		foreach my $oCDS_motifFeat (
			grep( $_->primary_tag eq $hParams{feature}, $oSeq->all_SeqFeatures )
		  )
		{
			next if ( !$oCDS_motifFeat->overlaps($oCDSFeat) );
			next
			  if (
				$oCDS_motifFeat->spliced_seq->length <= $hParams{minaa} * 3 );
			my ( $FStart, $FEnd );
			if ( $oCDS_motifFeat->has_tag( $hParams{tag} ) ) {
				foreach my $sTagValue (
					$oCDS_motifFeat->get_tag_values( $hParams{tag} ) )
				{
					if ( $sTagValue =~ /$hParams{tagvalue}/ ) {
						$j++;
						$FStart = $oCDS_motifFeat->start;
						$FEnd   = $oCDS_motifFeat->end;

						# print "Original Feature coordinates", $FStart, "..",
						# $FEnd, " strand ", $oCDS_motifFeat->strand, "\n";

						# calculate offset if cmdline parameter is given
						if ( $hParams{offset} =~ /^(\d+)\.\.(\d+)/ ) {
							my ( $iOffsetStart, $iOffsetEnd ) =
							  ( ( $1 * 3 ), ( $2 * 3 ) );
							if ( $oCDS_motifFeat->strand eq 1 ) {
								if ( $FStart + $iOffsetStart <
									$oCDS_motifFeat->end )
								{
									$FStart = $FStart + $iOffsetStart;
									$FStart = $oCDSFeat->start;
								}
								else {
									die
"ERROR: offset not within feature coordinates\n";
								}
								if ( $FStart + $iOffsetEnd <=
									$oCDS_motifFeat->end )
								{
									$FEnd =
									  $FStart + $iOffsetEnd - $iOffsetStart;
									  $oCDS_motifFeat->start($FStart)
								}
								else {
									die
"ERROR: offset not within feature coordinates\n";
								}
							}
							else {
								if ( $FEnd - $iOffsetEnd >
									$oCDS_motifFeat->start )
								{
									$FEnd = $FEnd - $iOffsetStart;
									$oCDS_motifFeat->end($FEnd)
								}
								else {
									die "ERROR: offset not within feature coordinates\n";
								}
								if ( $FEnd - $iOffsetStart <=
									$oCDS_motifFeat->end )
								{
									$FStart = $FEnd - $iOffsetStart;
									$FStart = $oCDSFeat->start;
								}
								else {
									die "ERROR: offset not within feature coordinates\n";
								}
							}
						}

						# calculate new starting point if cmdline parameter aa-before is given
						if ( defined $hParams{'aa-before'} ) {
							if ( $oCDS_motifFeat->strand eq 1 ) {
								$FStart =
								  $FStart - ( $hParams{'aa-before'} * 3 );
								if ( $FStart < $oCDSFeat->start ) {
									$FStart = $oCDSFeat->start;
								}
								$oCDS_motifFeat->start($FStart);
							}
							else {
								$FEnd = $FEnd + ( $hParams{'aa-before'} * 3 );
								if ( $FEnd > $oCDSFeat->end ) {
									$FEnd = $oCDSFeat->end;
								}
								$oCDS_motifFeat->end($FEnd);
							}
						}

						# calculate new starting point if cmdline parameter aa-behind is given
						if ( defined $hParams{'aa-behind'} ) {
							if ( $oCDS_motifFeat->strand eq 1 ) {
								$FEnd = $FEnd + ( $hParams{'aa-behind'} * 3 );
								if ( $FEnd > $oCDSFeat->end ) {
									$FEnd = $oCDSFeat->end - 3;
								}
								$oCDS_motifFeat->end($FEnd);
							}
							else {
								$FStart =
								  $FStart - ( $hParams{'aa-behind'} * 3 );
								if ( $FStart < $oCDSFeat->start ) {
									$FStart = $oCDSFeat->start;
								}
								$oCDS_motifFeat->start($FStart);
							}

						}

						# print "Modified Feature coordinates", $FStart, "..", $FEnd, " strand ", $oCDS_motifFeat->strand, "\n";

						# Extract subsequence and write as fasta file

						my $oProtSeq =
						  $oCDS_motifFeat->spliced_seq->translate( undef, undef,
							undef, 11 );
						my $sFastaDesc;
						my $sFastaID;
						if ( $oCDSFeat->has_tag('gene') ) {
							($sFastaID) = $oCDSFeat->get_tag_values('gene');
						}
						elsif ( $oCDSFeat->has_tag('locus_tag') ) {
							($sFastaID) =
							  $oCDSFeat->get_tag_values('locus_tag');
						}
						else {
							$sFastaID = "undef";
						}
						$sFastaID .= $hParams{prefix} . $j;
						$sFastaDesc = $oSeq->id;
						$sFastaDesc =~ s/\;$//;
						$sFastaDesc .=
						  "|orf" . $i . "_" . $FStart . ".." . $FEnd . "|";
						$sFastaDesc .= $hParams{tagvalue} . "." . $j;
						$oProtSeq->id($sFastaID);
						$oProtSeq->desc($sFastaDesc);
						$oSeqIOout->write_seq($oProtSeq);
					}
				}
			}
		}
	}
}
