#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program runs NRPSPredictor analysis against CDS-Features which were assigned NRPS
#
# InputFiles: EMBL-Format-File, processed with CLUSEAN pipeline (assignFunction.pl)
#
# Output: EMBL-File with blastp_file tags
#         HMMer-Reports of identified genes
#
#


=head1 NAME

NRPSPredEMBL.pl

=head1 SYNOPSIS

NRPSPredEMBL.pl --input <input-EMBL-file> --output <output-EMBL-file>
[--tag <tag used for annotation> --onlynew --verbose --help --run]

=head1 DESCRIPTION

Prepares and optionally runs analysis with NRPSPredictor

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --annotation <filename>           :       call annotateNRPSPred.pl script after
                                             completing the NRPSPredictor tool
                                             required parameter: Filename
   --tag <tag>                       :       Tag used for NRPSPredictor-reference
                                             default: NRPSPred_file
   --NRPSPDir						 :       Directory to store outputfiles
                                             default NRPSPred                                         
   --run                             :       Start NRPSPredictor from within script
  
   --onlynew                         :       Only process CDS features with no
                                             previous NRPSPred-results
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

# as the EMBL format changed mid 2006 we need to have a post Bioperl 1.4 version# so we switch to bioperl live in my home directory

# use lib "/home/weber/perllib";

use strict;
use Bio::SeqIO;
use Bio::SeqFeature::Generic;
use Getopt::Long;
use Config::General;
use FindBin qw ($Bin);
use File::Basename;



my $sInputSeqFn;                 # Filename for InputFile
my $sOutputSeqFn;                # Filename for Outputfile
my $sAnnoOutFn;                  # Filename for annotation output file
my $bRun     ;                   # Run NRPSPredictor from script?

my $bOnlyNew;                    # process only new CDS with no
                                 # previous results
my $bVerbose;                    # Verbose flag
my $bHelp;                       # Help flag

my $oSeqIOIn;                    # Input SeqIO-Object
my $oSeqIOOut;                   # Output SeqIO-Object
my $oSeq;                        # Bio::Seq main object

#Import Configuration data

	my $sCLUSEANROOT;
	if (defined $ENV{CLUSEANROOT}) {
		$sCLUSEANROOT = $ENV{CLUSEANROOT};
	}
	else {
		$sCLUSEANROOT = $Bin;
		$sCLUSEANROOT =~ s/(.*)\/.*?$/$1/;
	
	}

# Parse Config file
	my $oConfig = Config::General->new ('-ConfigFile'     =>  "$sCLUSEANROOT/etc/CLUSEAN.cfg");
	my %hConfig = $oConfig->getall;

my $sNRPSPredCMD  = $ENV{NRPSPredCMD}   || $hConfig{NRPSPredictor}->{Exe} || "$sCLUSEANROOT/bin/NRPSPredWrap.pl";
my $sNRPSPredDir  = $hConfig{NRPSPredictor}->{Dir};      
my $sTag        = $hConfig{NRPSPredictor}->{Tag};
my $sAnnoTag    = $hConfig{NRPSPredictor}->{NRPSLabel};

# Evaluate Cmdline and assign variables

GetOptions ( 'input|i=s'               =>     \$sInputSeqFn,
		     'output|o=s'              =>     \$sOutputSeqFn,
		     'annotation|a=s'          =>     \$sAnnoOutFn,
		     'run|r'                   =>     \$bRun,
	     	 'tag|t=s'                  =>     \$sTag,
	     	 'NRPSPDir|d=s'             =>     \$sNRPSPredDir,
	     	 'onlynew|n'                =>     \$bOnlyNew,
	     	 'verbose|v'                =>     \$bVerbose,
         	 'help|h'                   =>     \$bHelp);

# Check parameters


if ($bHelp) {
  print "\n\n";
  eval {require Pod::Text};
  if ( @$ ) {
    print "Error formatting help content! Perl-Module Pod::Text required!\n";
    print "To view help, please install Pod::Text module or look directly into\n";
    print "the perl script $0\n\n";
  }
  else {
    my $oPodParser = Pod::Text->new ();
    $oPodParser->parse_from_file($0);
  }
  exit 0;
}

if (! -e $sInputSeqFn) {
    die "EMBL Inputfile $sInputSeqFn not found";
}
my (undef, $sInputDirName, undef) = fileparse($sInputSeqFn);
if ($bVerbose) {
	print "Inputfile basedir is $sInputDirName \n";
}

# If directory does not exist create it
if (! -e $sInputDirName.$sNRPSPredDir) {

  mkdir $sInputDirName.$sNRPSPredDir || die "Can't create $sInputDirName.$sNRPSPredDir\n";
}


# First we have to load the annotated EMBL-file


$oSeqIOIn  =   Bio::SeqIO->new ('-file'    =>  $sInputSeqFn,
			                 	'-format'  =>  'EMBL');

# create output object

$oSeqIOOut=Bio::SeqIO->new ('-file'   => ">$sOutputSeqFn",
			                '-format' => 'EMBL');

# For every Sequence in File...
while ($oSeq=$oSeqIOIn->next_seq) {

  # local variables
  my $iGeneCount=1;

  if ($bOnlyNew) {
    $iGeneCount = scalar (grep ( ( ($_->primary_tag eq 'CDS') &&
				  ($_->has_tag($sTag) ) ),
				 $oSeq->top_SeqFeatures));

    $iGeneCount++; # We have to do this, as array numbering starts with 0
                   # and our gene numbering starts with 1
  }

  # get sequence id
  my $sSeqID=$oSeq->id;
  
  # if ID is unknown check accession...
  
  if ($sSeqID=~/.*unknown.*/) {
  	$sSeqID = $oSeq->accession;
  }
  $sSeqID =~ s/\s/_/g;
  $sSeqID =~ s/\;$//;
  
  # now cycle though every orf that was assigned NRPS..
  
  # In a later stage we might want to make this a little bit more sensitive
  # and directly check for the presence of A-Domains 
  foreach my $oFeature (grep ( ( ($_->primary_tag eq 'CDS') &&
  								 ($_->has_tag($sAnnoTag))),
			                      $oSeq->top_SeqFeatures)) {
	
	  # Skip CDS if it is not annotated as NRPS or PKSI
	  my ($sAssFunc) = $oFeature->get_tag_values($sAnnoTag);
	  chomp ($sAssFunc);
	  
	  next if (($sAssFunc ne 'NRPS') && 
	  		   ($sAssFunc ne 'PKSI') &&
	  		   ($sAssFunc ne 'ligase'));
	  	
	  if ($bVerbose) {
	  		print "processing gene $iGeneCount with label $sAssFunc \n";
	  }
      # to allow subsequence export we have to link our feature
      # with the sequence
      $oFeature->attach_seq($oSeq);

      # Set filnames
      my $sFilename = $sSeqID.".nrps".$iGeneCount;
      my $sNRPSPOutputFileName = "$sNRPSPredDir/$sFilename";

      if ($oFeature->has_tag($sTag)) {

		# If only new files are to be processed goto next CDS-fetature,
		# as existing $sTag indicates that this feature has already
		# been processed
		if ($bOnlyNew) {
		  next;
		}	
		else {

	  	  # If already a $sTag-tag is present remove it
	  	  $oFeature->remove_tag($sTag);
		}	
      }

      # Set acutal file reference
      $oFeature->add_tag_value($sTag,$sNRPSPOutputFileName.".out");

      # Get DNA-sequence of feature
      my $oOrfDNASeq=$oFeature->seq;
      if ($oFeature->strand eq -1) {
		$oOrfDNASeq->revcom;
      }

      # Translate to protein sequence and add translation tag
      if ($oFeature->has_tag('translation')) {
		$oFeature->remove_tag('translation');
      }
      my $oOrfProteinSeq=$oOrfDNASeq->translate(undef,undef,undef,11);
      $oFeature->add_tag_value('translation',$oOrfProteinSeq->seq);

      if (!-e "$sInputDirName$sNRPSPOutputFileName.fasta") {

		# Write ORF-Fasta protein sequence
		
		$oOrfProteinSeq->id($sFilename);
		$oOrfProteinSeq->description(undef);
		my $oOrfProtIO=Bio::SeqIO->new(	'-file'    =>   ">$sInputDirName$sNRPSPOutputFileName.fasta",
				       					'-format'  =>   'FASTA');
		$oOrfProtIO->write_seq($oOrfProteinSeq);
      	}

      # if blast execution is required by cmdline do it...
      if (defined $bRun) {
	my $sNRPSPredCmdLine = "$sNRPSPredCMD --input $sInputDirName$sNRPSPOutputFileName.fasta > $sInputDirName$sNRPSPOutputFileName.out";
	if ($bVerbose) {print "running NRPSPredictor on $sNRPSPOutputFileName.fasta\n" };
	`$sNRPSPredCmdLine`;
      }

      $iGeneCount++;
    }

  # Include all Features to output file
  $oSeqIOOut->write_seq($oSeq);
}

if (defined $sAnnoOutFn) {
	`annotateNRPSPred.pl --input $sOutputSeqFn --output $sAnnoOutFn`;
}