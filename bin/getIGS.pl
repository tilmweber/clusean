#!/usr/bin/perl

=head1 NAME

getIGS.pl

=head1 SYNOPSIS

getIGS.pl --input <input-EMBL-file> [ --minlength nn --trunclength nn --upstreambases
                                       --withnotes --verbose --help ]

=head1 DESCRIPTION

Extracts intergenic sequences from bacterial genome sequences and outputs to STDOUT

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   
   --minlength nn                    :       Minimal length of intergenic sequence
                                             (default 50 nt)
   --trunclength nn				     :       Truncate sequence at nn bases before start codon
   											 (default unset)
   --upstreambases nn                :       Return nn upstream bases of each CDS (irrespective
   											 wheter thea are real IGS and minlength setting)     											 
   --usegenenames					 :       Use gene names as fasta identifiers
   											 (may be problematic with some annotations, when
   											 gene names were assigned non uniquely for different genes)
   --withnotes                       :       Include content of EMBL-Note qualifiers in
                                             FASTA description line                                          
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use Getopt::Long;
use Bio::SeqIO;
use Bio::Seq;
use CLUSEAN::GetIGS;
use Data::Dumper;

# Define Variables
my $sInputSeqFn;
my $iMinLength = 50;
my $iUpstreamBases;
my $iTruncLength;
my $bWithNoteTags;
my $bUseGeneNames;
my $bVerbose;
my $bHelp;

# Evaluate Cmdline and assign variables
GetOptions(
			'input|i=s'          => \$sInputSeqFn,
			'minlength|m=s'      => \$iMinLength,
			'upstreambases|b=s'  => \$iUpstreamBases,
			'usegenenames|u'     => \$bUseGeneNames,
			'trunclength|t=s'    => \$iTruncLength,
			'withnotes|n'        => \$bWithNoteTags,
			'verbose|v'          => \$bVerbose,
			'help|h'             => \$bHelp
);

# Check parameters
if ( ($bHelp) || ( $sInputSeqFn eq "" ) )
{
	print "\n\n";
	eval { require Pod::Text };
	if (@$)
	{
		print
		  "Error formatting help content! Perl-Module Pod::Text required!\n";
		print
          "To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else
	{
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}

	#  print `pod2text $0`;
	exit 0;
}
if ( !-e $sInputSeqFn )
{
	die "Inputfile $sInputSeqFn not found";
}

# Prepare input and output Bio::SeqIO objects
my $oSeqIO = Bio::SeqIO->new( '-file'   => $sInputSeqFn,
							  '-format' => 'EMBL' );
my $oOutSeqIO = Bio::SeqIO->new( '-format' => 'FASTA' );
while ( my $oSeq = $oSeqIO->next_seq )
{

	# get CDS features
	my @aFeatures =
	  ( grep ( ( $_->primary_tag eq 'CDS' ), $oSeq->top_SeqFeatures ) );

	# set up GetIGS object
	my $oIGS = CLUSEAN::GetIGS->new(
									 '-aoFeatures'     => \@aFeatures,
									 '-SeqObj'         => \$oSeq,
									 '-truncLength'    => $iTruncLength,
									 '-upstreamBases'  => $iUpstreamBases
	);

	# for every IGS sequence (stored in a Bio::SeqFeature::Generic object...
	while ( my $oIGSFeature = $oIGS->nextIGS() )
	{

		# if upstremBases parameter is unset, skip if IGS is smaller than -minlength
		next if ( (! defined $iUpstreamBases) &&
		          ( $oIGSFeature->end - $oIGSFeature->start < $iMinLength ) );
		my $Gene;
		my $Locus;
		my $Product;
		my $Notes;
		my $Label;

		# get annotation of GetIGS object
		my $IGSid = "IGS_"
		  . $oIGSFeature->start . ".."
		  . $oIGSFeature->end . "/"
		  . $oIGSFeature->strand;
		if ( $oIGSFeature->has_tag('gene') )
		{
			$Gene = join( ",", $oIGSFeature->get_tag_values('gene') );
		}
		if ( $oIGSFeature->has_tag('locus_tag') )
		{
			$Locus = join( ",", $oIGSFeature->get_tag_values('locus_tag') );
		}
		if ( $oIGSFeature->has_tag('product') )
		{
			$Product = join( ",", $oIGSFeature->get_tag_values('product') );
		}
		if ( $oIGSFeature->has_tag('note') )
		{
			$Notes = join( ",", $oIGSFeature->get_tag_values('note') );
		}
		if ( $oIGSFeature->has_tag('label') )
		{
			$Label = join( ",", $oIGSFeature->get_tag_values('label') );
		}
		my $ID;
		if ( defined $Gene )
		{
			$ID = $Gene;
		}
		elsif ( defined $Locus )
		{
			$ID = $Locus;
		}
		else
		{
			$ID = $IGSid;
		}

		# Debug output
		if ($bVerbose)
		{
			if ( defined $Gene )
			{
				print $Gene;
			}
			else
			{
				print "undef\t";
			}
			print $oIGSFeature->start, "\t", $oIGSFeature->end, "\t";
			print $oIGSFeature->strand, "\t", $Product, "\t", $Notes, "\t",
			  $oIGSFeature->seq->seq, "\n";
		}
		my $IGSdesc;
		if ($bUseGeneNames) {
			$IGSdesc = $IGSid;
			$IGSid=$Gene;
		}
		else {
			$IGSdesc = $Gene;
		}
		if ( defined $Locus )
		{
			$IGSdesc .= " Locus: " . $Locus . " |";
		}
		
		if ( defined $Label )
		{
			$IGSdesc .= " " . $Label . " |";
		}
		if ( defined $Product )
		{
			$IGSdesc .= " " . $Product . " |";
		}
		if ( ( defined $Notes ) && ( defined $bWithNoteTags ) )
		{
			$IGSdesc .= " " . $Notes;
		}
		
  # Assemble new Bio::Seq object containing the IGS sequence and definition line
		my $oOutSeq = Bio::Seq->new(
									 '-id'          => $IGSid,
									 '-description' => $IGSdesc,
									 '-seq'         => $oIGSFeature->seq->seq
		);

		# and add it to oOutSeqIO output object
		$oOutSeqIO->write_seq($oOutSeq);
	}
}
