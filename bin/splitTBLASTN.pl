#!/usr/bin/perl

use strict;

my @OutputBlastBuffer;
my $Filename = "NOFILENAME";   # set default filename, has to be replaced later on!

open (INFILE, $ARGV[0]) || die "Fatal error: Can't open $ARGV[0]!";

@OutputBlastBuffer = ();   # empty array

my $readln=<INFILE>;
die "Sorry, $ARGV[0] is no BLAST output file" if (! $readln =~ /^BLAST.*/);

while ($readln=<INFILE>) {
	    # Determine filename by parsing Query= statement
		if ($readln =~ /^Query=\s(.*)$/ ) {
			$Filename = $1;

			$Filename =~ s/blastp\/(.*)/$1/;
			# print "processing $Filename \n";
			chomp ($Filename);
		}
		
		# This is only true if we arrive at the report for the next sequence
		# so we can write the last report...
		
		if ($readln =~ /^T*BLAST.*/) {
			open (OUTFILE, ">$Filename.blastp");
			print OUTFILE @OutputBlastBuffer;
			close (OUTFILE);
			@OutputBlastBuffer = ();
		}
	push (@OutputBlastBuffer, $readln);  # put line on stack
		
	
}
# Now we finally have to write the last report...
open (OUTFILE, ">$Filename.blastp");
print OUTFILE @OutputBlastBuffer;
close (OUTFILE);