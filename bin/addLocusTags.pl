#!/usr/bin/perl -w

=head1 NAME

addLocusTags.pl

=head1 SYNOPSIS

addLocusTags.pl --input <input-EMBL-file> --output <output-EMBL-file> [--verbose
                --help --prefix <locus_tag prefix> --num-chars <number> ]

=head1 DESCRIPTION

Makes sure that every CDS feature has a unique locus_tag property.

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --prefix <locus_tag prefix>       :       locus_tag prefix (def: AUTOORF_)
   --num-chars <number>              :       # of character for locus_tag index
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Kai Blin
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: kai.blin@biotech.uni-tuebingen.de

=cut

use strict;
use Getopt::Long;
use Bio::SeqIO;

my $in_seq_file;
my $out_seq_file;
my $prefix = "AUTOORF_";
my $num_chars = 5;
my $verbose;
my $help;

GetOptions(
    'input|i=s'    => \$in_seq_file,
    'output|o=s'   => \$out_seq_file,
    'prefix|p=s'    => \$prefix,
    'num-chars|n=i' => \$num_chars,
    'verbose|v'    => \$verbose,
    'help|h'       => \$help
);

if ($help) {
    print "\n\n";
    eval { require Pod::Text };
    if (@$) {
        print
          "Error formatting help content! Perl-Module Pod::Text required!\n";
        print
          "To view help, please install Pod::Text module or look directly\n";
        print "into the perl script $0\n\n";
    }
    else {
        my $oPodParser = Pod::Text->new();
        $oPodParser->parse_from_file($0);
    }

    #  print `pod2text $0`;
    exit 0;
}

if ( !-e $in_seq_file ) {
    die "EMBL Inputfile $in_seq_file not found";
}

my $in_seq = Bio::SeqIO->new( '-file' => $in_seq_file, '-format' => 'EMBL');

my $out_seq = Bio::SeqIO->new( '-file' => ">$out_seq_file", '-format' => 'EMBL');

while (my $seq = $in_seq->next_seq) {
    my $count = 1;
    foreach my $feature ( grep ( $_->primary_tag eq 'CDS',
                                 $seq->top_SeqFeatures ) ) {
        $feature->add_tag_value('locus_tag', $prefix . sprintf("%0${num_chars}d", $count++))
            unless $feature->has_tag('locus_tag');
    }
    $out_seq->write_seq($seq);
}

