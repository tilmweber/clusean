#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program runs extracts amino acids according to XML-file
#
# InputFiles: EMBL-Format-File, processed with CLUSEAN pipeline (evaluateDomains.pl)
#
# Output: EMBL-File with blastp_file tags
#         HMMer-Reports of identified genes
#
#

=head1 NAME

ExtractScaffoldAA.pl

=head1 SYNOPSIS

SpecSitePredict.pl  --input <input-EMBL-file> --output <output-EMBL-file> [--onlynew --csv <CSV-file>
--noCSVheader --verbose --help]

=head1 DESCRIPTION

	Extracts amino acids according to Alignments
	Configuration: $CLUSEANROOT/etc/SignatureResources.xml

=head2 PARAMETERS

=head3
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --onlynew                         :       Only process CDS features with no
                                             previous NRPSPred-results
   --csv <filename>					 :		 In addition to EMBL export, also export as CSV
   --noCSVheader					 :		 Omit CSV headerline (useful for automated processing of
   											 the CSV line	                                             
                                             
   --verbose                         :       verbose output
   --help                            :       print help

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use CLUSEAN::AligAnal;
use CLUSEAN::Generic;
use CLUSEAN::GetDomains;
use Bio::SeqIO;
use Getopt::Long;
use XML::Simple;
use FindBin qw ($Bin);
use Config::General;
use File::Basename;
use Data::Dumper;

my $sInputSeqFn;     # Filename for InputFile
my $sOutputSeqFn;    # Filename for Outputfile
my $sCSVOutFn;       # Filename for annotation output file
my $bVerbose;        # Verbose flag
my $bHelp;           # Help flag
my $bNoCSVHead;      # Don't print CSV-Headerline of field descriptions
my $oSeqIOIn;        # Input SeqIO-Object
my $oSeqIOOut;       # Output SeqIO-Object
my $oSeq;            # Bio::Seq main object

#Import Configuration data
my $CLUSEANROOT;
if (defined $ENV{CLUSEANROOT}) {
	$CLUSEANROOT = $ENV{CLUSEANROOT};
}
else {
	$CLUSEANROOT = $Bin;
	$CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
}

# Parse Config file
my $oConfig = Config::General->new ('-ConfigFile'     =>  "$CLUSEANROOT/etc/CLUSEAN.cfg");
my %hConfig = $oConfig->getall;

my $sSeparator = $hConfig{Formatting}->{Separator}    || "|";    	   # Character for joining entries;
my $sXMLFileName = $hConfig{SignatureResources}->{SignatureResourceFile};

if (! -e "$CLUSEANROOT/$sXMLFileName") {
	die "Can't find Singnature XML file at $CLUSEANROOT/$sXMLFileName\n";
}


# Parse XML-File and assign hashref
my $xml = XML::Simple->new( 'ForceArray' => [ ('choice', 'scaffold') ],
							'SuppressEmpty' => 1 );
my $xmlHash = $xml->XMLin("$CLUSEANROOT/$sXMLFileName");

# Evaluate Cmdline and assign variables
GetOptions(
			'input|i=s'      => \$sInputSeqFn,
			'output|o=s'     => \$sOutputSeqFn,
			'csv|c=s'        => \$sCSVOutFn,
			'noCSVHeader|n'  => \$bNoCSVHead,
			'verbose|v'      => \$bVerbose,
			'help|h'         => \$bHelp
);

# Check parameters
if ($bHelp)
{
	print "\n\n";
	eval { require Pod::Text };
	if (@$)
	{
		print
		  	"Error formatting help content! Perl-Module Pod::Text required!\n";
		print
			"To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else
	{
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}
	exit 0;
}
if ( !-e $sInputSeqFn )
{
	die "EMBL Inputfile $sInputSeqFn not found";
}

my ( undef, $sInputDirName, undef ) = fileparse($sInputSeqFn);


# First we have to load the annotated EMBL-file
$oSeqIOIn = Bio::SeqIO->new( '-file'   => $sInputSeqFn,
							 '-format' => 'EMBL' );

# create output object
$oSeqIOOut = Bio::SeqIO->new( '-file'   => ">$sOutputSeqFn",
							  '-format' => 'EMBL' );

if (defined $sCSVOutFn) {							 
	# open Filehandle for CSV output
	open (CSVOUT, ">$sCSVOutFn") || die "Cannot open $sCSVOutFn for writing";
	
	if (! defined $bNoCSVHead) {
		my @aHeader = ( "Accession number",
						"Analysis name",
						"Analysis description",
						"GeneName", "CDSstart", "CDSend",
						"FeatureLabel", "FeatureStart", "FeatureEnd",
						"QueryScaffold","HitScaffold","Scaff. binary comp.",
						"Extracted residues signature", "Corresponding signature of hit",
						"Reference signature to compare with",
						"Binary comparison",
						"Result Name", "Description of Result");
		print CSVOUT join ($sSeparator, @aHeader),"\n";	
	}
}

# For every Sequence in File...
while ( $oSeq = $oSeqIOIn->next_seq )
{

	# get sequence id
	my $sSeqID = $oSeq->id;

	# if ID is unknown check accession...
	if ( $sSeqID =~ /.*unknown.*/ )
	{
		$sSeqID = $oSeq->accession;
	}
	$sSeqID =~ s/\s/_/g;
	$sSeqID =~ s/\;$//;
	my @aAnalyses = keys( %{$xmlHash} );
	
	# For each analysis defined in SignatureResources.xml file 
	foreach my $sAnalysis (@aAnalyses)
	{

		# Skip xmlns data...
		next if ( $sAnalysis =~ /xmlns.*/ );
		
		# only run on prediction type analyses
				
		next if ($xmlHash->{$sAnalysis}->{type} !~ /ResidueExtract/);
		
		
		if ($bVerbose)
		{
			print "---------\nAnalysis: $sAnalysis\n\n";
		}
		
		# Now cylce through CDS features
		foreach my $oCDSFeature (
				 grep ( ( $_->primary_tag eq 'CDS' ), $oSeq->top_SeqFeatures ) )
		{
			my $oOverlappingFeatures =
			  CLUSEAN::GetDomains->new( '-featureObj' => \$oCDSFeature,
										'-seqObj'     => $oSeq );
			my @aOverlappingFeatures = $oOverlappingFeatures->getSubfeatures;

			# skip CDS if there are no overlapping features for this CDS
			next if ( !defined $aOverlappingFeatures[0] );
			my @aFeatures = grep ( (
					(
					  $_->primary_tag eq $xmlHash->{$sAnalysis}->{Prerequisite}
						->{primary_tag_type}
					)
					  && (
						   $_->has_tag(
								   $xmlHash->{$sAnalysis}->{Prerequisite}->{tag}
						   )
					  )
				 ),
				 @aOverlappingFeatures );
				 
				 
			foreach my $oFeature (@aFeatures)
			{

				# Skip CDS if it is not annotated as defined in XML file
				my ($sLabel) =
				  $oFeature->get_tag_values(
								$xmlHash->{$sAnalysis}->{Prerequisite}->{tag} );
				chomp($sLabel);
				if ( $sLabel !~
					 /$xmlHash->{$sAnalysis}->{Prerequisite}->{tag_value}/ )
				{
					next;
				}
				if ($bVerbose)
				{
					print "Analyzing CDS ", $oCDSFeature->start, "..",
					  $oCDSFeature->end, " ";
					if ( $oCDSFeature->has_tag('gene') )
					{
						my ($sGene) = $oCDSFeature->get_tag_values('gene');
						print "Gene: $sGene\n";
					}
					print "\n$sLabel at ", $oFeature->start, "..",
					  $oFeature->end, "\n";
				}

                # Now we should only have $oFeatures that match the prerequisites defined in xml file
                
                # Attach sequence data
				$oFeature->attach_seq($oSeq);
				
				# Get gene name
				my $sGene;
				if ( $oCDSFeature->has_tag('gene') ){
					($sGene) = $oCDSFeature->get_tag_values('gene');
				}
				else {
					$sGene="noGeneName";
				}
				
				# Generate AligAnal object
				my $oAligAnal =  CLUSEAN::AligAnal->new(
										  '-xml_Hash' => $xmlHash,
										  '-oFeature' => $oFeature,
										  '-program'  => $sAnalysis  );
										  
				if ($bVerbose) {
					print "************\n";
					print "Running analysis: $sAnalysis \n";
								
					}
				
				# Retrieve Scaffold information
				my @aScaffoldHit   = @{$oAligAnal->ExtractResidues(@{$oAligAnal->get_aScaffoldOffset})->get_aHit};
				my @aScaffoldQuery = @{$oAligAnal->ExtractResidues(@{$oAligAnal->get_aScaffoldOffset})->get_aQuery};
				
				# Check whether scaffolds match
				my $oScaffCompArray = CLUSEAN::Generic->new;
				
				my @ScaffCompArray =	$oScaffCompArray->compareArray(
										 '-Array1' => \@aScaffoldHit,
										 '-Array2' => \@aScaffoldQuery );
										 
				my $sScaffCompString = join ("",@ScaffCompArray); # @CompArray does contain 1 for every match and 0 for mismatch
				
					
				# Output Scaffold information
				
				if ($bVerbose) {
					print "###########################\n";
					print "QueryCoordinates: ",
							  $oAligAnal->get_HSP_startQ, "..",
							  $oAligAnal->get_HSP_endQ,   "\n";
					print "Gene             : $sGene\n";
							
					print "H-Scaffold       : ", @aScaffoldHit, "\n";
					print "Q-Scaffold       : ", @aScaffoldQuery, "\n";
					print "MatchArray       : $sScaffCompString\n";					
										 
					if ($sScaffCompString =~ /0/) {
						print "CAUTION: Scaffold sequence does not match 100% \n";
					}
					else {
						print "Scaffold sequences match :-)\n";
					}
					print "###########################\n";
					
				}
				
				# Now cycle through available comparisonsi
				 
												
						my $sNote =  "Checking for Scaffold for" . $sAnalysis . ". ";
						$sNote .= "Query-Scaffold  @aScaffoldQuery compared against Hit";
						$sNote .= " scaffold @aScaffoldHit resulted in match $sScaffCompString. ";
						
						if ($sScaffCompString =~ /0/) {
							$sNote  .= "Query does NOT 100% match against scaffold residues";
						}
						else {
							$sNote  .= "Qery does match scaffold.";
						}
						
						  
						$oFeature->add_tag_value( 'note', $sNote );

											
						if ($sCSVOutFn) {
							my $sSeqID = $oSeq->accession;
							$sSeqID =~ s/;$//;
							
							# Assemble output line
							# Accession number |
							my $sLine = $sSeqID . $sSeparator;
							
							# AnalysisName |
							$sLine .= $sAnalysis . $sSeparator . $xmlHash->{$sAnalysis}->{'description'} . $sSeparator;
							
												
							# GeneName | CDSstart | CDSend
							$sLine .= $sGene . $sSeparator. $oCDSFeature->start . $sSeparator; 
							$sLine .= $oCDSFeature->end. $sSeparator;
							
							# FeatureLabel | FeatureStart | FeatureEnd 
							$sLine .= $sLabel . $sSeparator. $oFeature->start . $sSeparator; 
							$sLine .= $oFeature->end . $sSeparator;
							
							#Scaffold information
							$sLine .= join ("", @{$oAligAnal->ExtractResidues(@{$oAligAnal->get_aScaffoldOffset})->get_aQuery})
					    				.$sSeparator;
					    	$sLine .= join ("", @{$oAligAnal->ExtractResidues(@{$oAligAnal->get_aScaffoldOffset})->get_aHit})
										.$sSeparator;
					    	
					    	$sLine .= $sScaffCompString . $sSeparator;
					    	
							#
							
							# And print...
							print CSVOUT $sLine,"\n";
						}
					}
				}
			}
	

	# Write results to output EMBL file
	$oSeqIOOut->write_seq($oSeq);
}
