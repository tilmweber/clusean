#!/usr/bin/perl
#
# Author: Tilmann Weber
#
# This program veryfies that the EMBL-file submitted to be analyzed
# contains no malformed Features
#
# InputFiles: EMBL-Format-File
#
# Output: EMBL-File with blastp_file tags
#         BLASTP-Reports of identified genes
#
#

=head1 NAME

checkEMBLfile.pl

=head1 DESCRIPTION

Checks EMBL formatted file for syntax-error/features not supported in CLUSEAN

=head1 USAGE

B<checkEMBLfile.pl> --input <input-EMBL-file> [--verbose --help]

=head2 PARAMETER description

    --input <filename>       : Name of the embl-file to process
   
=head3 Optional parameters

    --verbose                : Verbose output
    --help                   : print help information

=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle28
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

use strict;
use Bio::SeqIO;
use Getopt::Long;
my $sInputSeqFn;    # Filename for InputFile
my $bVerbose;       # Verbose flag
my $bHelp;          # Help flag

# Thresholds
my $iMinBases = 30;        # reject file if CDS have minimal
                           # lengths smaller than $iMinBases
my $iMaxBases = 200000;    # reject file if there are CDS with span
                           # more than $iMaxBases
my $oSeqIOIn;              # Input SeqIO-Object
my $sOutString;            # multi purpose variable
                           # for the assembly of complex output strings
my $iGeneCount = 0;        # counter for CDS features
my $iWarn      = 0;        # Flag that a warning has been issued
my $iThrow     = 0;        # Flag that a critical problem has been identified

# Import CLUSEAN ENVIRONMENT
   # Not needed for this script
   
   
# Evaluate Cmdline and assign variables
GetOptions(
			'input|i=s' => \$sInputSeqFn,
			'verbose|v' => \$bVerbose,
			'help|h'    => \$bHelp
);

# Check parameters
print "\n\n";
if ($bHelp) {
	print "\n\n";
	eval { require Pod::Text };
	if (@$) {
		print
		  "Error formatting help content! Perl-Module Pod::Text required!\n";
		print
"To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else {
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}
	exit 0;
}
if ( !-e $sInputSeqFn ) {
	die "EMBL Inputfile $sInputSeqFn not found";
}

# Open EMBL-formatted file in Bioperl
if ($bVerbose) {
	print "Now trying to open $sInputSeqFn \n";
}
$oSeqIOIn = Bio::SeqIO->new( '-file'   => $sInputSeqFn,
							 '-format' => 'EMBL' );

# For every Sequence in File...
while ( my $oSeq = $oSeqIOIn->next_seq ) {
	if ($bVerbose) {
		print "Now trying analyzing the first sequence ", $oSeq->id, " \n";
	}

	# get sequence id
	if (    ( !defined $oSeq->id )
		 || ( $oSeq->id =~ /.*unknown.*/ ) )
	{
		$sOutString = "Critical error:\n\n";
		$sOutString .= "Input file has no valid id tag " . $oSeq->id . "!\n";
		$sOutString .= "It is highly recommended to assign unique EMBL-IDs!\n";
		$sOutString .=
		  "EMBL-IDs are used as identifiers for analysis modules of CLUSEAN.\n";
		$sOutString .=
		  "Ambiguities in these identifiers WILL LEAD to pipeline malfunction!";
		print $sOutString;
		$iThrow++;
	}
	if (    ( !defined $oSeq->accession )
		 || ( $oSeq->accession =~ /.*unknown.*/ ) )
	{
		$sOutString = "Critical error:\n\n";
		$sOutString = "Input file has no valid accession tag!\n";
		$sOutString .=
		  "It is highly recommended to assign an unique EMBL-Accession tag!\n";
		$sOutString .=
		  "EMBL-Acc are used as identifiers for analysis modules of CLUSEAN.\n";
		$sOutString .=
		  "Ambiguities in these identifiers WILL LEAD to pipeline malfunction!";
		print $sOutString;
		$iThrow++;
	}

	# now cycle though every identified orf..
	foreach
	  my $oFeature ( grep ( $_->primary_tag eq 'CDS', $oSeq->top_SeqFeatures ) )
	{

		# OK, if this is executed we do have CDS features...
		# so count'em up...
		$iGeneCount++;
		if ($bVerbose) {
			print "Analyzing CDS ", $iGeneCount, " with coordinates ";
			print $oFeature->start, "..", $oFeature->end, "\n";
		}
		if ( $oFeature->has_tag('pseudo') ) {
			print "/pseudo tag found for CDS feature ", $oFeature->start, "..";
			print $oFeature->end, ".\n";
			$iWarn++;
		}

		# Check for "mini-ORFs" that might crash analysis tools used in CLUSEAN
		if ( $oFeature->length < $iMinBases ) {
			$sOutString = "Critical error:\n\n";
			$sOutString .= "CDS feature " . $iGeneCount . " spanning region";
			$sOutString .=
			  $oFeature->start . ".." . $oFeature->end . " is smaller ";
			$sOutString .= "than" . $iMinBases . "!\n";
			$sOutString .= "This will crash 3rd party analysis modules!";
			print $sOutString;
			$iThrow++;
		}

	# Check for CDS larger than $iMaxBases -> this indicates wrong annotation or
	# a parsing error (which can ocurr if circular genomes are analyzed and a
	# CDS is spanning the origin (base 1))
		if ( $oFeature->length > $iMaxBases ) {
			$sOutString = "Critical error:\n\n";
			$sOutString .= "CDS feature " . $iGeneCount . " spanning region";
			$sOutString .=
			  $oFeature->start . ".." . $oFeature->end . " is larger ";
			$sOutString .= "than " . $iMaxBases . "!\n";
			$sOutString .=
			  "Please check that there is no CDS overlapping coordinate 0!";
			print $sOutString;
			$iThrow++;
		}
	}
}

# Check if there are CDS features annotated in sequence
if ( $iGeneCount == 0 ) {
	$sOutString = "Critical error:\n\n";
	$sOutString .= "EMBL-file does not contain annotated CDS features!\n";
	print $sOutString;
	$iThrow++;
}
if ( $iWarn > 0 ) {
	print "\n========================================\n";
	print "There were " . $iWarn
	  . " warnings issued with file "
	  . $sInputSeqFn . "\n";
}
if ( $iThrow > 0 ) {
	print "\n========================================\n";
	print "There were " . $iThrow
	  . " critical errors issued with \nfile "
	  . $sInputSeqFn . "\n";
	print "CLUSEAN cannot start/continue until these problems are fixed!\n";
	exit(1);
}
print "\n========================================\n";
print "Good news: No critical problems detected!"
