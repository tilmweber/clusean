#!/usr/bin/perl
#
# This script reads an hmmscan/hmmpfam report and extracts the identified domains into a new fasta file
#
#

=head1 NAME

extractDomains.pl

=head1 SYNOPSIS

extractDomains.pl --input <input FASTA file> --hmm <hmmscan/hmmpfam report fasta file> --output <output fasta file>
--prefix <string to prepend to domain numbering> --verbose --help

=head1 DESCRIPTION

Extracts identified domains from hmmscan or hmmpfam results file (non-tabular)

=head2 PARAMETERS

=head3
   --input <filename>                  :    Input hmmscan/hmmpfam report
   --output <filename>                 :    Output filename
   --minHSPLength <number>             :    Mininmal length of HSP to be included in export
   --prefix <string>                   :    string to prepend to domain number
                                            default: dom
   --verbose                           :    Verbose output
   --help                              :    Print this help screen

=head3 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 Auf der Morgenstelle 28
 72076 Tuebingen
 Germany
 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut
use strict;
use Bio::SearchIO;
use Bio::SeqIO;
use Bio::Seq;
use Getopt::Long;

my $sInputSeqFn;
my $sOutputSeqFn;
my $bVerbose;
my $bHelp;
my $sHMMFn;
my $iMinHSPLength;
my $sDomPrefix ="dom";

# Evaluate Cmdline and assign variables
GetOptions(
			'input|i=s'	   => \$sHMMFn,
			'output|o=s'       => \$sOutputSeqFn,
			'minHSPlength|m=s' => \$iMinHSPLength,
			'prefix|p=s'       => \$sDomPrefix,
			'verbose|v'        => \$bVerbose,
			'help|h'           => \$bHelp
);

# Check parameters
if ($bHelp)
{
	print "\n\n";
	eval { require Pod::Text };
	if (@$)
	{
		print
		  	"Error formatting help content! Perl-Module Pod::Text required!\n";
		print
			"To view help, please install Pod::Text module or look directly into\n";
		print "the perl script $0\n\n";
	}
	else
	{
		my $oPodParser = Pod::Text->new();
		$oPodParser->parse_from_file($0);
	}
	exit 0;
}


if ( !-e $sHMMFn )
{
	die "imput file $sHMMFn not found\n\n";
}



# create output object
my $oSeqIOOut = Bio::SeqIO->new( '-file'   => ">$sOutputSeqFn",
				 '-format' => 'FASTA' );

my $oSearchIO = Bio::SearchIO->new ('-file'   => $sHMMFn,
				    '-format' => 'hmmer');



while (my $oSearchResult=$oSearchIO->next_result) {
  my $i=1;
	
	
	while (my $oHit=$oSearchResult->next_hit) {
		
		if ($bVerbose) {
			print "Found HMM-Hit for ",$oSearchResult->query_name," matching ",$oHit->name,"\n";
		}
		while (my $oHSP=$oHit->next_hsp) {

			if ($bVerbose) {
				print "Found HSP $i at ",$oHSP->start("query"),"..",$oHSP->end("query")," with length ",$oHSP->end("query")-$oHSP->start("query"),"\n";
			}

			if (($oHSP->end("query")-$oHSP->start("query")) < $iMinHSPLength,) {
			  warn ($oSearchResult->query_name.": ...skipping hit ".$oHSP->start("query")."..".$oHSP->end("query").
				" as HSP length is below threshold (HSP-length: ".
				$oHSP->length("query")."\n");
			  next;
			}
			my $oExportSeq=$oHSP->seq('query');
			my $stmpSeq=$oExportSeq->seq;
			$stmpSeq =~ s/-//g;
			$oExportSeq->seq($stmpSeq);
			$oExportSeq->id($oSearchResult->query_name."_".$sDomPrefix.sprintf ("%02d",$i));
			my $desc=$oSearchResult->query_description;
			$oExportSeq->description($oHSP->start("query")."..".$oHSP->end("query")." ".$desc);
			$oSeqIOOut->write_seq($oExportSeq);
			$i++
		}
	}
}
