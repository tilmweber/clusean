use strict;

sub readStdinAndReturnAsArray {
	my $stdin = <STDIN>;
	chomp $stdin;
	my @stdinArray = split(/\s+/, $stdin);
	return @stdinArray;
}


sub testIfFileExistsAndIfOpenable {
	my ($filename) = @_;
	#print $filename."insub\n";
	unless (-e $filename) {
		print "File  \"$filename\" doesn't seem to exist or entered no name!\n";
		exit;
	}
	# Can we open the file?
	unless ( open(FILE, $filename)) {
		print "Cannot open \"$filename\"\n\n";
		exit;
	}
}
1;
