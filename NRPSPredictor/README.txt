=========================================================================
=========================================================================
Installation and usage guide for the NRPSpredictor (command line version)
=========================================================================
=========================================================================


Dependence on other programs:
=============================
To be able to run NRPSpredictor you need to have the program package
*HMMer*
and
*SVMlight*
installed on your system.
The excecutables hmmpfam (of HMMer) and svm_classify (of SVMlight) need to be in
your in your PATH.

Installation
============
Unzipping of the NRPSpredictor.zip file you will give you a directory
"NRPSpredictor".
Move this to where you usually have installed your programs.
In the following we will call the full path of the directory NRPSpredictor
$scriptHomeDir, e.g. /home/dummy/programs/NRPSpredictor".
Create a temporary directory where the NRPSpredictor can write temporary files
and the resulting HTML file called "HTMLOUT.html". The full path to this temporary
directory will be called $tempDir in the following.

Input file formats
==================
The NRPSpredictor accepts your putative NRPS protein sequence(s) in FASTA or
Mulit-FASTA format, repectively.
Your may also provide the sequence as "raw" (nothing else but amino acids, i.e.
no ">"-fasta header).

Usage
======
copy your sequence file (e.g. sequence.fasta) to the $tempDir.
start the NRPSpredictor:
perl $scriptHomeDir/nrpsSpecPredictor.pl sequence.fasta $tempDir $scriptHomeDir
During runtime the NRPSpredictor writes into the result file HTMLOUT.html in the
$tempDir.
It also writes a simple text based output (without sequence alignment) to the STDOUT.
We recommend to pipe this output to a file, say "NRPSpredict.out":
perl $scriptHomeDir/nrpsSpecPredictor.pl sequence.fasta $tempDir $scriptHomeDir>NRPSpredict.out

There is also a program called "extractFromPredictor.pl"
that "digests" the simple text output of the NRPSpredictor:
perl $scriptHomeDir/extractFromPredictor NRPSpredict.out
it returns the output in CSV format to be read with a spreadsheet application.

Questions
=========
plase write to rausch@informatik.uni-tuebingen.de
