#!/usr/bin/perl

# TWChange: Changed "system" call to represent Windows directory nomenclature ( \ vs. / )
use strict;
use warnings;

my @programOptionsArray = <@ARGV>;
my $fastaFile           = $programOptionsArray[0];
my $tempDir             = $programOptionsArray[1];
my $scriptHomeDir       = $programOptionsArray[2];


if ( $fastaFile eq "" || $tempDir eq "" || $scriptHomeDir eq "" )
{
	print "please give the following program parameters:\n";
	print "first:  name your fasta-sequence-file (the program will look for it in the tempDir(the 2nd parameter))\n";
	print "second: the location of the tempDir (full path, no SPACES!!)\n";
	print "third:  the location of the scriptHomeDir (full path, no SPACES!!)\n";
	exit;
}

# TWCHANGE: Added OS detection

my $OS = $^O;        # Get OS string!
my $sDirectorySeparator;    # / for Linux, \ for Win

if ($OS =~ /MSWin.*/) {
	$sDirectorySeparator = "\\";
}
else {
	$sDirectorySeparator = "/";
}

open(FASTA, "$tempDir/$fastaFile") || die "Can't open $tempDir/$fastaFile";
my @fasta_array = <FASTA>;
close (FASTA);
if ($fasta_array[0] !~ /^>\S+/) {
	my $time = `date +%Y-%m-%d-%H-%M-%S`;
	open(FASTANEW, ">$tempDir/$fastaFile");
	print FASTANEW ">$time";
	for (my $i=0; $i<@fasta_array; $i++) {
		print FASTANEW $fasta_array[$i];
	}
	close(FASTANEW);

}
# TWCHANGE:


# system("hmmpfam -T 4 $scriptHomeDir/aa-activating.aroundLys.hmm $tempDir/$fastaFile > $tempDir/$fastaFile.hmmpfam.out");

#For windows we have to convert slashes to backslashes in $tempDir
my $ShelltempDir = $tempDir;
if ($OS =~ /.*MSWin.*/) {
	$ShelltempDir =~ s/\//\\/g;
}

my $hmmfastafile = $ShelltempDir.$sDirectorySeparator.$fastaFile;
my $CmdLine  = "hmmpfam2 -T 4 ".$scriptHomeDir.$sDirectorySeparator."aa-activating.aroundLys.hmm ";

$CmdLine    .= $hmmfastafile." > ".$tempDir.$sDirectorySeparator.$fastaFile.".hmmpfam.out";

system ($CmdLine);

# system("perl -I$scriptHomeDir $scriptHomeDir/nrpsPredictor.main.pl $fastaFile.hmmpfam.out $tempDir $scriptHomeDir");

$CmdLine   = "perl -I".$scriptHomeDir." ".$scriptHomeDir.$sDirectorySeparator."nrpsPredictor.main.pl";
$CmdLine  .= " ".$fastaFile.".hmmpfam.out ".$tempDir." ".$scriptHomeDir;

system ($CmdLine);

