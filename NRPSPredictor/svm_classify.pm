#!/usr/bin/perl
use strict;
use warnings;


sub svm_classify {
	# TWCHANGE: Added OS detection

	my $OS = $^O;        # Get OS string!
	my $sDirectorySeparator;    # / for Linux, \ for Win

	if ($OS =~ /MSWin.*/) {
		$sDirectorySeparator = "\\";
	}	
	else {
		$sDirectorySeparator = "/";
	}
	
	my @tempDir_scriptHomeDir_featureVector_stringBuffer = @_;
	my $tempDir       = $tempDir_scriptHomeDir_featureVector_stringBuffer[0];
	my $scriptHomeDir = $tempDir_scriptHomeDir_featureVector_stringBuffer[1];
	my $featureVector = $tempDir_scriptHomeDir_featureVector_stringBuffer[2];
	my $stringBuffer  = $tempDir_scriptHomeDir_featureVector_stringBuffer[3];
	open( FEATUREFILE, ">$$tempDir/featureVector.tmp") || die "Can't open $$tempDir/featureVector.tmp";
	print FEATUREFILE $$featureVector."\n";
	close(FEATUREFILE);
	my @arrayOfClustersOfSimilarAA;
	
	foreach my $typeOfClusters ("large", "small") {
		if($typeOfClusters eq "large") {
			@arrayOfClustersOfSimilarAA= split(/_/, "gly=ala=val=leu=ile=abu=iva_ser=thr=ser-thr=dht=dhpg=dpg=hpg_phe=trp=phg=tyr=bht_asp=asn=glu=gln=aad_cys_orn=lys=arg_pro=pip_dhb=sal");	
		}
		else {
			if($typeOfClusters eq "small") { 
			@arrayOfClustersOfSimilarAA= split(/_/, "gly=ala_val=leu=ile=abu=iva_ser_thr=dht_dhpg=dpg=hpg_phe=trp_phg_tyr=bht_asp=asn_glu=gln_aad_cys_orn_arg_pro_dhb=sal");
			}
			else {
				print "ClusterType must be large or small! Exiting now...\n"; exit;
			}
		}
		my %bestScoreRank;
		foreach my $model_file (@arrayOfClustersOfSimilarAA) {
			if(-e "$$scriptHomeDir/$model_file.model") 	{
				#print "file $model_file.model does exist.\n";
				
				
				# TWCHANGE: SUpport for Windows
				# system("svm_classify $$tempDir/featureVector.tmp $$scriptHomeDir/$model_file.model $$tempDir/$model_file".".predictions >/dev/null" );
				
				my $CmdLine = "svm_classify ".$$tempDir.$sDirectorySeparator."featureVector.tmp ";
				$CmdLine    .= $$scriptHomeDir.$sDirectorySeparator.$model_file.".model ";
				$CmdLine    .= $$tempDir.$sDirectorySeparator.$model_file.".predictions";
				
				if ($OS =~ /.*MSWin.*/) {
					$CmdLine .= " > nul";
				}
				else {
					$CmdLine .= " > /dev/null";
				}
				system ($CmdLine) ;
				
				open(PREDICTION, "$$tempDir/$model_file".".predictions") || die ("Can't open $$tempDir/$model_file.predictions");
				my @prediction_lines = <PREDICTION>;
				close(PREDICTION);
				chomp($prediction_lines[0]); # file contains one line for the one prediction
				#print "prediction:$prediction_lines[0]\n";			
				my $predictor_quality=0.9;
				if ($typeOfClusters eq "large") {
					if($model_file eq "dhb=sal" ) 				{$predictor_quality=0.96*0.96;}
					if($model_file eq "asp=asn=glu=gln=aad") 		{$predictor_quality=0.95*0.95;}
					if($model_file eq "pro=pip") 				{$predictor_quality=0.95*0.95;}
					if($model_file eq "cys" ) 				{$predictor_quality=0.94*0.94;}
					if($model_file eq "ser=thr=ser-thr=dht=dhpg=dpg=hpg")	{$predictor_quality=0.92*0.92;}
					if($model_file eq "gly=ala=val=leu=ile=abu=iva" )	{$predictor_quality=0.90*0.90;}
					if($model_file eq "orn=lys=arg") 			{$predictor_quality=0.90*0.90;}
					if($model_file eq "phe=trp=phg=tyr=bht")		{$predictor_quality=0.85*0.85;}

				}
				else {
					if($typeOfClusters eq "small") {				
						if($model_file eq "dhb=sal")			{$predictor_quality=1.00*1.00;}
						if($model_file eq "aad")			{$predictor_quality=1.00*1.00;}
						if($model_file eq "dhpg=dpg=hpg")		{$predictor_quality=0.97*0.97;}						
						if($model_file eq "ser")			{$predictor_quality=0.96*0.96;}
						if($model_file eq "cys")			{$predictor_quality=0.94*0.94;}
						if($model_file eq "thr=dht")			{$predictor_quality=0.93*0.93;}					
						if($model_file eq "glu=gln")			{$predictor_quality=0.93*0.94;}
						if($model_file eq "asp=asn" )			{$predictor_quality=0.92*0.92;}
						if($model_file eq "val=leu=ile=abu=iva")	{$predictor_quality=0.91*0.91;}
						if($model_file eq "orn" )			{$predictor_quality=0.87*0.87;}
						if($model_file eq "pro")			{$predictor_quality=0.93*0.93;}
						if($model_file eq "gly=ala" )			{$predictor_quality=0.84*0.84;}
						if($model_file eq "arg" )			{$predictor_quality=0.80*0.80;}
						if($model_file eq "tyr=bht")			{$predictor_quality=0.83*0.83;}
						if($model_file eq "phe=trp")			{$predictor_quality=0.60*0.60;}

					}
					else {
						print "Cluster type is neither \"large\" nor \"small\"\n";
						print "please specify the predictor_quality (weight) in the file svm_classify.pm\n";
						print "I'm assuming good quality (0.9) for now\n";
						$predictor_quality=0.9;				
					}				
				}
				if ($prediction_lines[0]>=0) {
					my $rank=$predictor_quality*$prediction_lines[0];
					unless (exists $bestScoreRank{$rank}) {
						$bestScoreRank{$rank}="$model_file-like specificity Score:$rank";
					}
					else {
						$bestScoreRank{$rank}.="<BR>\n$model_file-like specificity Score:$rank";
					}					
				}
			}
		}
		# evaluate %bestScoreRank
		my @array_of_best_models = sort by_number_decreasing (keys %bestScoreRank);
		if($typeOfClusters eq "large") {$$stringBuffer.= "Predictions assuming that several substrate amino acids have same properties <B>(\"large clusters\"):</B><BR>\n";}
		if($typeOfClusters eq "small") {$$stringBuffer.= "Predictions assuming that only few substrate amino acids have same properties <B>(\"small clusters\"):</B><BR>\n";}
		if (0==@array_of_best_models) {$$stringBuffer.="<B>[no predictions, all available models return negative]</B><BR>\n";}
		for (my $i=0; $i<@array_of_best_models; $i++) {
			$$stringBuffer.="<B>".$bestScoreRank{$array_of_best_models[$i]}."<BR></B>\n";	
		}
		#unlink("$$tempDir/featureVector.tmp");
	}
}

#==================================OTHER SUBS:===============================
# numericly sort an array:
sub by_number_decreasing {
#usage: my @numbers_sorted = sort by_number @numbers;
    chomp($a);
    chomp($b);
    if ($a < $b) {
        return 1;
    } elsif ($a == $b) {
        return 0;
    } elsif ($a > $b) {
        return -1;
    }
}
1;

########		
# kleine
########		
#gly=ala val=leu=ile=abu=iva
#ser thr dhpg=dpg=hpg
#asp=asn
#glu=gln
#aad
#cys_kleine (not used, other in grosse might be slightly better)
#orn
#pro
#dhb=sal_kleine (not used, other in grosse might be slightly better)
#tyr (not used, bad predictor)
#arg (not used, bad predictor)
#phe (not used, bad predictor)

########
# grosse
########
#gly=ala=val=leu=ile=abu=iva
#ser=thr=dhpg=dpg=hpg
#phe=trp=phg=tyr=bht
#asp=asn=glu=gln=aad
#cys_grosse
#orn=lys=arg
#pro=pip
#dhb=sal_grosse
