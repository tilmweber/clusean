#!/usr/bin/perl
use strict;

#use warnings;

my @programOptionsArray = <@ARGV>;
my $hmmpfamFile         = $programOptionsArray[0];
my $tempDir             = $programOptionsArray[1];
my $scriptHomeDir       = $programOptionsArray[2];

#use lib '/home/rausch/.eclipse/extract_pos_from_hmmpfamout';
use externalModules;
use digestHmmpfamModule;
use extractADomCode;
use CODE2features;
use svm_classify;
use alignToSignatureDB;

my $nameOfAMPdomainHMM = "aa-activating-core.198-334";
my $nameOflys517       = "aroundLys517";
my @aaindexFiles       = (
    "aa-hydrogenbond.aaindex",        "aa-hydrophobicity-neu1.aaindex",
    "aa-hydrophobicity-neu2.aaindex", "aa-hydrophobicity-neu3.aaindex",
    "aa-polar-zimmerman.aaindex",     "aa-polar-radzicka.aaindex",
    "aa-polar-grantham.aaindex",      "aa-volume.aaindex",
    "aa-beta-turn.aaindex",           "aa-beta-sheet.aaindex",
    "aa-alpha-helix.aaindex",         "aa-isoelectric.aaindex"
);

if ( $hmmpfamFile eq "" || $tempDir eq "" || $scriptHomeDir eq "" ) {
    print "please give the following program parameters:\n";
    print "first:  name of the hmmpfam-outfile (the program will look for it ";
    print         "in the tempDir(the 2nd parameter))\n";
    print "second: the location of the tempDir (full path)\n";
    print "third:  the location of the scriptHomeDir (full path)\n";
    exit;
}
testIfFileExistsAndIfOpenable("$tempDir/$hmmpfamFile");

my @seqID_array;
my %seqID2Description_hash;
my %seqID2ArrayOfDetectedHMMs;

#my @detectedHMMs_array;
digestHmmpfam(
    "$tempDir/$hmmpfamFile",  \@seqID_array,
    \%seqID2Description_hash, \%seqID2ArrayOfDetectedHMMs
);

#TWCHANGE
# Direct all output from STDOUT to file $temDir/NRPSPredictor.out

open( *STDOUT, ">$tempDir/NRPSPredictor.out" );

# TWCHANGE: We need the same structure for all domains, so I uncommented header lines
# print "These fasta sequences are to be analysed:\n";
for ( my $i = 0 ; $i < @seqID_array ; $i++ ) {

    # TWCHANGE
    # print "Sequence #".($i+1).":$seqID_array[$i]\n";
    my $key = $seqID_array[$i];

    my @temp_array         = @{ $seqID2ArrayOfDetectedHMMs{$key} };
    my $priviousDomainName = "";
    for ( my $j = 0 ; $j < @temp_array ; $j++ ) {
        my $returnString8A;
        my $returnStringStac;
        my $HTML_ali;

        #currentDomain = domainName;
        my ( $domainName, $modulNumber, $totalModulNumbers, $startPos, $endPos,
            $score, $topline, $downline )
          = split( /\t\t\t/, $temp_array[$j] );
        if ( $domainName eq "[no hits above thresholds]" ) {
            print "[no hits above thresholds]\n";
            next;
        }
        if ( $domainName eq $nameOflys517
          )    # lys-Domain is not writen here but in the look-ahead below
        {      # i.e. first domain is aroundLys
            next;
        }
        if ( $domainName eq $nameOfAMPdomainHMM ) {
            extractCode( \$topline, \$downline, $nameOfAMPdomainHMM,
                \$returnString8A, \$returnStringStac, \$HTML_ali );
            my (
                $nextDomainName,        $nextModulNumber,
                $nextTotalModulNumbers, $nextStartPos,
                $nextEndPos,            $nextScore,
                $nextTopline,           $nextDownline
              )
              = split( /\t\t\t/, $temp_array[ $j + 1 ] )
              ;  # this works only if $j+1th element exists, it's veryfied below
             #==> 2 ifs: 1st: if after the current AMPdomain the next one is following directly  (both blocks are almost equal)
            if (
                (
                       ( $j + 1 ) < @temp_array
                    && ( $nextDomainName eq $nameOfAMPdomainHMM )
                )
                || ( $j + 1 ) >= @temp_array
              )
            {

                # i.e. no aroundLys-Domain between this and next AMP-Dom or
                # this is the last Dom

                #TWchange: Add module coordinates
                print "Modul: $seqID_array[$i]_m$modulNumber spanning aa ";
                print        "$startPos..$endPos\n";
                print "8A-Code: $returnString8A\n";
                print "10 amino acid code defined by Stachelhaus et al.: ";
                print "$returnStringStac-\n";

                my $stringBuffer = "";
                my $featureVector =
                  CODE2features( $scriptHomeDir, $returnString8A,
                    @aaindexFiles );
                svm_classify(
                    \$tempDir,       \$scriptHomeDir,
                    \$featureVector, \$stringBuffer
                );
                $stringBuffer =~ s/\<.*?\>//g;        # remove HTML-Tags
                $stringBuffer =~ s/ \bnot.*?\n//g;    # remove neg. results
                $stringBuffer =~
s/Predictions                      \(reliability of predicter\)\n//g;
                unless ( $stringBuffer eq "" ) { print $stringBuffer; }
                else                           { print "[no predictions]\n"; }

                $stringBuffer = "";
                alignToSignatureDB( $hmmpfamFile, $tempDir, $scriptHomeDir,
                    "$returnStringStac-", \$stringBuffer );
                my $score_ident_line = "[no matches]";
                if ( $stringBuffer =~ m/^Score.*$/m ) {
                    $score_ident_line = $&;
                }
                print "Alis (>=70% Ident, only best-scoring)\n";

                # TWchange: Add // delimiter at end of each entry
                # print "$score_ident_line\n\n";
                print "$score_ident_line\n//\n";
                next;
            }

            #==> 2 ifs: 2nd: if after the current AMPdomain  a lys517 domain is
            #following (= the normal case)  (both blocks are almost equal)
            if ( $nextDomainName eq $nameOflys517 ) {
                $HTML_ali = "";
                extractCode(
                    \$nextTopline,    \$nextDownline,     $nameOflys517,
                    \$returnString8A, \$returnStringStac, \$HTML_ali
                );

                #TWchange: Add module coordinates
                print "Modul: $seqID_array[$i]_m$modulNumber spanning aa ";
                print        "$startPos..$endPos\n";
                print "8A-Code: $returnString8A\n";
                print "10 amino acid code defined by Stachelhaus et al.: ";
                print "$returnStringStac\n";

                my $stringBuffer = "";
                my $featureVector =
                  CODE2features( $scriptHomeDir, $returnString8A,
                    @aaindexFiles )
                  ; # relevant are only $scriptHomeDir, $returnString8A, @aaindexFiles
                svm_classify(
                    \$tempDir,       \$scriptHomeDir,
                    \$featureVector, \$stringBuffer
                );
                $stringBuffer =~ s/\<.*?\>//g;        # remove HTML-Tags
                $stringBuffer =~ s/ \bnot.*?\n//g;    # remove neg. results
                $stringBuffer =~
s/Predictions                      \(reliability of predicter\)\n//g;
                unless ( $stringBuffer eq "" ) { print $stringBuffer; }
                else                           { print "[no predictions]\n"; }

                # = end of <PRE>
                $stringBuffer = "";
                alignToSignatureDB(
                    $hmmpfamFile,      $tempDir, $scriptHomeDir,
                    $returnStringStac, \$stringBuffer
                );
                my $score_ident_line = "[no matches]";
                if ( $stringBuffer =~ m/^Score.*$/m ) {
                    $score_ident_line = $&;
                }
                print "Alis (>=70% Ident, only best-scoring)\n";

                # TWchange: Add // delimiter at end of each entry
                # print "$score_ident_line\n\n";
                print "$score_ident_line\n//\n";
                next;
            }
        }
    }

}
