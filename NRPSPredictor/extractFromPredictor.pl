#!/usr/bin/perl
use strict;
#use warnings;
#open( PREDICTOROUTPUT, "/home/rausch/nrps/predictor/x" );
#open( PREDICTOROUTPUT, "x2.ganz.stac.txt" );

my @programParameters = <@ARGV>;
# REMEMBER: $programParameters[0] must be the simplified output of the predictor (the non-HTML-output)

if ($programParameters[0] eq "") {
	print "you could have given the simplified output as parameter;\n";
	print "since you didn't please do it now:\n";	
	my $input = <STDIN>;
	chomp($input); #remove newline at the end of the string
	$input=~s/^\s+//; #remove leading spaces
	@programParameters = split(/\s+/, $input);
}


open( PREDICTOROUTPUT, "$programParameters[0]");
my @predictoroutput = <PREDICTOROUTPUT>;


my %modul_data; # GLOBAL HASH #####################################



	my $modulName="";
	my $largeClusterPrediction=0;
	my $smallClusterPrediction=0;


for ( my $i = 0 ; $i < @predictoroutput ; $i++ ) {


	my $AchtACode="";
	my $StacCode="";

	my $prediction="";
	my $identity=0;
	my $AliSpec="";


	my $line = $predictoroutput[$i];
	chomp($line);
	if ( $line =~ /These fasta sequences are to be analysed/ ) { next; }
	if ( $line =~ /Sequence \#/ ) { next; }
	if ( $line =~ /Modul: / ) {    # Neues Modul beginnt
		$modulName              = $';   #####################################
		#print "ModulName:$modulName:\n";
	}
	if ( $line =~ /8A-Code: / ) {
		$AchtACode = $';                #####################################           
		unless ( exists $modul_data{$modulName}{AchtA} ) {$modul_data{$modulName}{AchtA} = $AchtACode;}
		else {
			print "There exists already an AchtA entry for modul $modulName! exiting now...\n";
			exit;
		}		
		#print "AchtACode:$modul_data{$modulName}{AchtA}:\n";
	}
	if ( $line =~ /10 amino acid code defined by Stachelhaus et al\.: / ) {
		$StacCode = $';                 #####################################
		unless ( exists $modul_data{$modulName}{Stac} ) {$modul_data{$modulName}{Stac} = $StacCode;}
		else {
			print "There exists already an Stac entry for modul $modulName! exiting now...\n";
			exit;
		}
		#print "StacCode:$StacCode:\n";
	}
	if ( $line =~ /Predictions/ && $line =~ /large clusters/ ) {
		#print "largeclusters\n";
		$largeClusterPrediction = 1;                #####################################
		$smallClusterPrediction = 0;                #####################################
	}
	if ( $line =~ /Predictions/ && $line =~ /small clusters/ ) {
		#print "smallclusters\n";
		$largeClusterPrediction = 0;                #####################################
		$smallClusterPrediction = 1;                #####################################
	}
	if ( $line =~ /-like specificity/ ) {               # evaluating pred. for large clusters
		if ( $largeClusterPrediction == 1 ) {
			$prediction = $`;#####################################
			$prediction =~ s/^\s+//g;
			$line =~ /Score:/;
			$prediction .= "_s_$'";

			#print "Prediction:$prediction for largeC\n";
			if ( exists $modul_data{$modulName}{large} ) {
				if (exists $modul_data{$modulName}{large_more}) {
				$modul_data{$modulName}{large_more} .= "__$prediction";
				}
				else {
				$modul_data{$modulName}{large_more} .= "$prediction";
				}
			} else {
				$modul_data{$modulName}{large} .= "$prediction";
			}
			#print "PredictionnowLarge:$modul_data{$modulName}{large}\n";
		}
	}
	if ( $line =~ /-like specificity/ ) {               # evaluating pred. for small clusters
		if ( $smallClusterPrediction == 1 ) {
			$prediction = $`;#####################################
			$prediction =~ s/^\s+//g;
			unless(exists $modul_data{$modulName}{small}) {
				if($modul_data{$modulName}{large} !~ m/$prediction/) {$modul_data{$modulName}{ambiguity}=1;} ### Possible ambiguity: pred. of a small cluster is not contained in the predictions of large clusters
			}	
			#print "Prediction:$prediction for smallC\n";
			$line =~ /Score:/;
			$prediction .= "_s_$'";
			if ( exists $modul_data{$modulName}{small} ) {
				if (exists $modul_data{$modulName}{small_more}) {
				$modul_data{$modulName}{small_more} .= "__$prediction";
				}
				else {
				$modul_data{$modulName}{small_more} .= "$prediction";
				}

				$modul_data{$modulName}{ambiguity}=1;			### Possible ambiguity: several predictors of the small clusters say "yes"
			} else {
				$modul_data{$modulName}{small} .= "$prediction";
			}
			#print "PredictionnowSmall:$modul_data{$modulName}{small}\n";
		}
	}
	if ( $line =~ /^Alis/ )   { next; }
	if ( $line =~ /^Score=/ ) {              # suck in the score for the current module
		$identity = 0; ########################################
		if ( $line =~ /identity=\d+/ ) {
			$identity = $&;
			$identity =~ s/identity=//g;
		} else {
			print "no word identity found, exiting...\n";
			exit;
		}
		if ( exists $modul_data{$modulName}{ident} ) {
			print "for modul $modulName there exists already an entry ident. exiting now...\n";
			exit;
		} else {
			$modul_data{$modulName}{ident} = $identity;
		}
		#print "Ident:$identity:\n";
		if ( $line =~ /_m\d+__/ ) {
			$AliSpec = $'; ##################################################################
			if ($AliSpec =~ m/\?/) {
				$AliSpec =~ s/\?//g; 
				print "IIIIIIIIIFragenzeichen found bei: $modulName\n";
			}
		} else {
			print "could not read the specificity of this matching line:$line\n exiting now...\n";
			exit;
		}

		if(!exists($modul_data{$modulName}{AliSpec})) {$modul_data{$modulName}{AliSpec}=$AliSpec;}
		else                                           {$modul_data{$modulName}{AliSpec}.="__".$AliSpec;}
		#print "AlispecNow:$modul_data{$modulName}{AliSpec}:\n";
	}
	
}
#######################################################################
# ENDE Einsaugen des Files des Predictors in Hash %modul_data
# keys: $modulName
# values: AchtA	Stac ambiguity not:$rareSpec large small ident AliSpec
#######################################################################


print "\"comparison\",\"module\",\"8A-Code\",\"Stac-Code\",\"ambiguityWithinSVMPred\",\"rareSpec?\",\"SVMPredLarge\",\"SVMPredSmall\",\"identity\",\"PredBasedOn10aa\"\n";
foreach my $key ( keys %modul_data ) {    
# check for incompatible predictions.

	#print "CurrentKey:$key:\n";

	if (             (defined($modul_data{$key}{large})||defined($modul_data{$key}{small}))        &&       (defined($modul_data{$key}{AliSpec}))        )
	{
		my $CompatibilityBetweenTheTwoPredictors = 0;
		my $rareSpec            = 1;
		foreach my $largeOrSmall ( "large", "small" ) {
			if ( !defined( $modul_data{$key}{$largeOrSmall} ) ) { next; }
			my $AliSpec_tmp = $modul_data{$key}{AliSpec};
			$AliSpec_tmp = lc($AliSpec_tmp);
			$AliSpec_tmp =~ s/ser-thr/ser/g;
			$AliSpec_tmp =~ s/valhyphaa/val/g;
			my @split_alispec = split( /_or_|__/, $AliSpec_tmp );
			for ( my $i = 0 ; $i < @split_alispec ; $i++ ) {
				if ( $modul_data{$key}{$largeOrSmall} =~ $split_alispec[$i] ) {
					$CompatibilityBetweenTheTwoPredictors = 1;
				}
				my $all_my_specificities_temp = "aad=asp=asn=glu=gln=aad=asp=asn=cys=dhb=sal=dhpg=dpg=hpg=glu=gln=gly=ala=gly=ala=val=leu=ile=abu=iva=orn=lys=arg=orn=phe=trp=phg=tyr=bht=pro=pro=pip=ser=thr=dhpg=dpg=hpg=ser=thr=val=leu=ile=abu=iva";
				if ( $all_my_specificities_temp =~ $split_alispec[$i] ) {
					$rareSpec = 0;
				}
			}
		}
		if ($CompatibilityBetweenTheTwoPredictors == 0) { # might be: $rareSpec == 1
			print "\"Incompatible\",\"$key\",\"$modul_data{$key}{AchtA}\",\"$modul_data{$key}{Stac}\",\"$modul_data{$key}{ambiguity}\",\"$rareSpec\",\"$modul_data{$key}{large}\",\"$modul_data{$key}{small}\",\"$modul_data{$key}{ident}\",\"$modul_data{$key}{AliSpec}\"\n";
		}
		if ( $CompatibilityBetweenTheTwoPredictors == 1) {
			print "\"Compatible\",\"$key\",\"$modul_data{$key}{AchtA}\",\"$modul_data{$key}{Stac}\",\"$modul_data{$key}{ambiguity}\",\"$rareSpec\",\"$modul_data{$key}{large}\",\"$modul_data{$key}{small}\",\"$modul_data{$key}{ident}\",\"$modul_data{$key}{AliSpec}\"\n";
		}		
	}

# SVMPredictorSilent
	if (      !defined($modul_data{$key}{large})     &&     !defined($modul_data{$key}{small}) && defined($modul_data{$key}{AliSpec})        )
	{
		my $AliSpec_tmp = $modul_data{$key}{AliSpec};
		$AliSpec_tmp = lc($AliSpec_tmp);
		$AliSpec_tmp =~ s/ser-thr/ser/g;
		$AliSpec_tmp =~ s/valhyphaa/val/g;
		my @split_alispec = split( /_or_|__/, $AliSpec_tmp );
		my $rareSpec            = 1;
		for ( my $i = 0 ; $i < @split_alispec ; $i++ ) {
			my $all_my_specificities_temp = "aad=asp=asn=glu=gln=aad=asp=asn=cys=dhb=sal=dhpg=dpg=hpg=glu=gln=gly=ala=gly=ala=val=leu=ile=abu=iva=orn=lys=arg=orn=phe=trp=phg=tyr=bht=pro=pro=pip=ser=thr=dhpg=dpg=hpg=ser=thr=val=leu=ile=abu=iva";
			if ( $all_my_specificities_temp =~ $split_alispec[$i] ) {
				$rareSpec = 0;
			}
		}

	
	
			print "\"SVMPredictorSilent\",\"$key\",\"$modul_data{$key}{AchtA}\",\"$modul_data{$key}{Stac}\",\"$modul_data{$key}{ambiguity}\",\"$rareSpec\",\"$modul_data{$key}{large}\",\"$modul_data{$key}{small}\",\"$modul_data{$key}{ident}\",\"$modul_data{$key}{AliSpec}\"\n";
			#print "MyPredictorSilent: Modul:$key:Ident:$modul_data{$key}{ident}:AliSpec:$modul_data{$key}{AliSpec}\n";
	}

# BothPredictorsSilent
	if (!defined( $modul_data{$key}{large})
		 && !defined( $modul_data{$key}{small} )
		 && !defined( $modul_data{$key}{AliSpec} ) )
	{
#		print "BothSilent: Modul:$key:AchtA:$modul_data{$key}{AchtA}:stac:$modul_data{$key}{Stac}\n";
			print "\"BothSilent\",\"$key\",\"$modul_data{$key}{AchtA}\",\"$modul_data{$key}{Stac}\",\"$modul_data{$key}{ambiguity}\",\"\",\"$modul_data{$key}{large}\",\"$modul_data{$key}{small}\",\"$modul_data{$key}{ident}\",\"$modul_data{$key}{AliSpec}\"\n";	
	}


# StacPredictorSilent (there used to be a distinction...)
	if (    (  (defined($modul_data{$key}{large})) || (defined( $modul_data{$key}{small}))  ) && (!defined($modul_data{$key}{AliSpec}))   )
	{
		if ( defined( $modul_data{$key}{large} )
			 && !defined( $modul_data{$key}{small} ) )
		{
			# No distinction anymore
			#print "StacPredictorSilent1: Modul:$key:large:$modul_data{$key}{large}\n";
		}
		if ( !defined( $modul_data{$key}{large} )
			 && defined( $modul_data{$key}{small} ) )
		{
			# No distinction anymore
			#print "StacPredictorSilent2: Modul:$key:small:$modul_data{$key}{small}\n";
		}
		if (    defined( $modul_data{$key}{large} )
			 && defined( $modul_data{$key}{small} ) )
		{
			#my @small = split( /=|__/, $modul_data{$key}{small} );
			#my @large = split( /=|__/, $modul_data{$key}{large} );
			#my $SVMPredictionsLargeAndSmallHaveIntersection = 0;
			#for ( my $s = 0 ; $s < @small ; $s++ ) {
			#	for ( my $l = 0 ; $l < @large ; $l++ ) {
			#		if ( $small[$s] =~ $large[$l] ) { $SVMPredictionsLargeAndSmallHaveIntersection = 1; }
			#	}
			#}
			# No distinction anymore
			# print "StacPredictorSilent3:CorrespondanceFound:$SVMPredictionsLargeAndSmallHaveIntersection:Modul:$key:large:$modul_data{$key}{large}:small:$modul_data{$key}{small}\n";
			
		}
		print "\"StacPredictorSilent\",\"$key\",\"$modul_data{$key}{AchtA}\",\"$modul_data{$key}{Stac}\",\"$modul_data{$key}{ambiguity}\",\"\",\"$modul_data{$key}{large}\",\"$modul_data{$key}{small}\",\"$modul_data{$key}{ident}\",\"$modul_data{$key}{AliSpec}\"\n";	
	}
}
