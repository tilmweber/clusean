###########################################################################################################
#╔═════════════════════════════════════════════════════════════════════════════════════════════════════════╗
#║ This Perl Module
#║	* reads an hmmpfamFile (multiple hmms were run on multiple fasta seqs): <complete path>/$hmmpfamFile
#║	* writes all seqIDs (Fasta-Sequence names) to array: \@seqID_array
#║	* writes all discriptions for them in a hash: \%seqID2Description_hash
#║	* writes for each seqID an array of detected HMMs: \%seqID2ArrayOfDetectedHMMs
#║	*  └--> this array is an array of strings, each string contains the info
#║	*       for one detected HMM domain seperated with 3 tabs:
#║	*       $domainName, $modulNumber, $totalModulNumbers, $startPos, $endPos, $score, $topline, $downline
#╚═════════════════════════════════════════════════════════════════════════════════════════════════════════╝
############################################################################################################
#   Note:
#   hmm names are only alowed to contain
# 	characters that can be part of a word [a-zA-Z_0-9]
#	points (.)
#	and minus (-)
use strict;

#how this sub is called:
#digestHmmpfam("$tempDir/$hmmpfamFile", \@seqID_array, \%seqID2Description_hash, \%seqID2ArrayOfDetectedHMMs);
# usage in the sub: $hmmpfamFileWithPath, @$seqID_array, %$seqID2Description_hash, %$seqID2ArrayOfDetectedHMMs) = @_;
sub digestHmmpfam
{
	my ( $hmmpfamFileWithPath, $seqID_array, $seqID2Description_hash,
		 $seqID2ArrayOfDetectedHMMs )
	  = @_;
	my @detectedHMMs_array;

	#print "hmmpfamFileWithPath:$hmmpfamFileWithPath\n";
	open( HMMEROUTFILE, $hmmpfamFileWithPath )
	  or die "cant open $hmmpfamFileWithPath\n";
	my $headflag  = 1;
	my $entryflag = 0;
	my $head      = "";
	my $entry     = "\n";
	while (<HMMEROUTFILE>)
	{
		my $line = $_;
		chomp($line);

		#print "line:$line\n";
		if ( $headflag == 1 )
		{
			$head = $head . $line . "\n";
			if ( $line eq "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" )
			{

				#print REDFILE $head;
				$headflag = 0;
			}
		}
		if ( $line =~ /^Query sequence/ || $entryflag == 1 )
		{
			#if($line =~ /^Query sequence/) {print "ZeieMitQuerySequence:$line\n";}
			$entryflag = 1;
			$entry     = $entry . $line . "\n";
			if ( $line =~ /^\/\// )
			{ # when a line starts with // that means the entire hmmpfam-report for one seq has been read
				$entryflag = 0;

				#print "ENTRY starting directly here:$entry";
				#============
				# analyse entry:
				$entry =~ /Query sequence: .+\n/;
				my $queryName = substr( $&, 16, length($&) - 1 );
				chomp($queryName);
############################################
				#$queryName
				#print "$queryName\n";
				push( @$seqID_array, $queryName );
############################################
				$entry =~ /Accession:      .+\n/;
				my $accession = substr( $&, 16, length($&) - 1 );
				chomp($accession);
				$entry =~ /Description:    .+\n/;
				my $description = substr( $&, 16, length($&) - 1 );
				chomp($description);
############################################
				#$description
				$$seqID2Description_hash{$queryName} = $description;
############################################
####????????????????????????????????????????
				if ( $entry =~ m/\[no hits above thresholds\]/ )
				{
					my @arrayOfDetectedHMMs;
					$arrayOfDetectedHMMs[0] = "[no hits above thresholds]";
					@{ $$seqID2ArrayOfDetectedHMMs{$queryName} } =
					  @arrayOfDetectedHMMs;
					#print "For your info:no hits at $queryName\n";
				}
				my $aliOfAllDomains = "";
				if ( $entry =~ /\b[-\w\.|]+:\s{1}domain \d+.+/s )
				{    # in Brackets: defining name of hmm
					$aliOfAllDomains = $&;
				} else
				{
					$aliOfAllDomains = "";
				}
				my $currentAMP_Domain = "";
				my %aliOfOneAMPDomain2BestFollowingLys517;
				my %domain2DomainScore;
				while ( $aliOfAllDomains =~ /\b[-\w\.|\\]+:\s{1}domain \d+ of \d+/gs )
				{

					#print "Matched:$&\n";
					my $domainInfo = $&;

					#print "DomainInfo:$domainInfo:\n";
					$domainInfo =~ m/domain \d+ of \d+/;
					my $domain_x_of = $&;
					my $domainName  = $`;
					$domainName =~ s/: //g;
############################################
					#$domainName
					my $temp_detectedHMM = "$domainName\t\t\t";
############################################
					my @domainNumber = split( /\s+/, $domain_x_of );

					#$domainNumber[1] = from, $domainNumber[3] = to
					$temp_detectedHMM .=
					  $domainNumber[1] . "\t\t\t" . $domainNumber[3] . "\t\t\t";
					$aliOfAllDomains =~
					  /from \d+ to \d+/g;    # match "from number to number"
					my @fromto = split( /\s+/, $& );

					#$fromto[1] /[3]
					$temp_detectedHMM .=
					  $fromto[1] . "\t\t\t" . $fromto[3] . "\t\t\t";
					$aliOfAllDomains =~
					  /score -*\d*\.\d+,/g;    # match "score (-)number.number,"
					my $domainScore =
					  substr( $&, 6, length($&) - 7 );    # extract score

					#$domainScore
					$temp_detectedHMM .= $domainScore . "\t\t\t";
					my $queryName_maskedBackslash = substr($queryName, 0, 10); # need to duplicate the backslash because the backslash will be interpreted in the reg ex.
					# take substr. because hmmpfam output contains only 10 positions for the entry-names in the alignments
					$queryName_maskedBackslash =~ s/\\/\\\\/g;
					$queryName_maskedBackslash =~ s/\|/\\\|/g;
					$aliOfAllDomains =~ /\*->.+?\*\n +[ \+\w]*? +?\n +?$queryName_maskedBackslash +?\S+? +?[- \w]+? +\S+ */gs;    # go from *->till <-* plus 2 more lines.
					my $aliOfCurrentDomain = $&;

  #print 	"###############################\n$aliOfCurrentDomain\n@@@@@@@@@@@\n";
  #$aliOfCurrentDomain = the current domain
                                        #$aliOfCurrentDomain;
                                        #$queryName;
                                        #$domainNumber[1];
                                        #$domainScore;
################################
					my $topline = '';
					my $downline = '';
					takealiOfCurrentDomainAndTypeAndExtractTopAndDownline(
								   $aliOfCurrentDomain, \$topline, \$downline );
					$temp_detectedHMM .= $topline . "\t\t\t" . $downline;

					#print "temp_detectedHMM:$temp_detectedHMM:end\n";
					my @arrayOfDetectedHMMs;

					#seqID2ArrayOfDetectedHMMs
					if ( exists $$seqID2ArrayOfDetectedHMMs{$queryName} )
					{
						@arrayOfDetectedHMMs =
						  @{ $$seqID2ArrayOfDetectedHMMs{$queryName} };
						push( @arrayOfDetectedHMMs, $temp_detectedHMM )
						  ;    #print "push\n";
						@{ $$seqID2ArrayOfDetectedHMMs{$queryName} } =
						  @arrayOfDetectedHMMs;
					} else
					{
						$arrayOfDetectedHMMs[0] = $temp_detectedHMM;
						@{ $$seqID2ArrayOfDetectedHMMs{$queryName} } =
						  @arrayOfDetectedHMMs;
					}
				}
				$entry = "";
			}
		}
	}
	close(HMMEROUTFILE);
}

sub takealiOfCurrentDomainAndTypeAndExtractTopAndDownline
{
	my ( $aliOfCurrentDomain, $topline, $downline ) = @_;
	my @aliOfCurrentDomainArray = split( /\n/, $aliOfCurrentDomain );
	for ( my $i = 0 ; $i < @aliOfCurrentDomainArray ; $i++ )
	{
		if ( $i % 4 == 0 )
		{
			$aliOfCurrentDomainArray[$i] =~ s/^\s+//g;    #remove leading spaces
			$aliOfCurrentDomainArray[$i] =~ s/\*->//g;    #remove leading *->
			$aliOfCurrentDomainArray[$i] =~ s/\s*$//g;    #remove ending spaces
			$aliOfCurrentDomainArray[$i] =~ s/<-\*$//g;   #remove ending spaces
			$$topline = $$topline . $aliOfCurrentDomainArray[$i];
		}
		if ( ( $i - 2 ) % 4 == 0 )
		{
			$aliOfCurrentDomainArray[$i] =~ s/^\s+\S+\s+\d*-*\s+//g
			  ; #remove leading spaces queryName(=one or more non Whitespaces) and numbers
			$aliOfCurrentDomainArray[$i] =~
			  s/\s*\d*-*\s*$//g;    #remove ending spaces and numbers
			$$downline = $$downline . $aliOfCurrentDomainArray[$i];
		}
	}
}
1;
