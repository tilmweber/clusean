use strict;
use warnings;


#this sub calculates mean and std_dev for a given array of numbers
sub std_dev{
my ($array, $mean, $std_dev, $min, $max) = @_;
my $array_length=@$array;
my $array_sum=0;
my $array_mean;
my $std_inner_sum=0;
my $array_std_dev;
my $array_min=0;
my $array_max=0;

	#get mean and array_sum
	for (my $i=0; $i<@$array; $i++) {
		$array_sum+=$$array[$i];
	}
	$array_mean=$array_sum/$array_length;
	
	#get std_dev
	for(my $i=0; $i<@$array; $i++) {
		$std_inner_sum+= ($$array[$i]-$array_mean)*($$array[$i]-$array_mean)
	}
	$array_std_dev=sqrt((1/$array_length) * $std_inner_sum);
	
	#get min and max
	my @sorted_array = sort {$a <=> $b} (@$array);
	$array_min= $sorted_array[0];
	$array_max= $sorted_array[(@sorted_array)-1];	
	
	
$$min=$array_min;
$$max=$array_max;
$$mean=$array_mean;
$$std_dev=$array_std_dev;
}

1;
