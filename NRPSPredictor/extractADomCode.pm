use strict;
my $nameOfAMPdomainHMM = "aa-activating-core.198-334";
my $nameOflys517       = "aroundLys517";

sub extractCode
{
	my ( $topline, $downline, $extractionType, $returnString8A,
		 $returnStringStac, $HTML_ali )
	  = @_;
	my @removedInsert_array;
	my @removedInsertChar_array;

 # remove all insertion-characters from topline and same positions from downline
 # remember these positions and chars in $removedInsert and $removedInsertCharr
	while ( $$topline =~ /\.+?/g )
	{
		$$topline = substr( $$topline, 0, $-[0] ) . substr( $$topline, $+[0] );
		push( @removedInsertChar_array, substr( $$downline, $-[0], 1 ) );
		$$downline =
		  substr( $$downline, 0, $-[0] ) . substr( $$downline, $+[0] );
		push( @removedInsert_array, $-[0] )
		  ;    # removal took place after "human" position <given>
	}
	if ( $extractionType eq $nameOfAMPdomainHMM )
	{
		my $phealine ="KGTMLEHKGISNLKVFFEN---SLN----V-TEKDRIGQFAS-ISFDASVWEMFMALLTGASLYIILKD---TINDFVKFEQYINQKEITVITLPPTYVVHLD-----------PERILSIQTLITAGSATSPSLVNKWKEKV------TYINAYGPTETTICATT";
		my @topline_array  = split( //, $$topline );
		my @phealine_array = split( //, $phealine );
		my @downline_array = split( //, $$downline );
		$$topline =~ m/KGVmveHrnvvnlvkwl/;    #198-214
		my $zero                     = ( $-[0] );
		my @matchingInterval         = ( ( $-[0] ) .. ( $+[0] - 1 ) );
		my @positionsToBeExtracted8A = ( $zero + 12, $zero + 15, $zero + 16 )
		  ;    # 012=210, 015=213, 016=214: also 210+213-214
		my @positionsToBeExtractedStac = ();    # the Stachelhaus-Code has no pos in this matching area
		do_fillReturnString_for8AandStac_changeHTMLColors(
						$returnString8A,            $returnStringStac,
						$zero,                      $phealine,
						$topline,                   $downline,
						\@phealine_array,           \@topline_array,
						\@downline_array,           \@matchingInterval,
						\@positionsToBeExtracted8A, \@positionsToBeExtractedStac
		);
		$$topline =~ m/LqfssAysFDaSvweifgaLLnGgt/;    #227-231,A,232-250
		my $zero = ( $-[0] );
		my @matchingInterval = ( ( $-[0] ) .. ( $+[0] - 1 ) );
		my @positionsToBeExtracted8A = (
								 $zero + 3,  $zero + 8,  $zero + 9,  $zero + 10,
								 $zero + 11, $zero + 12, $zero + 13, $zero + 14,
								 $zero + 17
								 		);    #003=230, 008=234 - 014=240, 017=243: also 230+234-240+243
		my @positionsToBeExtractedStac = ( $zero + 9, $zero + 10, $zero + 13 );    #009=235, 010=236, 013=239
		do_fillReturnString_for8AandStac_changeHTMLColors(
						$returnString8A,            $returnStringStac,
						$zero,                      $phealine,
						$topline,                   $downline,
						\@phealine_array,           \@topline_array,
						\@downline_array,           \@matchingInterval,
						\@positionsToBeExtracted8A, \@positionsToBeExtractedStac
		);
		$$topline =~ m/iTvlnltPsl/;#274-283
		my $zero                     = ( $-[0] );
		my @matchingInterval         = ( ( $-[0] ) .. ( $+[0] - 1 ) );
		my @positionsToBeExtracted8A = ( $zero + 4, $zero + 5 );#004=278: also 278+279
		my @positionsToBeExtractedStac = ( $zero + 4 );    #004=278
		do_fillReturnString_for8AandStac_changeHTMLColors(
						$returnString8A,            $returnStringStac,
						$zero,                      $phealine,
						$topline,                   $downline,
						\@phealine_array,           \@topline_array,
						\@downline_array,           \@matchingInterval,
						\@positionsToBeExtracted8A, \@positionsToBeExtractedStac
		);
		$$topline =~ m/LrrvlvGGEaL/;                       #295-305
		my $zero                     = ( $-[0] );
		my @matchingInterval         = ( ( $-[0] ) .. ( $+[0] - 1 ) );
		my @positionsToBeExtracted8A =
		  ( $zero + 4, $zero + 5, $zero + 6, $zero + 7, $zero + 8 )
		  ;                                                #004=299 bis 008=303
		my @positionsToBeExtractedStac =
		  ( $zero + 4, $zero + 6 );                        #004=299, 006=301
		do_fillReturnString_for8AandStac_changeHTMLColors(
						$returnString8A,            $returnStringStac,
						$zero,                      $phealine,
						$topline,                   $downline,
						\@phealine_array,           \@topline_array,
						\@downline_array,           \@matchingInterval,
						\@positionsToBeExtracted8A, \@positionsToBeExtractedStac
		);
		$$topline =~ m/liNaYGPTEtTVcaTi/;                  #319-334
		my $zero = ( $-[0] );
		my @matchingInterval = ( ( $-[0] ) .. ( $+[0] - 1 ) );
		my @positionsToBeExtracted8A = (
								 $zero + 1,  $zero + 2,  $zero + 3,  $zero + 4,
								 $zero + 5,  $zero + 6,  $zero + 7,  $zero + 8,
								 $zero + 9,  $zero + 10, $zero + 11, $zero + 12,
								 $zero + 13, $zero + 14, $zero + 15
		);    #001=320 for length 15: also 320-334
		my @positionsToBeExtractedStac =
		  ( $zero + 3, $zero + 11, $zero + 12 );    #003=322, 011=330, 012=331
		do_fillReturnString_for8AandStac_changeHTMLColors(
						$returnString8A,            $returnStringStac,
						$zero,                      $phealine,
						$topline,                   $downline,
						\@phealine_array,           \@topline_array,
						\@downline_array,           \@matchingInterval,
						\@positionsToBeExtracted8A, \@positionsToBeExtractedStac
		);

# re-insert all insertion-characters into topline and the characters that have beeen removed from downline
		for ( my $index = ( @removedInsert_array - 1 ) ;
			  $index >= 0 ; $index-- )
		{
			splice( @phealine_array, $removedInsert_array[$index],0, '.' );
			splice( @topline_array, $removedInsert_array[$index],0, $removedInsertChar_array[$index] );
			splice( @downline_array, $removedInsert_array[$index],0, $removedInsertChar_array[$index] );
		}
		for ( my $i = 0 ; $i < @phealine_array ; $i++ )
		{
			$$HTML_ali .= $phealine_array[$i];
		}
		$$HTML_ali .= "<br>\n";
		for ( my $i = 0 ; $i < @topline_array ; $i++ )
		{
			$$HTML_ali .= $topline_array[$i];
		}
		$$HTML_ali .= "<br>\n";
		for ( my $i = 0 ; $i < @downline_array ; $i++ )
		{
			$$HTML_ali .= $downline_array[$i];
		}
		#$$HTML_ali .= "<br>\n";
	}
	if ( $extractionType eq $nameOflys517 )
	{
		my $phealine = "PLEQLRQFSSEELPTYMIPSY--FIQLDKMPLTSNGKIDRKQLPEPDLTF";
		my @topline_array  = split( //, $$topline );
		my @phealine_array = split( //, $phealine );
		my @downline_array = split( //, $$downline );
		$$topline =~ m/fvvLdalPLTpNGKlDRkALPaPd/;
		my $zero                       = ( $-[0] );
		my @matchingInterval           = ( ( $-[0] ) .. ( $+[0] - 1 ) );
		my @positionsToBeExtracted8A   = ();
		my @positionsToBeExtractedStac = ( $zero + 13 );
		do_fillReturnString_for8AandStac_changeHTMLColors(
						$returnString8A,            $returnStringStac,
						$zero,                      $phealine,
						$topline,                   $downline,
						\@phealine_array,           \@topline_array,
						\@downline_array,           \@matchingInterval,
						\@positionsToBeExtracted8A, \@positionsToBeExtractedStac
		);

# re-insert all insertion-characters into topline and the characters that have beeen removed from downline
		for ( my $index = ( @removedInsert_array - 1 ) ;
			  $index >= 0 ; $index-- )
		{
			splice( @phealine_array, $removedInsert_array[$index], 0, '.' );
			splice( @topline_array, $removedInsert_array[$index],
					0, $removedInsertChar_array[$index] );
			splice( @downline_array, $removedInsert_array[$index],
					0, $removedInsertChar_array[$index] );
		}
		for ( my $i = 0 ; $i < @phealine_array ; $i++ )
		{
			$$HTML_ali .= $phealine_array[$i];
		}
		$$HTML_ali .= "<br>\n";
		for ( my $i = 0 ; $i < @topline_array ; $i++ )
		{
			$$HTML_ali .= $topline_array[$i];
		}
		$$HTML_ali .= "<br>\n";
		for ( my $i = 0 ; $i < @downline_array ; $i++ )
		{
			$$HTML_ali .= $downline_array[$i];
		}
		#$$HTML_ali .= "<br>\n";
	}
}

sub fillReturnString
{
	my ( $returnString8A, $downline, $positionsToBeExtracted ) = @_;
	if ( @$positionsToBeExtracted > 0 )
	{
		foreach my $position (@$positionsToBeExtracted)
		{
			$$returnString8A .= substr( $$downline, $position, 1 );
		}
	} else
	{
		$$returnString8A;
	}
}

sub setCharToHTMLColor
{    # accepts colors red, green, blue
	my ( $char, $color ) = @_;
	my $openingHTMLColorTagRed   = "<span style=\"color: rgb(255, 0, 0);\">";
	my $openingHTMLColorTagGreen = "<span style=\"color: rgb(0, 255, 0);\">";
	my $openingHTMLColorTagBlue  = "<span style=\"color: rgb(0, 0, 255);\">";
	my $closingHTMLColorTag      = "</span>";
	if ( $color eq "red" )
	{
		$char =~ s/<.*?>//g;    #remove HTML tags before and after the char
		$char = $openingHTMLColorTagRed . $char . $closingHTMLColorTag;
	}
	if ( $color eq "green" )
	{
		$char =~ s/<.*?>//g;    #remove HTML tags before and after the char
		$char = $openingHTMLColorTagGreen . $char . $closingHTMLColorTag;
	}
	if ( $color eq "blue" )
	{
		$char =~ s/<.*?>//g;    #remove HTML tags before and after the char
		$char = $openingHTMLColorTagBlue . $char . $closingHTMLColorTag;
	}
	return
	  $char
	  ; # note: if color is neither red, green, or blue the char is returned unchanged
}

sub do_fillReturnString_for8AandStac_changeHTMLColors
{
	my (
		 $returnString8A,           $returnStringStac,
		 $zero,                     $phealine,
		 $topline,                  $downline,
		 $phealine_array,           $topline_array,
		 $downline_array,           $matchingInterval,
		 $positionsToBeExtracted8A, $positionsToBeExtractedStac
	  )
	  = @_;

#      ref (string)       ref(string)     numb    ref(str)   ref(str)  ref(str)   ref(array)                ref(array)                 ref(array)
	fillReturnString( $returnString8A, $downline, $positionsToBeExtracted8A )
	  ; # Arguments: returnString and downline are references, ($-[0]) is the zeroposition
	fillReturnString( $returnStringStac, $downline,
					  $positionsToBeExtractedStac )
	  ; # Arguments: returnString and downline are references, ($-[0]) is the zeroposition
	changeHTMLColors( $phealine_array, "blue",  $matchingInterval );
	changeHTMLColors( $topline_array,  "blue",  $matchingInterval );
	changeHTMLColors( $downline_array, "blue",  $matchingInterval );
	changeHTMLColors( $phealine_array, "green", $positionsToBeExtracted8A );
	changeHTMLColors( $topline_array,  "green", $positionsToBeExtracted8A );
	changeHTMLColors( $downline_array, "green", $positionsToBeExtracted8A );
	changeHTMLColors( $phealine_array, "red",   $positionsToBeExtractedStac );
	changeHTMLColors( $topline_array,  "red",   $positionsToBeExtractedStac );
	changeHTMLColors( $downline_array, "red",   $positionsToBeExtractedStac );
}

sub changeHTMLColors
{
	my ( $line_array, $color, $interval ) = @_;
	foreach my $position (@$interval)
	{
		$$line_array[$position] =
		  setCharToHTMLColor( $$line_array[$position], $color );
	}
}
1;
