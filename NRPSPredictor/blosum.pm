use strict;
use warnings;

sub blosum {
	my ($doppelHash_reference, $scriptHomeDir) = @_;
	open( BLOSUM, "$scriptHomeDir/BLOSUM62" ) or die "cant open BLOSUM62\n";
	my @blosum = <BLOSUM>;
	close(BLOSUM);
	my @aa;
	my $zero;

	my $columTitleLineRead = 0;
	for ( my $i = 0 ; $i < @blosum ; $i++ ) {
		$blosum[$i] =~ s/^\s+//g;
		if ( $blosum[$i] =~ /^#/) { next; }
		if ( $blosum[$i] =~ /A/ && $blosum[$i] =~ /R/ && $blosum[$i] =~ /N/ && $blosum[$i] =~ /D/ && $blosum[$i] =~ /C/ ) {			
			@aa = split( /\s+/, $blosum[$i] );
			$zero=$i+1;
			$columTitleLineRead=1;
			next;
		}
		if ($columTitleLineRead == 1 && $i<=($zero+(@aa))) {
			my @currentLine = split( /\s+/, $blosum[$i] );
			for (my $j=1; $j<@currentLine; $j++) {
				$$doppelHash_reference{$aa[$i-$zero]}{$aa[$j-1]}= $currentLine[$j];
				#print "Line:$aa[$i-$zero]:Column:$aa[$j-1]:$currentLine[$j]:";
				
			}
			#print "\n";
		}
		
#		foreach my $keyx (keys %$doppelHash_reference) {
#			foreach my $keyy (keys %{$$doppelHash_reference{$keyx}}) {
#				print "x:$keyx:y:$keyy:$$doppelHash_reference{$keyx}{$keyy}\n";
#				
#			}	
#		}

	}

}

sub alignTwoStrings {
	my ($string1, $string2, $score, $identity, $matchString, $scriptHomeDir) = @_; # all variables are references except $scriptHomeDir
	if (length($$string1)!=length($$string2)) { print "Problem at sub alignTwoStrings in blosum.pm: string1 ($$string1) and string2 ($$string2) differ in length\n"; exit;}
	$$score=0;
	my $identicalPos=0;
	$$matchString="";
	my %doppelHash;
	blosum(\%doppelHash, $scriptHomeDir);
	
	for (my $i=0; $i<length($$string1); $i++) {
			my $string_q = substr($$string1, $i, 1);
			my $string_s = substr($$string2, $i, 1);
				if ($string_q eq "-") {$string_q = "*";}
				if ($string_s eq "-") {$string_s = "*";}				
			my $tempScore = $doppelHash{$string_q}{$string_s};
			if ($tempScore>0) {
				if($string_q eq $string_s)  {$$matchString.="|"; $identicalPos++;}
				else {$$matchString.=":";}
			}
			else {$$matchString.="&nbsp;";} # ... score <= 0 "&nbsp;" is html-code for a blank
			
			$$score=$$score+$tempScore;
	}
	$$identity=100*($identicalPos/length($$string1));		
}

1;
