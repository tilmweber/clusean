#!/usr/bin/perl
# Dieses Programm liest ein Fasta-File als Parameter ein
# und startet stretcher gegen alle Fasta-Entries in einer DB (Multi-Fasta-File)

use blosum;

use strict;
use warnings;
##############################
# This module expects the Database of stac-codes (signatures) in the $scriptHomeDir
my $signatures_file = "ID-liste.all.7.fasta";
##############################
sub alignToSignatureDB {

	#	alignToSignatureDB($hmmpfamFile, $tempDir, $scriptHomeDir, $code_stac, $stringBuffer);
	my ( $hmmpfamFile, $tempDir, $scriptHomeDir, $code_stac, $stringBuffer) = @_;
	my @QUERY;
	$QUERY[0]=">code_stac\n";
	$QUERY[1]="$code_stac\n";
	
	open( DB, "$scriptHomeDir/$signatures_file") or die "can not open signatures_file";
	my @DB = <DB>;
	close(DB);
	
	my $tempSeq = "";
	my $seqName = "";

	
	my %DBName_Seq_hash; # filled in the overnext for-loop	
	my %QueryName_Seq_hash; # filled in the next for-loop
	for ( my $i = 0 ; $i < @QUERY ; $i++ ) { # this for-loop is historical. 
						#in a former version of the program it was used to extract the fasta entries from a multi-fasta-file
		chomp( $QUERY[$i] );
		if ( $QUERY[$i] =~ /^>.*\b/ )
		{    # wenn mit der aktuellen Zeile ein neuer Fasta-Eintrag beginnt ...
			$seqName = substr( $&, 1 );    # ... dann speichere den neuen Sequenznamen
			if ( exists $QueryName_Seq_hash{$seqName} ) {
				# name gibt's schon:
				print "Sequence $seqName already exists, please remove duplicates!\nI'm going to use only the last occuring sequence in this file with this name\n";
			}
			$QueryName_Seq_hash{$seqName} = ""; # alter Sequenzeintrag unter diesem Schl�ssel (Name) wird gel�scht, bzw. falls neu auf "" initialisiert.
		} else { # seqName gerade gelesen, append sequence
			$QueryName_Seq_hash{$seqName} = $QueryName_Seq_hash{$seqName}.$QUERY[$i];
		}
	}
	for ( my $i = 0 ; $i < @DB ; $i++ ) {
		chomp( $DB[$i] );
		if ( $DB[$i] =~ /^>.*\b/ )
		{    # wenn mit der aktuellen Zeile ein neuer Fasta-Eintrag beginnt ...
			$seqName =
			  substr( $&, 1 );    # ... dann speichere den neuen Sequenznamen
			$seqName =~ s/\//_or_/g; # replace slashes in the name with _or_
			if ( exists $DBName_Seq_hash{$seqName} ) {

				# name gibt's schon:
				print "Sequence $seqName already exists, please remove duplicates!\nI'm going to use only the last occuring sequence in this file with this name\n";
			}
			$DBName_Seq_hash{$seqName} = ""; # alter Sequenzeintrag unter diesem Schl�ssel (Name) wird gel�scht, bzw. falls neu auf "" initialisiert.
		} else {    # seqName gerade gelesen, append sequence
			$DBName_Seq_hash{$seqName} = $DBName_Seq_hash{$seqName} . $DB[$i];
		}
	}
	foreach my $querykey ( keys %QueryName_Seq_hash ) {
		my %score2aliData;
		my $string1 = $QueryName_Seq_hash{$querykey};
		foreach my $dbkey ( keys %DBName_Seq_hash ) {
			my $string2 = $DBName_Seq_hash{$dbkey};
			my $score;
			my $identity;
			my $matchString="";

			alignTwoStrings(\$string1, \$string2, \$score, \$identity, \$matchString, $scriptHomeDir);
#			print "String1:    $string1\n";
#			print "matchString $matchString\n";
#			print "String2:    $string2\n";
#			print "Score:$score\n";
#			print "Ident:$identity\n";
			
			my $ali="Score=$score, identity=$identity with subject $dbkey\n<br><font face=\"Courier New\">$string1<br>\n$matchString<br>\n$string2</font><br>\n<br>\n";
#			print "Ali:$ali\n";
#			print "===\n";
			if ($identity>=70) {
				if (exists $score2aliData{$score}) {$score2aliData{$score}.=$ali;}
				else {$score2aliData{$score}=$ali;}
			}
		}		
		my @keys = (sort numerically (keys %score2aliData));
		
		for (my $i=0; $i<2 && $i<@keys; $i++) {
			$$stringBuffer.= $score2aliData{$keys[$i]};			
		}
		#print "StringBuffer:$$stringBuffer\n";
	}
	foreach my $dbkey ( keys %DBName_Seq_hash ) {
		unlink("$tempDir/$dbkey.fasta");
	}
  }
  
 #============================================
  
	sub numerically {
		$b <=> $a
	}
1;
