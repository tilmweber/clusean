<?xml version="1.0" encoding="UTF-8"?>

<resource xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <tATKSpred type="BestHMMerHit">
        <Prerequisite type="profileHit" program="hmmscan" CaptureConsole="TRUE">
        <!--Currently, the location of the hmmscan binary and the database location is inferred from the CLUSEAN.cfg
configuration file! Maybe in a future version we might want to put it here...-->
			<!-- If prefixes are required you can set them here
		        
            <inputfileprefix></inputfileprefix>
            <outputfileprefix></outputfileprefix>
            <dbprefix></dbprefix>
            
            -->
            <parameters>--cpu 1</parameters><database>/home/clusean/CLUSEAN/lib/PKSI-KS.hmm</database><primary_tag_type>CDS_motif</primary_tag_type><tag>label</tag><tag_value>PKSI-KS</tag_value>
        </Prerequisite>
        <Execute program="hmmscan" CaptureConsole="TRUE">
<!--Currently, the location of the hmmscan binary and the database location is inferred from the CLUSEAN.cfg
configuration file! Maybe in a future version we might want to put it here...-->
			<!-- If prefixes are required you can set them here
		        
            <inputfileprefix></inputfileprefix>
            <outputfileprefix></outputfileprefix>
            <dbprefix></dbprefix>
            
            -->
            <parameters>--cpu 1</parameters>
            <database>/home/clusean/CLUSEAN/lib/KStypes.hmm</database>
            </Execute>
    <description>Pediction of tAT-PKSI KS specificities according to Nguyen et  al., 2008</description>
    <referenceList>
    	<reference>Nguyen, T., K. Ishida, H. Jenke-Kodama, E. Dittmann, C. Gurgui, T. Hochmuth, S. Taudien, M. Platzer, C. Hertweck, and J. Piel. 2008. Exploiting the mosaic structure of trans-acyltransferase polyketide synthases for natural product discovery and pathway dissection. Nat. Biotechnol. 26:225-233.</reference></referenceList>
    </tATKSpred></resource>
