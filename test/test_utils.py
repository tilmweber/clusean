#!/usr/bin/env python
#
# Test pipeline helpers for CLUSEAN
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import sys
from nose.tools import *

sys.path.append('.')
sys.path.append('..')
from CLUSEAN.utils import *

def test_return_value():
    args = ["true"]
    assert_equal(dispatch(args), 0)

    args = ["false"]
    assert_equal(dispatch(args), 1)

def test_filter_files():
    file_list = ["test.foo", "test.bar"]
    assert_equal(filter_files(file_list, ".foo"), ["test.foo"])

def test_tmp_file_pattern():
    pattern = tmp_file_pattern("foo", 42)
    expected = "foo.substep%02d.step42.embl"
    assert_equal(pattern, expected)
