#!/usr/bin/env python
#
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import sys
from nose.tools import *

sys.path.append('.')
sys.path.append('..')
from CLUSEAN.parseconfig import *

def test_minimal():
    lines = [
        "CLUSEANROOT = /this/is/a/test/path\n",
        "<Environment>\n",
        "\tPATH=/this/is/a/test/path/too\n",
        "</Environment>\n"]
    pc = ParseConfig()
    pc.parse(lines)

    # There should always be a Global key
    assert_true(pc.config.has_key("Global"))

    # We added an Environment section, there should be a key for that
    assert_true(pc.config.has_key("Environment"))

    # There is no Invalid section, so there shouldn't be a key for this
    assert_false(pc.config.has_key("Invalid"))

    # There's a CLUSEANROOT set in the general part
    assert_true(pc.config["Global"].has_key("CLUSEANROOT"))
    assert_equal(pc.config["Global"]["CLUSEANROOT"], "/this/is/a/test/path")

    # The Environment section sets a PATH
    assert_true(pc.config["Environment"].has_key("PATH"))
    assert_equal(pc.config["Environment"]["PATH"], "/this/is/a/test/path/too")

def test_multiple_options():
    lines = [
        "<Environment>\n",
        "\tPATH=/this/is/a/test/path\n",
        "\tPATH=/and/another/path\n",
        "</Environment>\n"]
    pc = ParseConfig()
    pc.parse(lines)

    # There should be one PATH with the values from both variables
    assert_true(pc.config["Environment"].has_key("PATH"))
    assert_equal(pc.config["Environment"]["PATH"], "/this/is/a/test/path:/and/another/path")


def test_comments():
    lines = [
        "# This is a comment line\n",
        "CLUSEANROOT = /this/is/a/test/path\n",
        "<Environment>\n",
        "# This is also a comment line\n",
        "\tPATH=/this/is/a/test/path/too\n",
        "\tCPUS=4 # and a line with a trailing comment\n",
        "</Environment>\n"]
    pc = ParseConfig()
    pc.parse(lines)

    # If something went wrong parsing the comment lines, we shouldn't get this
    assert_true(pc.config.has_key('Environment'))

    # Check if trailing comments work as well
    assert_true(pc.config['Environment'].has_key('CPUS'))
    assert_equal(pc.config['Environment']['CPUS'], "4")

def test_interpolation():
    lines = [
        "CLUSEANROOT = /this/is/the/root\n",
        "<Environment>\n",
        "\tPATH=$CLUSEANROOT/and/extended/path\n",
        "</Environment>\n"]
    pc = ParseConfig(interpolate=True)
    pc.parse(lines)

    # The CLUSEANROOT should be verbatim
    assert_equal(pc.config["Global"]["CLUSEANROOT"], "/this/is/the/root")

    # The PATH should be interpolated
    assert_equal(pc.config["Environment"]["PATH"], "/this/is/the/root/and/extended/path")
