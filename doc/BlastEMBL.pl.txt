NAME
    BlastEMBL.pl

DESCRIPTION
    Script based blasting of all CDS features in EMBL file

USAGE
    BlastEMBL.pl --input <input-EMBL-file> --output <output-EMBL-file>
    --blastpdir <directoryname> [--blastpdb <database name> --bLastparms
    <blastall parameters> --run --verbose --help --singleFASTA <filename>]

  PARAMETER description
        --input <filename>       : Name of the embl-file to process
        --output <filename>      : Name of the embl-output file
        --blastpdir <dirname>    : Directory to store blastfiles

   Optional parameters
        --blastpdb <database>    : Name of the BLAST database to use
                                   default: nr
        --blastparms <parameter> : Parameters for blastall execution
                                   default: -a2 -FF
        --singleFasta <filename> : In addtion to the fasta files in the --blastpdir, also
                                   generate a multi-Fastafile <filename>                          
        --run                    : Run blast analysis on local machine
        --verbose                : Verbose output
        --help                   : print help information

   (Optional) Environment Variables
       B<Name>                  B<defaults to>
       CLUSEANROOT                 /usr/local/abdb
       BLASTCMD                 nice -n 20 blastall
       BSEARCHDB                nr
       BPARMS                   -a2 -FF

REQUIREMENTS
    This script requires the following perl-modules installed on your
    system:

     Bioperl (Bio::SeqIO, Bio::SeqFeature::Generic)
     Getopt::Long

    For inline-blasting a working ncbi-blast package is required.

AUTHOR
     Tilmann Weber
     Microbiology/Biotechnology
     Auf der Morgenstelle28
     72076 Tuebingen
     Germany

     Email: tilmann.weber@biotech.uni-tuebingen.de

