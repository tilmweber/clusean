NAME
    annotateHMMHits.pl

SYNOPSIS
    annotateHMMHits.pl --input <input-EMBL-file> --output <output-EMBL-file>
    [--hmmertag HMMer_file --feattag CDS_motif --evalue 0.01 --score 1
    --verbose --help]

DESCRIPTION
    Enters data contained in CDS associated HMMer result files into
    EMBL-Style annotation.

  PARAMETERS
   --input <filename>                :       Input file (in EMBL-format)
   --output <filename>               :       Output filename (EMBL-format)
   --hmmertag <tag>                  :       Tag used for HMM-result-reference
                                             default: HMMer_file
   --feattag <tag>                   :       Tag used for feature annotation
                                             default: CDS_motif
   --evalue <number>                 :       E-Value treshold, default: 0.01
   --score  <number>                 :       minimal score, default 0
   --verbose                         :       verbose output
   --help                            :       print help
   AUTHOR
     Tilmann Weber
     Microbiology/Biotechnology
     Auf der Morgenstelle 28
     72076 Tuebingen
     Germany
     Email: tilmann.weber@biotech.uni-tuebingen.de

