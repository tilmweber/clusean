Name
    assignFunction.pl

SYNOPTSIS
    assignFunction.pl --input <EMBL-File> [--output <Filename> --onlyblast
    --onlyhmmer --verbose --help]

DESCRIPTION
    assignFunction.pm analyzes BLASTP and HMMer references included in (user
    generated) EMBL-Files. Using the AssignFunction module based on these
    analyses a putative function of every orf is predicted. This module is
    still in experimental stage, so all assignments should be handled with
    care.

OPTIONS
  required parameter:
      --input <EMBL-file>    EMBL-File to be processed. If '--output' parameter is
                             omitted, a new file with additional ".ass" is generated

  optional parameters:
      --output <filename>    Output EMBL-File, that includes assignments
      --onlyblast            Only process BLASTP-reports for assignment
      --onlyhmmer            Only process HMMer-reports for assignment
      --verbose              Verbose output
      --help                 Display help

AUTHOR
     Tilmann Weber
     Microbiology/Biotechnology
     University of Tuebingen
     72076 Tuebingen
     Germany

     Email: tilmann.weber@biotech.uni-tuebingen.de

