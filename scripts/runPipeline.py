#!/usr/bin/env python
#
# Run CLUSEAN for all *.embl or *.gbk files in the current directory

import sys
import os
from optparse import OptionParser

if not os.environ.has_key('CLUSEANROOT'):
    os.environ['CLUSEANROOT'] = "."
sys.path.append(os.environ['CLUSEANROOT'])
from CLUSEAN.parseconfig import ParseConfig, ConfigParserError
from CLUSEAN.pipeline import Pipeline
from CLUSEAN.check_prereqs import check_prereqs
from CLUSEAN.prepare_analysis import prepare_analysis, prepare_ab_specific_analysis
from CLUSEAN.calculate_annotation import calculate_annotation, calculate_ab_specific_annotation
from CLUSEAN.annotate import annotate, annotate_antibiotics
from CLUSEAN.cluster_finder import cluster_finder

# Usage overview, currently there's no required arguments
usage = "%prog [options]"

# Version
version = "%prog 1.9"

def register_functions(pipeline):
    """Register functions that should run as part of the pipeline"""
    pipeline.register(check_prereqs)
    pipeline.register(prepare_analysis)
    pipeline.register(calculate_annotation)
    pipeline.register(annotate)
    pipeline.register(prepare_ab_specific_analysis)
    pipeline.register(calculate_ab_specific_annotation)
    pipeline.register(annotate_antibiotics)
    pipeline.register(cluster_finder)

def main():
    """Run CLUSEAN for all suitable input files

    Currently, suitable input files are EMBL (*.embl) and GenBank (*gbk) files.
    The pipeline runs in steps creating intermediate files for reference.
    In the end, for every input file foo.embl or bar.gbk, there will be a file
    called foo.final.embl or bar.final.embl with the full annotations.

    NOTE: Regardless of the type of the input file, the intermediate and final
    files always are EMBL files.
    """
    pipeline = Pipeline()
    register_functions(pipeline)

    parser = OptionParser(usage=usage, version=version)
    parser.add_option("-s", "--start-at", dest="start_at", type="int",
                      help="start the pipeline at a certain step",
                      default=1)
    parser.add_option("-u", "--until", dest="until", type="int",
                      help="run the pipeline up to a certain step",
                      default=pipeline.count())
    parser.add_option("-c", "--continue", dest="cont", action="store_true",
                      help="keep existing data instead of overwriting it")
    parser.add_option("-l", "--list-steps", dest="list_steps",
                      action="store_true", help="list available steps")
    parser.add_option("-C", "--configfile", dest="configfile",
                      help="specify the configuration file to use",
                      default=os.environ['CLUSEANROOT'] + os.sep + 'etc' + \
                              os.sep + "CLUSEAN.cfg")
    parser.add_option("--without-nrpspredictor", dest="skip_nrpspredictor",
                      action="store_true", default=False,
                      help="skip running NRPSPredictor (needed for antiSMASH)")
    (options, args) = parser.parse_args()


    if options.list_steps:
        print pipeline.list_steps()
        sys.exit(0)

    if options.until > pipeline.count() + 1:
        options.until = pipeline.count() + 1
    elif options.until < 0:
        options.until = 0

    pc = ParseConfig(interpolate=True)
    pc.parse_file(options.configfile)

    if options.skip_nrpspredictor:
        pc.config['Environment']['RUN_NRPSPREDICTOR'] = "false"

    # Call all the pipeline functions
    for step in xrange(options.start_at, options.until+1):
        func = pipeline.get_step(step)
        retcode = func(step, pc.config)
        if retcode != 0:
            print >> sys.stderr, "Run failed at step %s (%s)" % (step, func.func_name)
            sys.exit(step)
        step += 1

if __name__ == "__main__":
    main()

