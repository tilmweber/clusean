#!/usr/bin/perl -w

use strict;
use Test::More tests => 75;
use Bio::SeqIO;
#use Data::Dumper;

use_ok('CLUSEAN::AnnotateClusterPredict');
my $in = Bio::SeqIO->new(-file => 't/data/cluster_predict.embl',
                         -format => 'embl');
my $seq = $in->next_seq;

open(TABFILE, 't/data/cluster_predict.tab') || die "Failed to open tabfile";
my (@lines) = <TABFILE>;
close(TABFILE);

my $out_seq = annotateClusterPredict($seq, \@lines);
isa_ok($out_seq, 'Bio::Seq::RichSeq');

my @cds_motives = grep ($_->primary_tag eq 'CDS_motif',
                        $out_seq->top_SeqFeatures);

is($#cds_motives, 8, "Found correct number of CDS_motif tags");

my @expected_values = (
    [106, 1284, 'bpsA', 'AMP-binding', 'AMP-binding enzyme', 'PFAM-Id: PF00501',
     'ClusterPredict probability: 0.996575318417',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: AMP-binding. Score: 360.7. E-value: 1.3e-111. Domain range: 36..428.'],
    [3175, 4347, 'bpsA', 'AMP-binding', 'AMP-binding enzyme', 'PFAM-Id: PF00501',
     'ClusterPredict probability: 0.999786846535',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: AMP-binding. Score: 362.7. E-value: 3.2e-112. Domain range: 1059..1449.'],
    [7690, 8910, 'bpsA', 'AMP-binding', 'AMP-binding enzyme', 'PFAM-Id: PF00501',
     'ClusterPredict probability: 0.999063527238',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: AMP-binding. Score: 354.4. E-value: 1.1e-109. Domain range: 2564..2970.'],
    [1801, 2679, 'bpsA', 'Condensation', 'Condensation domain', 'PFAM-Id: PF00668',
     'ClusterPredict probability: 0.999947670396',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: Condensation. Score: 206.8. E-value: 6.2e-65. Domain range: 601..893.'],
    [4876, 5715, 'bpsA', 'Condensation', 'Condensation domain', 'PFAM-Id: PF00668',
     'ClusterPredict probability: 0.999960930501',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: Condensation. Score: 103.1. E-value: 2.4e-33. Domain range: 1626..1905.'],
    [6265, 7143, 'bpsA', 'Condensation', 'Condensation domain', 'PFAM-Id: PF00668',
     'ClusterPredict probability: 0.999932384186',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: Condensation. Score: 226.6. E-value: 6e-71. Domain range: 2089..2381.'],
    [1519, 1701, 'bpsA', 'PP-binding', 'Phosphopantetheine attachment site', 'PFAM-Id: PF00550',
     'ClusterPredict probability: 0.999717767878',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: PP-binding. Score: 42.3. E-value: 1.3e-14. Domain range: 507..567.'],
    [4579, 4764, 'bpsA', 'PP-binding', 'Phosphopantetheine attachment site', 'PFAM-Id: PF00550',
     'ClusterPredict probability: 0.999920601147',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: PP-binding. Score: 41.4. E-value: 2.5e-14. Domain range: 1527..1588.'],
    [9163, 9345, 'bpsA', 'PP-binding', 'Phosphopantetheine attachment site', 'PFAM-Id: PF00550',
     'ClusterPredict probability: 0.995589304805',
     '/data/biodata/HMMerDB/Pfam-A.hmm-Hit: PP-binding. Score: 41.2. E-value: 2.9e-14. Domain range: 3055..3115.'] );

for(my $i = 0; $i <= $#cds_motives; $i++) {
    my ($start, $end, $locus_tag, $label, $hitdesc, $pfam_id, $prob, $desc) =
            @{$expected_values[$i]};
    my $motif = $cds_motives[$i];
    my ($motif_locus_tag) = $motif->get_tag_values('locus_tag');
    my ($motif_label) = $motif->get_tag_values('label');
    my ($motif_hitdesc, $motif_desc, $motif_pfam, $motif_prob) =
            $motif->get_tag_values('note');

    is($motif->start, $start, "Motif start position match");
    is($motif->end, $end, "Motif end positon match");
    is($motif_locus_tag, $locus_tag, "Motif locus tag match");
    is($motif_label, $label, "Motif label match");
    is($motif_hitdesc, $hitdesc, "Motif hit description match");
    is($motif_desc, $desc, "Motif description match");
    is($motif_pfam, $pfam_id, "Motif PFAM ID match");
    is($motif_prob, $prob, "Motif cluster probability match");
}
