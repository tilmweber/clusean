#!/usr/bin/perl -w

use strict;
use Test::More tests => 48;
use Test::Exception;
use Bio::SearchIO;
use Bio::SeqIO;


use_ok('CLUSEAN::MapHSPtoCDS');

# We have to setup a SeqFeature object and an HSP object first
# Therfore we use the files already in t/data

my $oSeq = Bio::SeqIO->new(-file => 't/data/analysis.embl')->next_seq;
my ($oSeqFeature) = grep ($_->primary_tag eq 'CDS',
                          $oSeq->top_SeqFeatures);

# test whether all things to setup $oFeature worked...
isa_ok($oSeqFeature, 'Bio::SeqFeature::Generic');

my $oHit = Bio::SearchIO->new ('-file'	=> 't/data/PREPARE01.orf1.hsr',
											 '-format'  => 'hmmer')->next_result->next_hit;
my $oHSP = $oHit->next_hsp;

# test whether all things to setup $oHSP worked
isa_ok($oHSP, 'Bio::Search::HSP::GenericHSP');
is ($oHit->name, 'AMP-binding', 'we found an A domain in BpsA');

throws_ok {CLUSEAN::MapHSPtoCDS->new} 'Bio::Root::Exception', 'Exception thrown if called without parameters';

throws_ok {CLUSEAN::MapHSPtoCDS->new ('-SeqFeature' => $oSeqFeature, '-HSP' => $oHit)} 'Bio::Root::Exception', 'Exception thrown if HSP class is incorrect';

throws_ok {CLUSEAN::MapHSPtoCDS->new ('-SeqFeature' => $oSeq, '-HSP' => $oHSP)} 'Bio::Root::Exception', 'Exception thrown if SeqFeature class is incorrect';
my $oMapper = CLUSEAN::MapHSPtoCDS->new ('-HSP'	=> $oHSP,
										'-SeqFeature' => $oSeqFeature);
isa_ok($oMapper, 'CLUSEAN::MapHSPtoCDS');

my $oLocation = $oMapper->getLocation;

isa_ok($oLocation, 'Bio::Location::Simple');

is($oLocation->start, 106, 'Returned start coordinate correct');
is($oLocation->end,1284,'Returned end coordinate correct');
is($oLocation->strand,1,'Returned strand correct');

# OK, now let's see whether this also works on the minus strand

my $rev_oSeq = Bio::SeqIO->new(-file => 't/data/analysis_rev.embl')->next_seq;
($oSeqFeature) = grep ($_->primary_tag eq 'CDS',
                          $rev_oSeq->top_SeqFeatures);

$oMapper = CLUSEAN::MapHSPtoCDS->new ('-HSP'	=> $oHSP,
									'-SeqFeature' => $oSeqFeature);
									
isa_ok($oMapper, 'CLUSEAN::MapHSPtoCDS');

$oLocation = $oMapper->getLocation;

isa_ok($oLocation, 'Bio::Location::Simple');

is($oLocation->start, 8215, 'Returned start coordinate in -1 strand correct');
is($oLocation->end, 9393,'Returned end coordinate in -1 strand correct');
is($oLocation->strand,-1,'Returned strand -1 correct');

# and with eukaryotic sequences


$oSeq = Bio::SeqIO->new(-file => 't/data/triat.embl')->next_seq;
($oSeqFeature) = grep ($_->primary_tag eq 'CDS',
                          $oSeq->top_SeqFeatures);
isa_ok($oSeqFeature, 'Bio::SeqFeature::Generic');

my $oResult=Bio::SearchIO->new ('-file'	=> 't/data/triat.orf1.hsr',
						     	 '-format'  => 'hmmer')->next_result;
while ($oHit->name ne "Ketoacyl-synt_C") {
	$oHit = $oResult->next_hit;
}
$oHSP = $oHit->next_hsp;

isa_ok($oHSP, 'Bio::Search::HSP::GenericHSP');

$oMapper = CLUSEAN::MapHSPtoCDS->new ('-HSP'	=> $oHSP,
										'-SeqFeature' => $oSeqFeature);
isa_ok($oMapper, 'CLUSEAN::MapHSPtoCDS');
is ($oHit->name, 'Ketoacyl-synt_C', 'we found the KS domain');

$oLocation = $oMapper->getLocation;

isa_ok($oLocation, 'Bio::Location::Split');

is($oLocation->start, 908, 'Returned start coordinate correct');
is($oLocation->end,1252,'Returned end coordinate correct');
is($oLocation->strand,1,'Returned strand correct');

# and check for the sublocations
my ($subloc1, $subloc2) = $oLocation->sub_Location;
isa_ok($subloc1,'Bio::Location::Simple');
isa_ok($subloc2,'Bio::Location::Simple');

is ($subloc1->start, 908, "Exon1 start coordinates OK");
is ($subloc1->end, 1027, "Exon1 end coordinates OK");
is ($subloc1->strand, 1, "Exon1 strand OK");
is ($subloc2->start, 1091, "Exon2 start coordinates OK");
is ($subloc2->end, 1252, "Exon2 end coordinates OK");
is ($subloc2->strand, 1, "Exon2 strand OK");

# and eukas in the -1 strand

$oSeq = Bio::SeqIO->new(-file => 't/data/triat_rev.embl')->next_seq;
($oSeqFeature) = grep ($_->primary_tag eq 'CDS',
                          $oSeq->top_SeqFeatures);
                          
isa_ok($oSeqFeature, 'Bio::SeqFeature::Generic');

$oMapper = CLUSEAN::MapHSPtoCDS->new ('-HSP'	=> $oHSP,
										'-SeqFeature' => $oSeqFeature);
isa_ok($oMapper, 'CLUSEAN::MapHSPtoCDS');
is ($oHit->name, 'Ketoacyl-synt_C', 'we found the KS domain');

$oLocation = $oMapper->getLocation;

isa_ok($oLocation, 'Bio::Location::Split');

is($oLocation->start, 65462, 'Returned start coordinate correct');
is($oLocation->end,65806,'Returned end coordinate correct');
is($oLocation->strand,-1,'Returned strand correct');

# and check for the sublocations
($subloc1, $subloc2) = $oLocation->sub_Location;
isa_ok($subloc1,'Bio::Location::Simple');
isa_ok($subloc2,'Bio::Location::Simple');

is ($subloc1->start, 65687, "Exon1 start coordinates OK");
is ($subloc1->end, 65806, "Exon1 end coordinates OK");
is ($subloc1->strand, -1, "Exon1 strand OK");
is ($subloc2->start, 65462, "Exon2 start coordinates OK");
is ($subloc2->end, 65623, "Exon2 strand OK");
is ($subloc2->strand, -1, "Exon2 end coordinates OK");