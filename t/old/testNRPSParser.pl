#!/usr/bin/perl

use strict;
use CLUSEAN::NRPSPredIO;

my $oNRPSIO = CLUSEAN::NRPSPredIO->new('-file'   =>   '/home/weber/CLUSEAN/t/CP000249.nrps1.out');

while (my $oNRPS = $oNRPSIO->next_result ) {
	my $ResultName = $oNRPS->get_name;
	my $LargeClusters =join ("##", @{$oNRPS->get_large_clustArray});
	my $SmallClusters = join ("##", @{$oNRPS->get_small_clustArray});
	my $SmallClustScore = join ("##", @{$oNRPS->get_sc_scoreArray});
	my $LargeClustScore = join ("##", @{$oNRPS->get_lc_scoreArray});
	print "$ResultName has a specificity for the follwing large cluster: $LargeClusters with Score $LargeClustScore \n";
	print "$ResultName has a specificity for the follwing small cluster: $SmallClusters with Score $SmallClustScore \n";
}
