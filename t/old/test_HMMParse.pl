#!/usr/bin/perl

 use CLUSEAN::HMMParse;

    my $HMMRes=CLUSEAN::HMMParse->new ('-file' => $ARGV[1]);
    my @Results=$HMMRes->getResultSet ('-evalue' => 10,
    								   '-score' => 1,
    								   '-gene_id' => "noID");


    foreach my $hHMMHit (@Results) {
       print $hHMMHit->{"QuerySeq"},"\t",$hHMMHit->{"GeneID"},"\t",$hHMMHit->{"HMMName"},"\t";
       print $hHMMHit->{"Domain_start"},"\t\t",$hHMMHit->{"Domain_end"},"\t\t";
       print $hHMMHit->{"Score"},"\t",$hHMMHit->{"Evalue"},"\n";
    }
