#!/usr/bin/perl

 use CLUSEAN::HMMParse;

    my $HMMRes=CLUSEAN::HMMParse->new ('-file' => "/home/weber/CLUSEAN/t/test_HMM.hmm");
    my @Results=$HMMRes->getResultSet;


    foreach my $hHMMHit (@Results) {
       print $hHMMHit->{"GeneID"},"\t",$hHMMHit->{"HMMName"},"\t";
       print $hHMMHit->{"Domain_start"},"\t\t",$hHMMHit->{"Domain_end"},"\t\t";
       print $hHMMHit->{"Score"},"\t",$hHMMHit->{"Evalue"},"\n";
    }
