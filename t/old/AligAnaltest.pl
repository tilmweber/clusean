#!/usr/bin/perl

use strict;
use CLUSEAN::AligAnal;
use XML::Simple;
use Data::Dumper;
use Bio::SeqIO;


my $xml=XML::Simple->new('ForceArray'    =>  [('choice')]);
#my $xml=XML::Simple->new();
my $xmlHash=$xml->XMLin('/home/weber/CLUSEAN/etc/SignatureResources.xml');
#print Dumper($xmlHash);



#print Dumper($a);


# print Dumper($b);

my $oSeqIn=Bio::SeqIO->new('-file' => '/home/weber/CLUSEAN/t/kirAI_kr1.aa');
my $oSeq=$oSeqIn->next_seq;

my $a = CLUSEAN::AligAnal->new('-xml_Hash' => $xmlHash,
                               '-aaSeq'    => $oSeq->seq,
                               '-oSeq'     => $oSeq,
			       '-program'  => 'PKSI_KR_Stereo');

#print Dumper ($a);


# print $a->get_program;


#$a->_execute;


print "Query was: ",$a->get_query_string,"\n";
print "Coordinates: ",$a->get_HSP_startQ,"..",$a->get_HSP_endQ,"\n";
print "Hit was: ", $a->get_hit_string,"\n";
print "Coordinates: ",$a->get_HSP_startH,"..",$a->get_HSP_endH,"\n";

print "Desc: ", $xmlHash->{'PKSI-KR-Stereo'}->{description},"\n";

#my @Alignments = $xmlHash->{'PKSI-KR-Stereo'}->{Alignment};
my @Alignments = @{$a->get_alignments};

foreach my $Alig (@Alignments) {

  my @Residues = split(",", $Alig->{offset});
  




}

while (my $oComp = $a->next_comparisonData) {

print "\n----\nComment: ",$oComp->get_comment,"\n";
print "Values: ", $oComp->get_sValue,"\n";

print "Offsets: ",$oComp->get_sOffset,"\n";
print "\nExtracting positions...\n";

my $ExtrResidues = $a->ExtractResidues(@{$oComp->get_aOffset});
print "Query     : ", @{$ExtrResidues->get_aQuery},"\n";
print "Hit       : ", @{$ExtrResidues->get_aHit},"\n";
print "ValueArray: ", @{$oComp->get_aValue},"\n";
my $oCompArray = CLUSEAN::Generic->new;
my @CompArray=$oCompArray->compareArray('-Array1'  =>  $ExtrResidues->get_aQuery,
			   '-Array2'  =>  $oComp->get_aValue);
print "Comparison: ", @CompArray,"\n";
}
print "finished!\n";
