#!/usr/bin/perl -w

use strict;
use Test::More tests => 9;
use lib '.';

use_ok('CLUSEAN::HMMParse');

note("Testing hmmer3 parsing");
my $obj = CLUSEAN::HMMParse->new ('-file' => 't/data/hmmparse.hsr3');
isa_ok($obj, 'CLUSEAN::HMMParse');

my @results = $obj->getResultSet();
ok(@results, "getResultSet() returned a result");
cmp_ok( @results, '==', 9, 'getResultSet() found 9 HMM hits' );

my @expected = (
  {
    'Score' => '360.7',
    'Domain_end' => 428,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.3e-111',
    'HMMDesc' => 'AMP-binding enzyme',
    'Domain_start' => 36,
    'GeneID' => 'noID',
    'HMMName' => 'AMP-binding'
  },
  {
    'Score' => '362.7',
    'Domain_end' => 1449,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '3.2e-112',
    'HMMDesc' => 'AMP-binding enzyme',
    'Domain_start' => 1059,
    'GeneID' => 'noID',
    'HMMName' => 'AMP-binding'
  },
  {
    'Score' => '354.4',
    'Domain_end' => 2970,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.1e-109',
    'HMMDesc' => 'AMP-binding enzyme',
    'Domain_start' => 2564,
    'GeneID' => 'noID',
    'HMMName' => 'AMP-binding'
  },
  {
    'Score' => '206.8',
    'Domain_end' => 893,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '6.2e-65',
    'HMMDesc' => 'Condensation domain',
    'Domain_start' => 601,
    'GeneID' => 'noID',
    'HMMName' => 'Condensation'
  },
  {
    'Score' => '103.1',
    'Domain_end' => 1905,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '2.4e-33',
    'HMMDesc' => 'Condensation domain',
    'Domain_start' => 1626,
    'GeneID' => 'noID',
    'HMMName' => 'Condensation'
  },
  {
    'Score' => '226.6',
    'Domain_end' => 2381,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '6e-71',
    'HMMDesc' => 'Condensation domain',
    'Domain_start' => 2089,
    'GeneID' => 'noID',
    'HMMName' => 'Condensation'
  },
  {
    'Score' => '42.3',
    'Domain_end' => 567,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.3e-14',
    'HMMDesc' => 'Phosphopantetheine attachment site',
    'Domain_start' => 507,
    'GeneID' => 'noID',
    'HMMName' => 'PP-binding'
  },
  {
    'Score' => '41.4',
    'Domain_end' => 1588,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '2.5e-14',
    'HMMDesc' => 'Phosphopantetheine attachment site',
    'Domain_start' => 1527,
    'GeneID' => 'noID',
    'HMMName' => 'PP-binding'
  },
  {
    'Score' => '41.2',
    'Domain_end' => 3115,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '2.9e-14',
    'HMMDesc' => 'Phosphopantetheine attachment site',
    'Domain_start' => 3055,
    'GeneID' => 'noID',
    'HMMName' => 'PP-binding'
  }
);

is_deeply(\@results, \@expected, "getResultSet() returned the correct results");

note("Testing hmmer2 parsing");
$obj = CLUSEAN::HMMParse->new ('-file' => 't/data/hmmparse.hsr2');
isa_ok($obj, 'CLUSEAN::HMMParse');

@results = $obj->getResultSet();
ok(@results, "getResultSet() returned a result");
cmp_ok( @results, '==', 10, 'getResultSet() found 9 HMM hits' );

@expected = (
  {
    'Score' => '507.7',
    'Domain_end' => 428,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.5e-149',
    'HMMDesc' => 'AMP-binding enzyme',
    'Domain_start' => 36,
    'GeneID' => 'noID',
    'HMMName' => 'AMP-binding'
  },
  {
    'Score' => '63.3',
    'Domain_end' => 569,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.4e-16',
    'HMMDesc' => 'Phosphopantetheine attachment site',
    'Domain_start' => 506,
    'GeneID' => 'noID',
    'HMMName' => 'PP-binding'
  },
  {
    'Score' => '212.4',
    'Domain_end' => 893,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.2e-60',
    'HMMDesc' => 'Condensation domain',
    'Domain_start' => 600,
    'GeneID' => 'noID',
    'HMMName' => 'Condensation'
  },
  {
    'Score' => '520.9',
    'Domain_end' => 1449,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.6e-153',
    'HMMDesc' => 'AMP-binding enzyme',
    'Domain_start' => 1059,
    'GeneID' => 'noID',
    'HMMName' => 'AMP-binding'
  },
  {
    'Score' => '60.4',
    'Domain_end' => 1589,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '8.9e-16',
    'HMMDesc' => 'Phosphopantetheine attachment site',
    'Domain_start' => 1526,
    'GeneID' => 'noID',
    'HMMName' => 'PP-binding'
  },
  {
    'Score' => '41.4',
    'Domain_end' => 1804,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.9e-11',
    'HMMDesc' => 'Condensation domain',
    'Domain_start' => 1631,
    'GeneID' => 'noID',
    'HMMName' => 'Condensation'
  },
  {
    'Score' => '35.1',
    'Domain_end' => 1906,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '1.2e-09',
    'HMMDesc' => 'Condensation domain',
    'Domain_start' => 1850,
    'GeneID' => 'noID',
    'HMMName' => 'Condensation'
  },
  {
    'Score' => '282.6',
    'Domain_end' => 2382,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '8.8e-82',
    'HMMDesc' => 'Condensation domain',
    'Domain_start' => 2087,
    'GeneID' => 'noID',
    'HMMName' => 'Condensation'
  },
  {
    'Score' => '507.3',
    'Domain_end' => 2970,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '2e-149',
    'HMMDesc' => 'AMP-binding enzyme',
    'Domain_start' => 2564,
    'GeneID' => 'noID',
    'HMMName' => 'AMP-binding'
  },
  {
    'Score' => '69.8',
    'Domain_end' => 3117,
    'QuerySeq' => 'PREPARE01.orf1',
    'Evalue' => '2.2e-18',
    'HMMDesc' => 'Phosphopantetheine attachment site',
    'Domain_start' => 3054,
    'GeneID' => 'noID',
    'HMMName' => 'PP-binding'
  }
);

is_deeply(\@results, \@expected, "getResultSet() returned the correct results");
