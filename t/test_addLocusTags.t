#!/usr/bin/perl -w
use strict;

use Test::More tests => 9;
use File::Temp qw(tempdir);
use Bio::SeqIO;

my $tmpdir = tempdir("test_addLocusTag_XXXX",
                     DIR => "t",
                     CLEANUP => 1);

my $cmd = "bin/addLocusTags.pl --input t/data/locus_tags.embl --output $tmpdir/output.embl";

system($cmd);

ok( -e "$tmpdir/output.embl", "Check if output file was created");

my $seq = Bio::SeqIO->new( '-file' => "$tmpdir/output.embl", '-format' => 'EMBL')->next_seq;

ok(defined($seq), "Check if output tag is a valid EMBL file");
is(ref($seq), "Bio::Seq::RichSeq", "Check if correct object is parsed from EMBL file");

my ($first, $second) = grep ( $_->primary_tag eq 'CDS', $seq->top_SeqFeatures );

ok(defined($first), "Check if first feature exists");
ok($first->has_tag("locus_tag"), "Check if pre-existing locus tag can be found");
my ($tag) = $first->get_tag_values("locus_tag");
is($tag, "existing_tag", "Check if pre-existing locus tag has correct value");
ok(defined($second), "Check if second feature exists");
ok($second->has_tag("locus_tag"), "Check if added locus tag can be found");
($tag) = $second->get_tag_values("locus_tag");
is($tag, "AUTOORF_00001", "Check if added locus tag has correct value");

