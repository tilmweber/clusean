#!/usr/bin/perl -w
use strict;

use Test::More tests => 34;
use File::Temp qw(tempdir);
use Cwd;
use Data::Dumper;
use lib "t/lib";
use TestUtils;
use lib ".";
use_ok('CLUSEAN::AssignFunction');

my $tmpdir = tempdir("test_AssignFunction_XXXX",
                     DIR => "t",
                     CLEANUP => 1);

TestUtils::fixup_config($tmpdir, {});

my $obj = CLUSEAN::AssignFunction->new("-reportfile" => "t/data/assign_function.blastp",
                                       "-configfile" => "$tmpdir/CLUSEAN.cfg");
isa_ok($obj, 'CLUSEAN::AssignFunction');
is($$obj{'Rep_max_score'}, '1881', "Checking Rep_max_score");
is($$obj{'Rep_min_score'}, '1662', "Checking Rep_min_score");
is($$obj{'AssignmentArray'}, undef, "AssignmentArray is undefined before call to doAssignment");

my @array = $obj->doAssignment;
ok($$obj{'AssignmentArray'}, "AssignmentArray is defined after call to doAssignment");
ok(@array, "Checking if returned array is defined");

my $hits = $obj->getHitArray;
ok($hits, "HitArray is defined");
is($#{$hits}, 19, "HitArray has 20 entries");
ok($$hits[0]->[1] <= $$hits[1]->[1], "Array sort order is correct");

my @expected =  ( ['NRPS', '0.986177565124934'], ['unclassified', '0.1'] );
is_deeply(\@array, \@expected, "doAssignment returned sane result");

my $aa = $obj->getAssignmentArray;
is_deeply($aa, \@array, "getAssignmentArray returns the same AssignmentArray");

is($obj->getAssignmentName, "NRPS", "getAssignmentName returned the correct result");
is($obj->getAssignmentScore, "0.986177565124934", "getAssignmentScore returned the correct result");

$obj = CLUSEAN::AssignFunction->new("-reportfile" => "t/data/assign_function.hsr3",
                                    "-configfile" => "$tmpdir/CLUSEAN.cfg",
                                    "-method" => "HMMTab");
isa_ok($obj, 'CLUSEAN::AssignFunction');
is($$obj{'AssignmentArray'}, undef, "AssignmentArray is undefined before call to doAssignment");

@array = $obj->doAssignment;
ok($$obj{'AssignmentArray'}, "AssignmentArray is defined after call to doAssignment");
ok(@array, "Returned array is defined");
is($#array, 0, "Returned array has 1 entry");

$hits = $obj->getHitArray;
ok($hits, "HitArray is defined");
is($#{$hits}, 8, "HitArray has 9 entries");
ok($$hits[0]->[1] <= $$hits[1]->[1], "Array sort order is correct");

is($obj->getAssignmentName, "NRPS", "getAssignmentName returned the correct result");
is($obj->getAssignmentScore, "34.8276812792911", "getAssignmentScore returned the correct result");

$obj = CLUSEAN::AssignFunction->new("-reportfile" => "t/data/assign_function.hsr2",
                                    "-configfile" => "$tmpdir/CLUSEAN.cfg",
                                    "-method" => "HMMTab");
isa_ok($obj, 'CLUSEAN::AssignFunction');
is($$obj{'AssignmentArray'}, undef, "AssignmentArray is undefined before call to doAssignment");

@array = $obj->doAssignment;
ok($$obj{'AssignmentArray'}, "AssignmentArray is defined after call to doAssignment");
ok(@array, "Returned array is defined");
is($#array, 0, "Returned array has 1 entry");

$hits = $obj->getHitArray;
ok($hits, "HitArray is defined");
is($#{$hits}, 9, "HitArray has 10 entries");
ok($$hits[0]->[1] <= $$hits[1]->[1], "Array sort order is correct");

is($obj->getAssignmentName, "NRPS", "getAssignmentName returned the correct result");
is($obj->getAssignmentScore, "33.5304281052025", "getAssignmentScore returned the correct result");
