#!/usr/bin/perl -w
use strict;

use Test::More tests => 11;
use File::Temp qw(tempdir);
use Cwd;
use lib "t/lib";
use TestUtils;

my $tmpdir = tempdir("test_testPrereq_XXXX",
                     DIR => "t",
                     CLEANUP => 1);

my $oldcwd = cwd();

my %env = ( CPUS => '1',
            BLASTDB => "t/data",
            HMMERDB => "t/data",
            DB => "clusean",
            BLAST_EXECUTABLE => "blastp",
            DIALOG => "dialog",
            PATH => "\${PATH}:$oldcwd/NRPSPredictor" );

TestUtils::fixup_config($tmpdir, \%env);

ok(-e "$tmpdir/CLUSEAN.cfg", "Checking if config file exists");

chdir($tmpdir);

my $cmd = "../../bin/testPrereq.pl CLUSEAN.cfg >/dev/null";
system($cmd);
ok($? == 0, "Check if testPrereq.pl runs successfully");
ok(-e "bioinfosoft.sh", "Check if bash config file was created");

# Now check if the generated bash config file is sane

ok(open(BASH, "bioinfosoft.sh"), "Check if bioinfosoft.sh can be opened");

while(<BASH>) {
    chomp;
    next if /^#/;
    next if /^$/;
    next unless m/export (.*)=(.*)/;
    is($2, $env{$1}, "Checking for correct $1 setting");
}

# leave temp dir so it will be cleaned up
chdir($oldcwd);
