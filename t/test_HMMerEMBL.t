#!/usr/bin/perl -w
use strict;

use Test::More tests => 7;
use File::Temp qw(tempdir);
use Bio::SeqIO;

my $tmpdir = tempdir("test_HMMerEMBL_XXXX",
                     DIR => "t",
                     CLEANUP => 1);

my $cmd = "bin/HMMerEMBL.pl --input t/data/prepare.embl --output $tmpdir/output_hmmer.embl";

system($cmd);

ok( -e "$tmpdir/output_hmmer.embl", "Check if output file was created");

my $seq = Bio::SeqIO->new( '-file' => "$tmpdir/output_hmmer.embl", '-format' => 'EMBL')->next_seq;

ok(defined($seq), "Check if output tag is a valid EMBL file");
is(ref($seq), "Bio::Seq::RichSeq", "Check if correct object is parsed from EMBL file");

my ($first) = grep ( $_->primary_tag eq 'CDS', $seq->top_SeqFeatures );

ok(defined($first), "Check if first feature exists");
ok($first->has_tag("HMMer_file"), "Check if HMMer_file tag can be found");
my ($tag) = $first->get_tag_values("HMMer_file");
is($tag, "HMMer/PREPARE01.orf1.hsr", "Check if HMMer_file tag has correct value");

my $fasta = $tag;
$fasta =~ s/\.hsr$/.fasta/;
ok(-e "$tmpdir/$fasta", "Check if HMMer_file exists");

