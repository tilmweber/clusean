#!/usr/bin/perl -w

use strict;
use Test::More tests => 5;
use Bio::SearchIO;

use_ok('CLUSEAN::ExtractSignature');

my $obj = Bio::SearchIO->new(-file => "t/data/extract.hsr",
                             -format => "hmmer2");

my @module_seqs = getModuleSequences($obj);

is($#module_seqs, 2, "Got a correct number of modules");

my @exp = (
    ['KGVTISQGCVAELTMDA-----GWAMEP-----GEAVLMHSP-HAFDASLFELWMPLASGVRVVLAEP----GSVDARRLREAAAA-GVTRVYLTAGSLRAVAEEA--------PESFAEFREVLTGGDVVPAHAVERVRTAAPR-A--RFRNMYGPTEATMCATW',
     'DPRVIRERLGAVLPEHLVPAA--VLALDALPLTGNGKVDRSALPAPEFAA', 'PREPARE01.orf1_m1'],
    ['KGVVVTHAGLGNLALAHID---RFGVSP-----SSRVLQFAA-LGFDTIVSEVMMALLSGATLVVPPE---RDLPPRASFTDALERWDITHVKAPPSVLGTAD------------VLPSTVETVVAAGELCPPGLVDRLSADR------RMINAYGPTETTICATM',
     'DTQAVRAHLASRMPQYMVPAA--VVALDALPLTAHGKIDRRALPDPDFTA', 'PREPARE01.orf1_m2'],
    ['KAVVVTHRNLTNYLLHCGR---MYPG---L---RGRSVLHSS-IAFDLTVTATFTPLIVGGEIHVGALE------D---LIGVVEAAPSIFLKATPSHLLTLDTAS---------RGSAGSGDLLLGGEQLPADTVVQWRRKYPN-I--VVVNEYGPTEATVGCVE',
     'DEAAVREGVAARLPQYMVPAA--LVVLGALPLTANGKVDRAALPDPDFGA', 'PREPARE01.orf1_m3'] );

is_deeply(\@module_seqs, \@exp, "Got expected sequence strings");

my @stachelhaus_sigs = extractStachelhaus(@module_seqs);

my @stachelhaus_exp = (['DAFYLGMMCK','PREPARE01.orf1_m1'], ['DTSKVAAICK','PREPARE01.orf1_m2'], ['DLTKLGEVGK','PREPARE01.orf1_m3']);

is_deeply(\@stachelhaus_sigs, \@stachelhaus_exp, "Got expected Stachelhaus signatures");

my @eightA_sigs = extract8Angstrom(@module_seqs);

my @eightA_exp = (
    ['LDASFDASLFEMYLLTGGDRNMYGPTEATMCATW','PREPARE01.orf1_m1'],
    ['LAHAFDTIVSEMKAVAAGEINAYGPTETTICATM','PREPARE01.orf1_m2'],
    ['YHCSFDLTVTATKALLGGEVNEYGPTEATVGCVE','PREPARE01.orf1_m3']);

is_deeply(\@eightA_sigs, \@eightA_exp, "Got expected 8 Aangstrom signatures");
