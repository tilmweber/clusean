#!/usr/bin/env python
#
# Run HMMer
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
from utils import dispatch, dispatch_to_file, filter_files
from progressbar import *

def hmmscan(directory, database, parallel_jobs=None):
    """Run hmmscan on a database"""

    hmmer_files = filter_files(os.listdir(directory), ".fasta")

    dbname = database[database.rfind(os.sep)+1:]

    widgets = ["Waiting for HMMer against %s to finish: " % dbname,
               RotatingMarker(), " ", Percentage(), " ", Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(hmmer_files)).start()

    i = 0

    for f in hmmer_files:
        f = directory + os.sep + f
        args = ['hmmscan', '-o', f.replace(".fasta", ".hsr")]
        if parallel_jobs is not None:
            args += ['--cpu', '%s' % parallel_jobs]
        args += [database, f]
        dispatch(args)
        pbar.update(i+1)
        i += 1

    pbar.finish()

def hmmpfam(directory, database, parallel_jobs=None):
    """Run hmmpfam on a database"""
    hmmer_files = filter_files(os.listdir(directory), ".fasta")

    dbname = database[database.rfind(os.sep)+1:]

    widgets = ["Waiting for HMMer against %s to finish: " % dbname,
               RotatingMarker(), " ", Percentage(), " ", Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(hmmer_files)).start()

    i = 0

    for f in hmmer_files:
        f = directory + os.sep + f
        args = ['hmmpfam2']
        if parallel_jobs is not None:
            args += ['--cpu', '%s' % parallel_jobs]
        args += [database, f]
        dispatch_to_file(args, f.replace(".fasta", ".hsr"))
        pbar.update(i+1)
        i += 1

    pbar.finish()
