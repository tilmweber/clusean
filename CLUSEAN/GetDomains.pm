
=head1 NAME

CLUSEAN::GetDomains - Returns all Features overlapping/contained in a SeqFeature object

=head1 SYNOPSIS


$oDomains = CLUSEAN::GetDomains->new ('-featureObj'    =>    \$oFeature,
                                      '-seqObj'        =>   $oSeq);

@aDomains = $oDomains->getDomainArray;

=head1 DESCRIPTION

This module identifies all Features that are overlapping/contained in an SeqFeature object.
Mainly, it is used to find CDS_motif tags cd ..within a CDS-Feature of megasynthases like PKS
or NRPS enzymes.

The getDomainArray method returns an Array of Arrays containing

("domain type", "domain start", "domain end", "strand", "note-tags (concatenated)")


=head1 EXAMPLES

use CLUSEAN::GetDomains;
use Bio::SeqIO;


my $oSeqIO = Bio::SeqIO->new ("-file"    =>   $sInputSeqFn,
                              "-format"  =>   "EMBL");

while ( $oSeq = $oSeqIO->next_seq) {
    $sSeqID = $oSeq->id;
    $sSeqID =~ s/;$//;
    my $i=0;
     # now cycle though every identified orf..
    foreach $oFeature (grep ( $_->primary_tag eq 'CDS',
                              $oSeq->top_SeqFeatures)) {
        $i++;
        my $oDomains = CLUSEAN::GetDomains->new ('-featureObj'    =>    \$oFeature,
                                                 '-seqObj'        =>   $oSeq);

        @aDomains = $oDomains->getDomainArray;

        foreach my $aDomainLine (@aDomains) {
                print join ($sSeparator, @{$aDomainLine}),"\n";
                # print $aDomainLine->[1],"\t",$aDomainLine->[0],"\n";
        }
    }
}

=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 University of Tuebingen
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

package CLUSEAN::GetDomains;

use strict;

use base ("Bio::Root::Root");
use Sort::ArrayOfArrays;

=head1 Methods

=head2 new

 Usage      :  my $HMMRes=CLUSEAN::GetDomains ("-featureObj"    =>    \$oFeature,
                                               "-seqObj"        =>    $oSeq,
                                               "-FeatTag"       =>   "CDS_motif",
                                               "-DomainTag"     =>   "label");


 Parameters :  -FeatTag                   Tag used for subfeatures
               -DomainTag                 Tag used within subfeatures to label subfeature

 Function   :  Sets up GetDomains object

 Returns    :  A CLUSEAN::GetDomains object


=cut

sub new {

    # Set up Method
    my $invocant = shift;
    my $class    = ref($invocant) || $invocant;
    my %aParams  = (
        "-featureObj" => undef,
        "-seqObj"     => undef,
        "-FeatTag"    => "CDS_motif",
        "-DomainTag"  => "label",
        @_
    );
    my $self = {};

    my $oFeature = ${ $aParams{"-featureObj"} };
    my $oSeq     = $aParams{"-seqObj"};

    if ( $oFeature !~ /^Bio::SeqFeature.*/ ) {
        $self->throw(
            "-featureObj needs to be a reference to a Bio::SeqFeature object");
    }

    if ( $oSeq !~ /^Bio::Seq.*/ ) {
        $self->throw("-seqObj needs to be a Bio::Seq object");
    }

    my @aAllFeatures;

# Added check for locus_tag, as exception is thrown otherwise if no locus_tag is present
# Should be optimized!!!

    my $bHasLocusTag = 'TRUE';
    @aAllFeatures = $oSeq->get_SeqFeatures( $aParams{'-FeatTag'} );
    foreach my $otempFeat (@aAllFeatures) {
        if ( !$otempFeat->has_tag('locus_tag') ) {
            $bHasLocusTag = undef;
        }
    }
    if ($bHasLocusTag) {
        @aAllFeatures = grep {
            my ($ref_locus_tag)  = $oFeature->get_tag_values('locus_tag');
            my ($curr_locus_tag) = $_->get_tag_values('locus_tag');
            $ref_locus_tag eq $curr_locus_tag;
        } @aAllFeatures;
    }
    else {
        @aAllFeatures =
          grep ( $_->primary_tag eq $aParams{'-FeatTag'},
            $oSeq->all_SeqFeatures );
    }

    my @oSubfeatures;
    my @aFeatureList;

    foreach my $oCmpFeat (@aAllFeatures) {

        # Check wheter feature overlaps and that idenified $CmpFeature is
        # completely included in $oFeature (Required for overlapping
        # start/stop codons)
        if (   ( $oCmpFeat->overlaps($oFeature) )
            && ( $oCmpFeat->end <= $oFeature->end ) )
        {
            if ( $aParams{'-verbose'} ) {
                print "Found Subfeature at ", $oCmpFeat->start, "..",
                  $oCmpFeat->end, "\n";
            }

            # Push found Feature to @oSubfeatures;
            push @oSubfeatures, $oCmpFeat;
            if ( $oCmpFeat->has_tag( $aParams{"-DomainTag"} ) ) {
                my ($sLabel) =
                  $oCmpFeat->get_tag_values( $aParams{"-DomainTag"} );
                my $iOvlStart  = $oCmpFeat->start;
                my $iOvlEnd    = $oCmpFeat->end;
                my $iOvlStrand = $oCmpFeat->strand;
                my $sNotes;
                if ( $oCmpFeat->has_tag("note") ) {
                    $sNotes = join( "//", $oCmpFeat->get_tag_values("note") );

                    push(
                        @aFeatureList,
                        [
                            (
                                $sLabel,     $iOvlStart, $iOvlEnd,
                                $iOvlStrand, $sNotes
                            )
                        ]
                    );

                    if ( $aParams{'-verbose'} ) {
                        print "Content: ",
                          join( "//",
                            ( $sLabel, $iOvlStart, $iOvlEnd, $iOvlStrand ) ),
                          "\n";
                    }
                }
            }
        }
    }

    # print Dumper(@aFeatureList);
    # Now sort...

    # If Feature is in the + strand, we have to sort ascending,
    # if feature is in the - strand, we have to sort descending to get the
    # order of  domains

    my @aSortedDomains;
    if ( $#aFeatureList > 0 ) {
        my $sSortCode;
        if ( $aFeatureList[0][3] eq "-1" ) {
            $sSortCode = "nd";
        }
        else {
            $sSortCode = "na";
        }
        my $oSorter = Sort::ArrayOfArrays->new(
            {
                "results"     => \@aFeatureList,
                "sort_column" => 1,
                "sort_code"   => { 1 => $sSortCode }
            }
        );

        @aSortedDomains = @{ $oSorter->sort_it };

    }

    $self->{DomainArray}  = \@aSortedDomains;
    $self->{oSubfeatures} = \@oSubfeatures;

    bless( $self, $class );
    return $self;
}

sub _findFeatures {

}

=head 2 getDomainArray

 Usage:        @aDomains = $oDomains->getDomainArray();

 Parameters:   none

 Function:     Returns AoA containing the subfeature type, its location and description

 Returns:      AoA of subfeature descriptions
 			   ("domain type", "domain start", "domain end", "strand", "note-tags (concatenated)")

=cut

sub getDomainArray {
    my $self = shift @_;
    return @{ $self->{DomainArray} };
}

=head 2 getSubfeatures;

 Usage:        @aDomains = $oDomains->getSubfeatures();

 Parameters:   none

 Function:     Returns Array of references to Bio::SeqFeature objects overlapping the Feture specified
 			   by the constructor method

 Returns:      Array of references to Bio::SeqFeature objects

=cut

sub getSubfeatures {
    my $self = shift @_;
    return @{ $self->{oSubfeatures} };
}

1;
