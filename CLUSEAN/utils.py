#!/usr/bin/env python
#
# Pipeline helpers for CLUSEAN
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import subprocess

def dispatch(args):
    """Call the external command"""
    return subprocess.call(args)

def dispatch_to_file(args, out_filename):
    """Call the external command writing stdout to a filename"""
    ret = 127
    try:
        fh = open(out_filename, 'w')
        ret = subprocess.call(args, stdout=fh)
    except IOError:
        fh = None
    finally:
        if fh is not None:
            fh.close()
    return ret

def filter_files(file_list, extension):
    """Filter a file list for files with a certain extension"""
    return [entry for entry in file_list if entry.endswith(extension)]

def tmp_file_pattern(embl_id, step):
    return "%s.substep%s.step%02d.embl" % (embl_id, "%02d", step)
