#!/usr/bin/env python
#
# Run blast processes
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
import multiprocessing
from utils import dispatch, filter_files
from progressbar import *

def update_progress_bar(bar):
    """Update the progress bar"""
    update_progress_bar.count += 1
    update_progress_bar.bar.update(update_progress_bar.count)

def parallel_blast(directory, database, parallel_jobs=None):
    """Run blast scripts in parallel on all CPUs"""
    pool = multiprocessing.Pool(processes=parallel_jobs)

    blast_files = filter_files(os.listdir(directory), ".fasta")

    dbname = database[database.rfind(os.sep)+1:]

    widgets = ["Waiting for BLAST against %s to finish: " % dbname,
               RotatingMarker(), " ", Percentage(), " ", Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(blast_files)).start()
    update_progress_bar.count = 0
    update_progress_bar.bar = pbar

    for f in blast_files:
        f = directory + os.sep + f
        args = ['blastp', '-db', database, '-query', f, '-out',
                f.replace(".fasta", ".blastp"), '-num_threads', '1',
                '-num_descriptions', '250', '-num_alignments', '250']
        pool.apply_async(dispatch, (args, ),
            callback=update_progress_bar)

    pool.close()
    pool.join()
    pbar.finish()

    result_files = filter_files(os.listdir(directory), ".blastp")
    for f in result_files:
        f = directory + os.path.sep + f
        dispatch(['gzip', f])


