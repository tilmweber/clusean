#!/usr/bin/env python
#
# Make sure CLUSEAN prerequisites are met
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

from utils import dispatch

def check_prereqs(step, config):
    """Check if all prerequisites for CLUSEAN are met"""
    args = ["testPrereq.pl"]
    return dispatch(args)
