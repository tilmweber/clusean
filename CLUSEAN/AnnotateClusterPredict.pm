=head1 NAME

CLUSEAN::AnnotateClusterPredict - Annotate probabilities that a given CDS_motif
                                  is part of a secondary metabolite producing
                                  cluster.

=head1 SYNOPSIS

$in = Bio::SeqIO->new(-file => "file.embl,
                      -format => 'embl');
$in_seq = $in->next_seq;

open(TABFILE, "predictions.tab");
@lines = <TABFILE>;
close(TABFILE);

use CLUSEAN::AnnotateClusterPredict;
$out_seq = annotateClusterPredict(in_seq, \@lines);

=cut

package CLUSEAN::AnnotateClusterPredict;
require Exporter;
our @ISA      = qw(Exporter);
our @EXPORT   = qw(annotateClusterPredict);
our $VERSION  = 1.001;

use strict;


sub annotateClusterPredict {
    my $seq = shift;
    my @lines = @{shift()};
    my @cds_motives = grep ($_->primary_tag eq 'CDS_motif', $seq->top_SeqFeatures);
    my %motif_by_start_pos;

    foreach my $motif (@cds_motives) {
        next unless $motif->has_tag('note');
        next unless (grep /^PFAM-Id/, $motif->get_tag_values('note'));
        if (!defined($motif_by_start_pos{$motif->start})) {
            $motif_by_start_pos{$motif->start} = {};
        }
        $motif_by_start_pos{$motif->start}{$motif->end} = $motif;
    }

    foreach my $line (@lines) {
        chomp $line;
        my ($pfam, $name, $start, $end, $prob) = split /\t/, $line;
        my $motif = $motif_by_start_pos{$start}{$end};
        die "Motif has no locus_tag" unless $motif->has_tag('locus_tag');
        my ($tag) = $motif->get_tag_values('locus_tag');
        if ($tag ne $name) {
            die "Motif starting at $start has wrong locus tag $tag, expected $name";
        }
        $motif->add_tag_value('note', "ClusterPredict probability: $prob");
    }

    return $seq;
}

1;
