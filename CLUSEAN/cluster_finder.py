#!/usr/bin/env python
#
# Run the cluster_predict software from the Fishbach group
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
import sys
import re
from utils import dispatch, filter_files, tmp_file_pattern


def cluster_finder(step, config):
    """Run the cluster_predict software from the Fishbach group"""

    for embl_file in filter_files(os.listdir('.'), ".step07.embl"):
        if re.search(r'substep..\.step07\.embl', embl_file) is not None:
            continue

        embl_id = embl_file.replace(".step07.embl", "")
        tmp_file = tmp_file_pattern(embl_id, step)
        pfam_id_file = "%s_pfam_ids.py" % embl_id
        prediction_file = "%s.cluster_predictions.tab" % embl_id

        substep = 1
        if get_pfam_ids(embl_file, pfam_id_file) > 0:
            return step

        if run_cluster_finder(embl_id, prediction_file) > 0:
            return step

        if annotate_cluster_results(embl_file, prediction_file,
                                    tmp_file % substep) > 0:
            return step

        os.rename(tmp_file % substep, "%s.step%02d.embl" % (embl_id, step))
        for i in range(1, substep):
            os.remove(tmp_file % (i))

    return 0

def get_pfam_ids(infile, outfile):
    """Generate the sorted PFAM ids for the genome"""
    args = ['clusterPfam.pl', '--input', infile, '--output', outfile]
    return dispatch(args)

def run_cluster_finder(embl_id, outfile):
    """Load the data and run the cluster_predict tool"""
    sys.path.append(os.curdir)
    module = __import__("%s_pfam_ids" % embl_id)
    sys.path.pop()

    pfam_ids = module.pfam_ids

    observations = [o[0] for o in pfam_ids]

    from CLUSEAN.cluster_predict import do_HMM as predict_clusters
    from CLUSEAN.cluster_predict import A as trans_prob
    from CLUSEAN.cluster_predict import B as emit_prob
    from CLUSEAN.cluster_predict import start_p, index_dict
    predictions = predict_clusters(observations, trans_prob, emit_prob,
                                   index_dict, start_p, graph=True,
                                   name=embl_id)

    if len(pfam_ids) != len(predictions):
        print "input length %s != output length %s" % (len(pfam_ids), len(predictions))

    fh = None
    try:
        fh = open(outfile, 'w')
        for i in range(len(pfam_ids)):
            (pfam, name, start, end) = pfam_ids[i]
            prediction = predictions[i]
            fh.write("%s\t%s\t%s\t%s\t%s\n" %
                        (pfam, name, start, end, prediction))
    except IOerror, e:
        print e.text
        return 1
    finally:
        if fh is not None:
            fh.close()

    return 0

def annotate_cluster_results(infile, tabfile, outfile):
    """Annotate cluster_predict results into the embl file"""
    args = ['annotateClusterPredict.pl', '--input', infile, '--table', tabfile,
            '--output', outfile]
    return dispatch(args)
