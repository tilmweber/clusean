#
#  CLUSEAN Module to parse HMMer output files
#
#  Author: Tilmann Weber
#
# New implementation of HMMParse class.
# Does not use inheritance from the Bio::SearchIO classes anymore but instead makes
# use of the facade pattern to keep the API the same.
# As a bonus, with BioPerl 1.6.2, this class supports parsing HMMer2 and HMMer3.
#
=head1 NAME

CLUSEAN::HMMParse - Analysed HMMPfam and HMMSearch outputfiles

=head1 SYNOPSIS


$HMMRes=CLUSEAN::HMMParse->new ('-file' => "/home/weber/abdb/hmmmod/t/test2.hmm");

@Results=$HMMRes->getResultSet;

=head1 DESCRIPTION

This modules opens and analyzed HMMPfam or HMMSearch outputfiles and
parses the included information and returns an Array of Hashes including the
following information:



     $aHMMResultSet = { 'HMMName'      => Name of Pfam-Domain Hit,
                        'Domain_start' => aa pos of domain start,
                        'Domain_end'   => aa pos of domain end,
                        'Evalue'       => E-value of domain hit,
                        'Score'        => Score of domain hit,
                        'GeneID'       => GeneID ($sGeneID),
		            	'QuerySeq'     => ID of query sequence,
		                'HMMDesc'      => PlainText description of PfamHit}

     $HMMRes->getResultSet returns an Array of $aHMMResultSet arrays

=head1 INCLUSIONS

HMMParse includes following 3rd Party modules:

Bio::SearchIO::hmmer
Bio::Search::HSP::HSPFactory
Bio::Search::Hit::HitFactory


=head1 EXAMPLES

    use CLUSEAN::HMMParse;

    my $HMMRes=CLUSEAN::HMMParse->new ('-file' => "HMMPfamOut.hmm",
			                           '-type' => "hmmpfam");
    my @Results=$HMMRes->getResultSet;


    foreach my $hHMMHit (@Results) {
       print $hHMMHit->{"GeneID"},"\t",$hHMMHit->{"HMMName"},"\t";
       print $hHMMHit->{"Domain_start"},"\t\t",$hHMMHit->{"Domain_end"},"\t\t";
       print $hHMMHit->{"Score"},"\t",$hHMMHit->{"Evalue"},"\n";
}



=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 University of Tuebingen
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

package CLUSEAN::HMMParse;

=head1 Methods

=cut

use strict;
use Bio::SearchIO::hmmer;



=head2 new

 Title      :  new

 Usage      :  my $HMMRes=CLUSEAN::HMMParse->new ('-file'    =>  'HMMSearchOut.hmm')


 Function   :  Reads and parses a HMMPfam or outputfile

 Returns    :  An object that forwards some calls to Bio::SearchIO::hmmer


=cut


sub new {
    my $invocant  = shift;
    my $class     = ref($invocant)  ||  $invocant;

    my $self = {};
    $self->{'_parser'} = Bio::SearchIO::hmmer->new(@_);
    bless ($self, $class);
    return $self;
}


=head2 getResultSet

 Title      :  getResultSet

 Usage      :  @aResults=$oHMMParseObject->getResultSet ('-evalue'  => 2,
		                                         '-gene_id' => test.01
                                                         '-score'   => 1);

 Function   :  Parses the HMMer results and Returns HMM-Hits as @aResults if
               the hit's Evalue is < '-evalue' and the score is > '-score'
               Defaults:  E-value < 2, Score > 1

 Returns    :  Array of $aHMMResultSet
               where $aHMMResultSet is defined as
               $aHMMResultSet = { 'HMMName'      => Name of Pfam-Domain Hit
				  'Domain_start' => aa pos of domain start
				  'Domain_end'   => aa pos of domain end
                  'Evalue'       => E-value of domain hit
                  'Score'        => Score of domain hit
                  'GeneID'       => Gene identifier
				  'QuerySeq'     => ID of query sequence
                  'HMMDesc'      => PlainText description of PfamHit}

=cut

sub getResultSet {

    # Set up Method
    my $self  = shift;
    # Get parameters
    my $aParams   =  {
	"-evalue"   =>  1,
	"-score"    =>  10,
	"-gene_id"   =>  "noID",
	@_ };
    
    my $oSeqSet;
 
    my $aHMMResultSet;
    my @aResults;
    my %hDescTable;

    while (my $oResult=$self->next_result) {
		while (my $oHMMerHit=$oResult->next_hit ) {
			while (my $oHMMerHSP=$oHMMerHit->next_hsp) {
	   		next if ($oHMMerHSP->evalue > $aParams->{'-evalue'});
	        next if ($oHMMerHSP->score < $aParams->{'-score'});
         
           $aHMMResultSet = { 'HMMName'         =>  $oHMMerHit->name,
			      			  'Domain_start'    =>  $oHMMerHSP->start,
			      			  'Domain_end'      =>  $oHMMerHSP->end,
			                  'Evalue'          =>  $oHMMerHSP->evalue,
			                  'Score'           =>  $oHMMerHSP->score,
			                  'QuerySeq'        =>  $oResult->query_name,
			                  'GeneID'          =>  $aParams->{'-gene_id'},
			                  'HMMDesc'         =>  $oHMMerHit->description
           					};
          
	    push @aResults, $aHMMResultSet;
		}
       }
    }
    return @aResults;
}

=head2 next_result

 Title      :  next_result

 Usage      :  $result_ref = $oHMMParseObject->next_result

 Function   :  Forward call to Bio::SearchIO::hmmer next_result call

 Returns    :  Bio::Search::Result::HMMERResult object

=cut

sub next_result {
    my $self = shift;
    return $self->{'_parser'}->next_result;
}

1;
