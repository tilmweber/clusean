#!/usr/bin/perl

=head1 NAME

CLUSEAN::AligAnal - Generates HMMer/Blast alignment and extracts consensus positions from $oFeatures / raw sequences

=head1 SYNOPSIS
    $oAligAnal = CLUSEAN::AligAnal->new ('-xml_Hash' => $xmlHash,
                                         '-oFeature' => $oFeature,
                                         '-program'  => 'PKSI_ER_Stereo');

    $oExtractedResiudes   = $oAligAnal->ExtractResidues(1,2,3,4);

    $HitToCompareArrayRef = $oExtractedResidues->get_aHit;
    $QueryArrayRef        = $oExtractedResidues->get_aQuery;

    my $oComparison       = $oAligAnal->next_comparisonData;



Synopsis:       -xml_Hash       : Hash reference to parsed XML-Data
                -oFeature       : Bio::SeqFeature object containing the sequence to analyse
                -aaSeq          : Amino acid to analyse (can be used alternatively to -oFeature)
                -program        : Analysis to run (defined in XML file)


        The CLUSEAN::AligAnal object provides some useful information via AUTOLOAD get_ methods

        get_choiceArray     : ArrayRef to List of different Choices
        get_choices         : ArrayRef to Hash containing the Choice data (from XML)
        get_sScaffoldOffset : Offset for scaffold (string as in XML)
        get_aScaffoldOffset : Offset for scaffold (array)
        get_sScaffoldValue  : Value for scaffold (string as in XML)
        get_aScaffoldValue  : Values for scaffold (array)

        get_query_string    : Sequence which is analyzed via external HMMer/Blast call
        get_hit_string      : Consensus sequence of HMMer/Blast hit
        get_homology_string : Homology string (provided by Bio::HSP->homology_string
        get_HSP_startQ      : Start of HSP in query sequence
        get_HSP_endQ        : End of HSP in query sequence
        get_HSP_startH      : Start of HSP in hit sequence
        get_HSP_endH        : End of HSP in hit sequence

	The CLUSEAN::AligAnal->ExtractResidues method returns a CLUSEAN::Generic object
	which only provides two get_ methods:

	get_aHit            : ArrayRef to extracted amino acids in hit sequence
	get_aQuery          : ArrayRef to extracted amino acids in query sequence


        The CLUSEAN::AligAnal->next_comparisonData method returns data defined in the Alignmet->choice
        sections of the XML file. This method does not do any evaluation/analysis. It uses a
        streaming feature (as in Bio::SeqIO->next_seq) for cycling throug the different choices.
        If there is an unprocessed "choice", it returns a CLUSEAN::Generic object with the below methods,
        otherwise it returns "undef".

	Methods returned by next_comparisonData:

        get_comment:  Text comment on the comparison made
	get_sOffset:  String of positions to extract (comma separated)
	get_sValue:   String of expected aa/nts (comma separated)
	get_aOffset:  Array reference to array containing positions to extract
	get_aValue:   Array reference to array containing the expected aa/nts
	get_result:   conclusion, if Hit/Query do match. IMPORTANT NOTE:
          The get_result method only returns the information from the XML file
          IT DOES NOT EVALUATE THE EXTRACTED RESIDUES

          All content is available via AUTOLOAD get_ methods (e.g. get_comment to get Text comment)



=head1 AUTHOR

 Tilmann Weber
 Microbiology/Biotechnology
 University of Tuebingen
 72076 Tuebingen
 Germany

 Email: tilmann.weber@biotech.uni-tuebingen.de

=cut

package CLUSEAN::AligAnal;

use strict;
use Carp;
use vars '$AUTOLOAD';
use base ("Bio::Root::Root");
use File::Temp qw(tempdir);
use File::Basename;
use FindBin qw ($Bin);
use Bio::SearchIO;
use Bio::SeqIO;
use CLUSEAN::Generic;
use Data::Dumper;

sub AUTOLOAD {

         # For AUTOLOAD to work, we must not be too strict ;-)
         no strict "refs";
         my ( $self, $newval ) = @_;

         # Do not handle DESTROY method!!!
         return if $AUTOLOAD =~ /::DESTROY$/;

         # If it's a get_XXX  method return value
         if (        ( $AUTOLOAD =~ /.*::get([_-]\w+)/ )
                  && ( $self->_accessible($1) ) )
         {
                  my $attr_name = $1;

         # Define method get_XXX to save time if the same method is called again
                  *{$AUTOLOAD} = sub { return $_[0]->{$attr_name} };
                  return $self->{$1};
         }

         # if it's a set_XXX  method set value
         if ( ( $AUTOLOAD =~ /.*::set(_\w+)/ ) ) {
                  my $attr_name = $1;
                  *{$AUTOLOAD} = sub { $_[0]->{$attr_name} = $_[1]; return };
                  $self->{$attr_name} = $newval;
                  return;
         }

  # If we arrive here, it must have been an erroneous method call so bail out...
         croak "Method $AUTOLOAD is not defined\n ";
}

=head2

Title:             _accessible

Usage:             $self->accessible('xyz')

Function:          Tests whether xyz is defined

Returns:           DEFINED/TRUE if xyz is defined

=cut

# To check whether method exists
sub _accessible {
         my ( $self, $var ) = @_;
         exists $self->{$var};
}

=head2

Title:          new

Usage:          my $oAligAnal = CLUSEAN::AligAnal->new ('-xml_Hash' => $xmlHash,
                                                        '-oFeature' => $oFeature,
                                                        '-program'  => 'PKSI_ER_Stereo');

Synopsis:       -xml_Hash       : Hash reference to parsed XML-Data
                -oFeature       : Bio::SeqFeature object containing the sequence to analyse
                -aaSeq          : Amino acid to analyse (can be used alternatively to -oFeature)
                -program        : Analysis to run (defined in XML file)


                This method also provides some useful information via AUTOLOAD methods

                get_choiceArray     : ArrayRef to List of different Choices
                get_choices         : ArrayRef to Hash containing the Choice data (from XML)
                get_sScaffoldOffset : Offset for scaffold (string as in XML)
                get_aScaffoldOffset : Offset for scaffold (array)
                get_sScaffoldValue  : Value for scaffold (string as in XML)
                get_aScaffoldValue  : Values for scaffold (array)

Returns:        CLUSEAN::AligAnal object
=cut

sub new {

         # Set up Method
         my $invocant = shift;
         my $class    = ref($invocant) || $invocant;
         my $self     = {};
         bless( $self, $class );
		# Get CLUSEANROOT
         my $CLUSEANROOT;
         if ( defined $ENV{CLUSEANROOT} ) {
                  $CLUSEANROOT = $ENV{CLUSEANROOT};
         }
         else {
                  $CLUSEANROOT = $Bin;
                  $CLUSEANROOT =~ s/(.*)\/.*?$/$1/;
         }
        
         my %aParams = (
                  '-program'   => undef,
                  "-oFeature"  => undef,
                  "-oHSP"      => undef,
                  "-aaSeq"     => undef,
                  "-xml_Hash"  => undef,
                  '-countGaps' => 1,
                  '-gapChars'  => '-.',
                  '-profiledir'=> $CLUSEANROOT."/lib",
                  @_
         );

         foreach my $HashKey ( keys %aParams ) {
                  my $sObjData = $HashKey;

                # Lets start the variables with an underscore '_' instead of '-'
                  $sObjData =~ s/^-/_/;

                  # $sMethodName = "set".$sMethodName;
                  # print "$sObjData";
                  $self->{$sObjData} = $aParams{$HashKey};
         }

         # Execute alignment
         $self->_execute;

         # Assign Alignment to analyse (from XML config) to method
         my $xml_hash = $self->get_xml_Hash;

         # get Scaffold data
         my @aScaffold =
           @{ $xml_hash->{ $self->get_program }->{Alignment}->{scaffold} };
         $self->set_scaffold( \@aScaffold );

         #get Choice data
         my @hChoices = ();
         if ( defined $xml_hash->{ $self->get_program }->{Alignment}->{choice} )
         {
                  @hChoices =
                    @{ $xml_hash->{ $self->get_program }->{Alignment}
                             ->{choice} };
         }

# Add choices-hashes to stack; NOTE: next_ComparisonData method removes one element every
# time the method is called! Don't use ->get_choices method for getting a overview on the number
# of elements; instead use ->get_choiceArray to get a list of the different choices

         $self->set_choices( \@hChoices );

         my @aChoiceArray;
         foreach my $hChoice (@hChoices) {
                  push( @aChoiceArray, $hChoice->{result} );
         }

         # print Dumper (@aChoiceArray);

         $self->set_choiceArray( \@aChoiceArray );

         # Extract scaffold information from XML-File and add to object

         # In principle, one might think about multiple scaffolds per analyis
         # This is not yet implemented, so just use the first one
         my %scaffold = %{ $self->get_scaffold->[0] };

         $self->set_sScaffoldOffset( $scaffold{'scaffoldOffset'} );
         $self->set_aScaffoldOffset(
                  [ split( ",", $scaffold{'scaffoldOffset'} ) ] );
         $self->set_sScaffoldValue( $scaffold{'scaffoldValue'} );
         $self->set_aScaffoldValue(
                  [ split( ",", $scaffold{'scaffoldValue'} ) ] );
         $self->set_aScaffoldEmission(
                  [ split( ",", $scaffold{'scaffoldEmission'} ) ] );
         $self->set_sScaffoldEmission( $scaffold{'scaffoldEmission'} );

         return $self;

}

=head2

Title: _execute (internal use only)

Function: Run external tool to generate alignment; add information to AligAnal_object

=cut

sub _execute {

         # Set up Method
         my $self    = shift;
         my %aParams = @_;

         my $xml_hash = $self->get_xml_Hash;
         my $Database =
           $xml_hash->{ $self->get_program }->{Execute}->{database};

         # Check whether database exists
         if ( !-e $self->get_profiledir."/". $xml_hash->{ $self->get_program }->{'Execute'}->{'database'} )
         {
                  $self->throw(
"ERROR: Can't find database $xml_hash->{ $self->get_program }->{Execute}->{database} in ".$self->get_profiledir."!\n"
                  );

         }

         # to help debugging: Lets start the tempfile with date/time
         my ( $sec, $min, $hour, $day, $mon, $year, $wday, $yday, $isdst ) =
           localtime;
         $year += 1900;
         $mon++;

         # Create temporary directory below $sTempDirBase
         my $sTempDirBase = $ENV{TEMP}
           || '/tmp'
           ;    # Temp dir, use /tmp if no TEMP environment variable is set
         my $sTempDir = tempdir(
                  "AligAnal_" 
                    . $year 
                    . $mon 
                    . $day 
                    . $hour 
                    . $min 
                    . $sec . "XXXX",
                  DIR     => $sTempDirBase,
                  CLEANUP => 1
         );
         open( FASTAOUT, ">$sTempDir/FeatTransl.fasta" )
           || $self->throw(
                  "Can't open $sTempDir/FeatTransl.fasta for writing!\n");

# if a $oFeature is given as parameter we have to extract the sequence and assign it to aaSeq
         if ( defined $self->{_oFeature} ) {
                  my $oFeature = $self->get_oFeature;

                  # Get DNA-sequence of feature
                  my $oOrfDNASeq = $oFeature->seq;
                  if ( $oFeature->strand eq -1 ) {
                           $oFeature->revcom;
                  }
                  $self->set_aaSeq(
                           $oOrfDNASeq->translate( undef, undef, undef, 11 )
                             ->seq );
         }
         if ( !defined $self->{_aaSeq} ) {
                  $self->throw(
"Neither amino acid sequence nor SeqFeature handed over to AligAnal object"
                  );
         }

         my $oHSP;

# Check whether we have to execute the tools or whether a oSearchIO-object was already given as parameter

         if ( !defined $self->get_oHSP ) {

                  # OK, write Fastafile
                  print FASTAOUT ">tempFasta\n";
                  print FASTAOUT $self->get_aaSeq;
                  close(FASTAOUT);
                  my $sCmdLine =
                    $xml_hash->{ $self->get_program }->{'Execute'}->{'program'};
                  $sCmdLine .= " "
                    . $xml_hash->{ $self->get_program }->{'Execute'}
                    ->{'parameters'};
                  if (
                           defined $xml_hash->{ $self->get_program }
                           ->{'Execute'}->{'dbprefix'} )
                  {
                           $sCmdLine .= " "
                             . $xml_hash->{ $self->get_program }->{'Execute'}
                             ->{'dbprefix'};
                  }
                  $sCmdLine .= " " . $self->get_profiledir."/";
                  $sCmdLine .=
                    $xml_hash->{ $self->get_program }->{'Execute'}
                    ->{'database'};
                  if (
                           defined $xml_hash->{ $self->get_program }
                           ->{'Execute'}->{'inputfileprefix'} )
                  {
                           $sCmdLine .= " "
                             . $xml_hash->{ $self->get_program }->{'Execute'}
                             ->{'inputfileprefix'};
                  }
                  $sCmdLine .= " " . $sTempDir . "/FeatTransl.fasta";
                  if ( $xml_hash->{ $self->get_program }->{'Execute'}
                           ->{'CaptureConsole'} eq "TRUE" )
                  {
                           open( OUTFILE, ">$sTempDir/Analysis.out" )
                             || $self->throw("Can't open file for tool output");
                           my $Output = `$sCmdLine`
                             || $self->throw("Error executing:\n$sCmdLine\n");
                           print OUTFILE $Output;
                           close(OUTFILE);
                  }
                  else {
                           if (
                                    defined $xml_hash->{ $self->get_program }
                                    ->{'Execute'}->{'outputfileprefix'} )
                           {
                                    $sCmdLine .= " "
                                      . $xml_hash->{ $self->get_program }
                                      ->{'Execute'}->{'outputfileprefix'};
                           }
                           $sCmdLine .= " " . $sTempDir . "/Analysis.out";
                           system($sCmdLine)
                             || $self->throw("Error executing:\n$sCmdLine\n");
                  }

# OK, now we have run the analysis (jut tested with HMMer, but Blast should also work

                  my %ParserSelect = (
                           'hmmpfam2' => 'hmmer',
                           'hmmpfam'  => 'hmmer',
                           'hmmscan'  => 'hmmer3',
                           'blastall' => 'blast'
                  );

                  my $oSearchIO = Bio::SearchIO->new(
                           '-file'   => $sTempDir . "/Analysis.out",
                           '-format' => $ParserSelect{
                                    $xml_hash->{ $self->get_program }
                                      ->{'Execute'}->{'program'}
                             }
                  );

# For this tool we only expect one Hit on the single HMM-profile in the analysis file, so we just need to look
# at the first result/hit/hsp

                  my $oResult = $oSearchIO->next_result();
                  my $oHit    = $oResult->next_hit();
                  if ( defined $oHit ) {
                           $oHSP = $oHit->next_hsp();
                  }
         }
         else {
                  $oHSP = $self->get_oHSP;
         }
         if ( defined $oHSP ) {

                  $self->set_query_string( $oHSP->query_string );

                  # This is the sequence we query
                  $self->set_hit_string( $oHSP->hit_string );

                  # This is the consensus sequence of the HMM-Profile/BlastHit
                  $self->set_homology_string( $oHSP->homology_string );

            # This is the homology string (useful for reformatting the alignment
                  $self->set_HSP_startQ( $oHSP->start('query') );
                  $self->set_HSP_endQ( $oHSP->end('query') );
                  $self->set_HSP_startH( $oHSP->start('hit') );
                  $self->set_HSP_endH( $oHSP->end('hit') );
         }
         else {
                  $self->set_query_string(undef);
                  $self->set_hit_string(undef);
                  $self->set_homology_string(undef);
                  $self->set_HSP_startQ(undef);
                  $self->set_HSP_endQ(undef);
                  $self->set_HSP_startH(undef);
                  $self->set_HSP_endH(undef);
         }
}

=head2

Title: ExtractResidues

Synopsis:   $oResidues = $oAligAnal->ExtractResidues((4,5,6,7));

Function:   Extracts aminoacids/nucleotides at positions 3,4,5,6 of alignment (relative to hit HMM-Model/full hit sequence)

Returns:    A CLUSEAN::Generic object with content

            aQuery:   Returns Array-reference to Array of extracted aa/nts of Query sequence
            aHit:     Returns Array-reference to Array of extracted aa/nts of Hits sequence

            Content is available via AUTOLOAD get_ methods (get_aQuery/get_aHit)

=cut

sub ExtractResidues {

         my $self = shift;

         my @aResidues = @_;

         my @aHit;
         my @aQuery;
         my %xml_hash = %{ $self->get_xml_Hash };

         my $lastGapOffset = 0
           ; # Save number of gaps in last hit in this context as we have to add this number
             # for identifying the gaps for the next residue
             # This might be just a hack, there may be situations where this algoritm fails
             # Haven't had the time yet to analyse this properly

         foreach my $Residue (@aResidues) {
                  my $gapOffset = 0;
                  $Residue = $Residue - $self->get_HSP_startH;

                  if ( $self->get_countGaps eq "1" ) {

# To count gaps before $Residue in Hit-String, get substring starting at the beginning up to the
# residue we want to extract (including gaps analysed in the last run
                           my $sHitTemp = substr( $self->get_hit_string, 0,
                                    $Residue - 1 + $lastGapOffset );
                           my $GapChars = $self->get_gapChars;
                           $gapOffset++ while $sHitTemp =~ /[$GapChars]/g;
                           $lastGapOffset = $gapOffset;

                           # print "Residue: $Residue, Gaps: $gapOffset.\n";
                           if (
                                    substr(
                                             $self->get_hit_string,
                                             $Residue + $gapOffset,
                                             1
                                    ) =~ /[$GapChars]/
                             )
                           {
                                    $gapOffset++;
                           }
                  }

                  if ( $Residue < 1 ) {
                           push @aQuery, "-";
                           push @aHit,   "-";
                  }
                  else {
                           my $Char = substr( $self->get_query_string,
                                    $Residue + $gapOffset, 1 );
                           push @aQuery, $Char;
                           $Char = substr( $self->get_hit_string,
                                    $Residue + $gapOffset, 1 );
                           push @aHit, $Char;
                  }
         }

         $self->set_aHitResidues( \@aHit );
         $self->set_aQueryResidues( \@aQuery );

         # Check for matches against <value>

         my $oExtrResidues = CLUSEAN::Generic->new(
                  '-aHit'   => \@aHit,
                  '-aQuery' => \@aQuery
         );
         return ($oExtrResidues);

}

=head2

Title:      next_comparisonData

Function:   Compares extracted residues to values provided in XML file

Returns:    CLUSEAN::Generic object with content:

            comment:  Text comment on the comparison made
            sOffset:  String of positions to extract (comma separated)
            sValue:   String of expected aa/nts (comma separated)
            sOffset:  Array reference to array containing positions to extract
            aValue:   Array reference to array containing the expected aa/nts
            result:   conclusion, if Hit/Query do match. IMPORTANT NOTE:
              The get_result method only returns the information from the XML file
              IT DOES NOT EVALUATE THE EXTRACTED RESIDUES

              All content is available via AUTOLOAD get_ methods (e.g. get_comment to get Text comment)

=cut

sub next_comparisonData {

         my $self = shift;

         my %scaffold = %{ $self->get_scaffold->[0] };

         my $choiceRef = shift( @{ $self->{_choices} } ) || return;

         my %choice = %{$choiceRef};

         my $oData = CLUSEAN::Generic->new(
                  '-comment'        => $choice{'comment'},
                  '-sValue'         => $choice{'value'},
                  '-aValue'         => [ split( ",", $choice{'value'} ) ],
                  '-sOffset'        => $choice{'offset'},
                  '-aOffset'        => [ split( ",", $choice{'offset'} ) ],
                  '-sValueEmission' => $choice{'valueEmission'},
                  '-aValueEmission' =>
                    [ split( ",", $choice{'valueEmission'} ) ],
                  '-result' => $choice{'result'}
         );

         return ($oData);
}

1;
