#!/usr/bin/perl

=head1 NAME

CLUSEAN::MapHSPtoCDS - Maps protein coordinates of HSP to nucleotide coordinates (including introns/exons)

=head1 SYNOPSIS

$oHSPmap = CLUSEAN::MapHSPtoCDS->new('-HSP'        => $oHSP,
                          			 '-SeqFeature' => $oFeature);

my $oLocation = $oHSPmap->getLocation;

=head1 DESCRIPTION

Maps coordinates of Blast or HMMer HSP to DNA coordinates of SeqFeature. Introns are automatically introduced
at the correct position, if they are included in the SeqFeature

Returns a Bio::Location object

=cut

package CLUSEAN::MapHSPtoCDS;

use strict;
use base ("Bio::Root::Root");
use Bio::Location::Simple;
use Bio::Location::Fuzzy;
use Bio::Location::Split;


=head2 new

Title:    new

Function: Constructur to set up Mapper object

Usage: 
       $oHSPmap = CLUSEAN::MapHSPtoCDS->new('-HSP'        => $oHSP,
                                            '-SeqFeature' => $oFeature);
                                            
=cut


sub new {

    # Set up Method
    my $invocant = shift;
    my $class    = ref($invocant) || $invocant;
    my %aParams  = (@_);
    my $self     = {};
    bless( $self, $class );
    my $oHSP = $aParams{'-HSP'};
    my $oFeature = $aParams{'-SeqFeature'};
    
    if (!defined $oHSP) {
    	$self->throw ("Required parameter -HSP missing")
    }
     if(! $oHSP->isa('Bio::Search::HSP::GenericHSP')) {
    	$self->throw ("Wrong parameter -HSP; Given -HSP argument is of type " . ref $oHSP ." but required is Bio::Search::HSP");
    }
    if (! defined $oFeature) {
    	$self->throw ("Required parameter -SeqFeature missing")
    }
    if (!$oFeature->isa('Bio::SeqFeature::Generic')) {
    	$self->throw ("Wrong or missing parameter -HSP; Given -HSP argument is of type " . ref $oFeature ." but required is Bio::SeqFeature::Generic");
    }
    
    $self->{oHSP} = $oHSP;
    $self->{oFeature} = $oFeature;
    
    
    return $self;
}


=head2 getLocation

Function: Do the actual HSP to SeqFeature mapping

Usage:    my $oLocation = $oHSPmap->getLocation;

Returns:  A Bioperl Bio::Location object

=cut

sub getLocation {
	
	my $self = shift;
	my $exon;
	my @aLocations;
	
	my $oHSP = $self->{oHSP};
	my $oFeature = $self->{oFeature};

	# Get location information

	my $CDSfeatLocation = $oFeature->location;


	# print "======================\n\n";

# Calculate start and end positions without respecting probable intron sequences
	# print "Feature coordinates: ",$oFeature->start,"..",$oFeature->end,"; strand",$oFeature->strand,"\n";
	# print "Hsp-coordinates: ",$oHSP->start,"..",$oHSP->end,"\n";
	my ( $start, $end );
	if ( $oFeature->strand eq 1 ) {
		$start = $oFeature->start + ( 3 * $oHSP->start ) - 3;
		$end   = $oFeature->start + ( 3 * $oHSP->end ) - 1;
	}
	else {
		my $oFeature_length = $oFeature->seq->translate->length;
		# print "Feature has length $oFeature_length\n";
		$end   = $oFeature->end - ( 3 * $oHSP->start ) + 3;
		$start = $oFeature->end - ( 3 * $oHSP->end ) + 1;
		# print "calculated start/end: $start..$end\n";
	}

# If we deal with bacterial sequences or sequences without introns, just add the coordinates
	if ( $CDSfeatLocation->isa('Bio::Location::Simple') ) {
		my $loc = Bio::Location::Simple->new(
			'-start'  => $start,
			'-end'    => $end,
			'-strand' => $oFeature->strand
		);
		push( @aLocations, $loc );
	}

	elsif ( $CDSfeatLocation->isa('Bio::Location::Fuzzy') ) {
		my $loc = Bio::Location::Fuzzy->new(
			'-start'  => $start,
			'-end'    => $end,
			'-strand' => $oFeature->strand
		);
		push( @aLocations, $loc );
	}

	# If the feature contains introns, the included location object is of type
	# Bio::Location::Split, which contains the exon coordinates
	# Therefore we have to iterate through all exons

	elsif ( $CDSfeatLocation->isa('Bio::Location::Split') ) {

 # Check splittype to be sure, that feature describes a normal gene with introns

		die "$oFeature has unsupported split type"
		  if ( $CDSfeatLocation->splittype ne "JOIN" );

		my $last_exon_end = $oFeature->start;
		my $last_exon_start=$oFeature->end;

		# Now cycle through every exon (sorted)
		foreach my $exon (
			sort {
				if   ( $oFeature->strand eq 1 ) { $a->start <=> $b->start }
				else                           { $b->start <=> $a->start }
			} $CDSfeatLocation->each_Location
		  )
		{
			# We have to use a different ruleset for features on plus and minus strand
			if ( $oFeature->strand eq 1 ) {
					
				my $exon_offset = $exon->start - $last_exon_end;

				# print "Exon offset: $exon_offset; Exon ", $exon->start, "..",
				#  $exon->end, "\n";

				# print "\t last_exon_end: $last_exon_end; Start/end before recalculation: $start..$end\n";

				# exon ends before hsp
				if ( $exon->end <= $start + $exon_offset ) {

				   # print "exon ends before hsp don't write location yet...\n";
					$start += $exon_offset;
					$end   += $exon_offset;
				}

				# hsp is completely contained in exon
				if (   ( $exon->start <= $start + $exon_offset )
					&& ( $exon->end >= $end + $exon_offset ) )
				{

					$start += $exon_offset;
					$end   += $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $start,
						'-end'    => $end,
						'-strand' => $exon->strand
					);

					# print "\texon contains whole hsp; writing location $start..$end\n";
					push( @aLocations, $loc );
				}

				# whole exon is part of HSP
				if (   ( $exon->start >= $start + $exon_offset )
					&& ( $exon->end <= $end + $exon_offset ) )
				{

					$end += $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $exon->start,
						'-end'    => $exon->end,
						'-strand' => $exon->strand
					);

					# print "\twhole exon is part of hsp ",$exon->start,"..",$exon->end,"\n";
					push( @aLocations, $loc );
				}

				# hsp starts within exon and does exceed exon
				if (   ( $exon->start <= $start + $exon_offset )
					&& ( $exon->end >= $start + $exon_offset )
					&& ( $exon->end <= $end + $exon_offset ) )
				{

					$start += $exon_offset;
					$end   += $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $start,
						'-end'    => $exon->end,
						'-strand' => $exon->strand
					);

					# print "\thsp starts within hsp exceeds exon; writing location $start..",$exon->end,"\n";
					push( @aLocations, $loc );

				}

				# hsp ends in exon
				if (   ( $exon->end >= $end + $exon_offset )
					&& ( $exon->start >= $start + $exon_offset )
					&& ( $exon->start <= $end + $exon_offset ) )
				{

					$end += $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $exon->start,
						'-end'    => $end,
						'-strand' => $exon->strand
					);

					# print "\thsp started before and ends within exon; writing location ",$exon->start,"..$end\n";
					push( @aLocations, $loc );
				}

				# print "Start/end after recalculation: $start..$end\n";

				$last_exon_end = $exon->end + 1;
			}
			else {

				#
				#
				#	MINUS STRAND
				#
				#
				#
				
				my $exon_offset = $last_exon_start - $exon->end;

				# print "Exon offset: $exon_offset; Exon ", $exon->start, "..", $exon->end, "\n";

				# print "\tlast_exon_end: $last_exon_end; Start/end before recalculation: $start..$end\n";

				# exon before hsp
				#if ( $exon->start >= $start - $exon_offset ) {
				if ( $exon->start >= $end - $exon_offset ) {
					# print "exon ends before hsp don't write location yet...\n";
					$start -= $exon_offset;
					$end   -= $exon_offset;
				}

				# hsp is completely contained in exon
				if (   ( $exon->start <= $start - $exon_offset )
					&& ( $exon->end >= $end - $exon_offset ) )
				{
					$start -= $exon_offset;
					$end   -= $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $start,
						'-end'    => $end,
						'-strand' => $exon->strand
					);

					# print "\texon contains whole hsp; writing location $start..$end\n";
					push( @aLocations, $loc );
				}

				# whole exon is part of HSP
				if (   ( $exon->start >= $start - $exon_offset )
					&& ( $exon->end <= $end - $exon_offset ) )
				{

					$start -= $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $exon->start,
						'-end'    => $exon->end,
						'-strand' => $exon->strand
					);

					# print "\twhole exon is part of hsp ",$exon->start,"..",$exon->end,"\n";
					push( @aLocations, $loc );
				}

				# hsp starts within exon and does exceed exon
				if (   ( $exon->start <= $start - $exon_offset )
					&& ( $exon->end >= $start - $exon_offset )
					&& ( $exon->end <= $end - $exon_offset ) )
				{

					$start -= $exon_offset;
					$end   -= $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $start,
						'-end'    => $exon->end,
						'-strand' => $exon->strand
					);

					# print "\thsp starts within hsp exceeds exon; writing location $start..",$exon->end,"\n";
					push( @aLocations, $loc );

				}

				# hsp ends in exon
				if (   ( $exon->end >= $end - $exon_offset )
					&& ( $exon->start >= $start - $exon_offset )
					&& ( $exon->start <= $end - $exon_offset ) )
				{

					$end -= $exon_offset;
					$start -= $exon_offset;

					my $loc = Bio::Location::Simple->new(
						'-start'  => $exon->start,
						'-end'    => $end,
						'-strand' => $exon->strand
					);

					# print "\thsp started before and ends within exon; writing location ",$exon->start,"..$end\n";
					push( @aLocations, $loc );
				}

				# print "Start/end after recalculation: $start..$end\n";

				$last_exon_start = $exon->start - 1;

			}	
		}
	}

	# Just to be sure, that nothing unusual happens here...
	else {
		die(    "Unknown location format of SeqFeature: "
			  . ref( $oFeature->location )
			  . "\n" );
	}

	# Now set up location object which is returned from function

	my $location;

	die "No location assigned to \@aLocations" if ( scalar(@aLocations) == 0 );

	# If there is only on location object in @aLocation, return it as a
	# Bio::Location::Simple object
	if ( scalar(@aLocations) == 1 ) {
		$location = $aLocations[0];
	}

# If there are more locations objects in @aLocation, this indicates introns/exons,
# so we have to return a Bio::Location::Split object
	else {
		$location = Bio::Location::Split->new();

		foreach my $locref (@aLocations) {
			$location->add_sub_Location($locref);
		}
	}

	return ($location);
	
}

1;