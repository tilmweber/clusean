#!/usr/bin/env python
#
# Run the general annotation calculation
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

import os
from blast import parallel_blast as blast
from hmmer import hmmscan, hmmpfam
from nrpspredictor import nrpspredictor

def calculate_annotation(step, config):
    """Calculate the annotation data using BLAST and HMMer"""
    blastdb = config['Environment']['BLASTDB'] + os.sep + config['Blast']['DB']
    hmmerdb = config['Environment']['HMMERDB'] + os.sep + config['HMMer']['DB']
    keggdb = config['Environment']['BLASTDB'] + os.sep + config['ECpred']['DB']

    blastdir = config['Blast']['Dir']
    hmmerdir = config['HMMer']['Dir']
    keggdir = config['ECpred']['Dir']

    cpus = config['Environment'].get('CPUS', None)
    if cpus is not None:
        cpus = int(cpus)

    skip_blast = (config['Environment'].has_key('RUN_BLAST') and
                  config['Environment']['RUN_BLAST'] == "false")

    skip_hmmer = (config['Environment'].has_key('RUN_HMMER') and
                  config['Environment']['RUN_HMMER'] == "false")

    skip_kegg  = (config['Environment'].has_key('RUN_BLAST_KEGG') and
                  config['Environment']['RUN_BLAST_KEGG'] == "false")

    if not skip_blast:
        blast(blastdir, blastdb, cpus)

    if not skip_hmmer:
        hmmscan(hmmerdir, hmmerdb, cpus)

    if not skip_kegg:
        blast(keggdir, keggdb, cpus)

    return 0

def calculate_ab_specific_annotation(step, config):
    """Calculate the antibiotics-specific annotation using HMMer and NRPSPredictor"""
    root = config['Global']['CLUSEANROOT']
    cpus = config['Environment'].get('CPUS', None)
    if cpus is not None:
        cpus = int(cpus)
    skip_nrpspredictor = (config['Environment'].has_key('RUN_NRPSPREDICTOR') and
                          config['Environment']['RUN_NRPSPREDICTOR'] == 'false')

    abdomdir = 'ABDomains'
    abdomdb = root + os.sep + 'lib' + os.sep + 'AB_domains.hmm2'

    abmotifdir = 'ABmotifs'
    abmotifdb = root + os.sep + 'lib' + os.sep + 'AB_motifs.hmm2'

    nrpsdir= 'NRPSPred'

    hmmpfam(abdomdir, abdomdb, cpus)
    hmmpfam(abmotifdir, abmotifdb, cpus)
    if not skip_nrpspredictor:
        nrpspredictor(nrpsdir)
    return 0
