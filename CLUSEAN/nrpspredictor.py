#!/usr/bin/env python
#
# Run NRPSPredictor
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>
import os
from utils import dispatch_to_file, filter_files
from progressbar import *

def nrpspredictor(directory):
    """Run NRPSPredictor"""

    nrps_files = filter_files(os.listdir(directory), ".fasta")

    widgets = ["Waiting for NRPSPredictor to finish: ",
               RotatingMarker(), " ", Percentage(), " ", Bar()]
    pbar = ProgressBar(widgets=widgets, maxval=len(nrps_files)).start()

    i = 0

    for f in nrps_files:
        f = directory + os.sep + f
        args = ['NRPSPredWrap.pl', '--input', f]
        dispatch_to_file(args, f.replace(".fasta", ".out"))
        pbar.update(i+1)
        i += 1

    pbar.finish()
