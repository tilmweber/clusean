#!/usr/bin/perl

=head1 NAME

CLUSEAN::AnnotateHMMHits - Annotate CDS features with results from HMMer output

=head1 SYNOPSIS

$in_seq = Bio::SeqIO->new(-file => "file.embl,
                          -format => 'embl');

use CLUSEAN::AnnotateHMMHits
$out_seq = annotateHMMHits(-sequence => $in_seq);

=cut

package CLUSEAN::AnnotateHMMHits;
require Exporter;
our @ISA     = qw(Exporter);
our @EXPORT  = qw(annotateHMMHits);
our $VERSION = 1.001;

use strict;
use Bio::SearchIO;
use Bio::SeqFeature::Generic;
use Bio::Location::Simple;
use Bio::Location::Fuzzy;
use Bio::Location::Split;
use CLUSEAN::NameToPfam;
use CLUSEAN::MapHSPtoCDS;

sub annotateHMMHits {
	my %params = (
		'-config'   => {},
		'-hmmertag' => undef,
		'-feattag'  => undef,
		'-basedir'  => '',
		'-evalue'   => 0.01,
		'-score'    => 0,
		'-verbose'  => 0,
		@_
	);
	my $seq    = $params{'-sequence'};
	my %config = %{ $params{'-config'} };

	my $hmmertag = $params{'-hmmertag'};
	if ( !defined($hmmertag) ) {
		$hmmertag = $config{HMMer}->{Tag};
	}

	if ( !defined( $params{'-feattag'} ) ) {
		$params{'-feattag'} = $config{HMMer}->{HMMerQual};
	}

	foreach my $feature ( _filter_cds_features($seq) ) {
		next unless ( $feature->has_tag($hmmertag) );

		my ($hmmfile) = $feature->get_tag_values($hmmertag);
		$hmmfile = $params{'-basedir'} . $hmmfile;

		if ( !-e $hmmfile ) {
			die "Error: HMM report at $hmmfile not found! Bailing out...";
		}

		if ( $params{-verbose} ) {
			print "processing $hmmfile\n";
		}

		my $hmmreport = Bio::SearchIO->new(
			'-file'    => $hmmfile,
			'-format'  => 'hmmer',
			'-verbose' => $config{'-verbose'}
		);

		_annotate_seq_with_hmmreport( $seq, $feature, $hmmreport, \%params );

	}
	return $seq;
}

sub _filter_cds_features {
	my $seq = shift;
	return grep ( $_->primary_tag eq 'CDS', $seq->top_SeqFeatures );
}

sub _annotate_seq_with_hmmreport {
	my ( $seq, $feature, $report, $config ) = @_;
	my $evalue = $$config{'-evalue'};
	my $score  = $$config{'-score'};
	my $tag    = $$config{'-feattag'};

	die "tag is undefined" unless defined($tag);

	while ( my $result = $report->next_result ) {
		while ( my $hit = $result->next_hit ) {
			while ( my $hsp = $hit->next_hsp ) {
				next if ( _is_low_quality_hsp( $hsp, $evalue, $score ) );

				my $oMapper = CLUSEAN::MapHSPtoCDS->new ('-HSP'       => $hsp,
														 '-SeqFeature'=> $feature);
														 
				my $location = $oMapper->getLocation;

# The coordinates of the new feature are included in the $location object
# To support eukaryotic sequences with introns, we cannot use the -start/-end parameters
# in the constructor but have to submit a complete Bio::LocationI (Bio::Location::Simple or
# Bio::Location::Split) object.

				my $hmmfeature = Bio::SeqFeature::Generic->new(
					'-primary_tag' => $tag,
					'-location'    => $location
				);

				$hmmfeature->add_tag_value( 'note',  $hit->description );
				$hmmfeature->add_tag_value( 'label', $hit->name );

				if ( $feature->has_tag('locus_tag') ) {
					my ($tag) = $feature->get_tag_values('locus_tag');
					$hmmfeature->add_tag_value( 'locus_tag', $tag );
				}

				my $desc = $result->hmm_name . "-Hit: ";
				$desc .= $hit->name . ". ";
				$desc .= "Score: " . $hsp->score . ". ";
				$desc .= "E-value: " . $hsp->evalue . ". ";
				$desc .=
				  "Domain range: " . $hsp->start . ".." . $hsp->end . ".";
				$hmmfeature->add_tag_value( 'note', $desc );
				my $pfam_id = name_to_pfam( $hit->name );
				if ( defined($pfam_id) ) {
					$hmmfeature->add_tag_value( 'note', "PFAM-Id: $pfam_id" );
				}

				$seq->add_SeqFeature($hmmfeature);

			}
		}
	}
}

sub _is_low_quality_hsp {
	my ( $hsp, $evalue, $score ) = @_;
	return ( $hsp->evalue > $evalue or $hsp->score < $score );
}

1;
