
=head1 NAME

CLUSEAN::GetIGS - Generic object to process NRPSPredictor output files

=head1 SYNOPSIS

$oIGS = CLUSEAN::GetIGS->new('-aoFeatures'   => \@aFeatures, 
							 '-SeqObj'       => \$oSeq,
							 '-truncLength'    => $iMinLength);
$oIGSFeature = $oIGS->nextIGS()

=head1 DESCRIPTION

This Object identifies and extracts intergenic regions in bacterial DNA sequences.
By default, they are returned as Bio::SeqFeature::Generic objects

=head1 INCLUSIONS

GetIGS.pm includes following 3rd Party modules:

Bio::Root::Root
Bio::SeqFeature::Generic
Bio::Seq

=head1 EXAMPLES

my $oSeqIO=Bio::SeqIO->new ('-file'    =>  $sInputSeqFn,
			                '-format'  =>  'EMBL');
			                
while (my $oSeq = $oSeqIO->next_seq) {

	my @aFeatures = (grep (  ($_->primary_tag eq 'CDS') ,
	   	                      $oSeq->top_SeqFeatures));	
	   	                      
	my $oIGS = CLUSEAN::GetIGS->new('-aoFeatures'   => \@aFeatures,
									'-SeqObj'       => \$oSeq);
									
    while (my $oIGSFeature = $oIGS->nextIGS()) {
    	if ($oIGSFeature->has_tag('gene')) {
    		print join (",", $oIGSFeature->get_tag_values('gene')),"\t";
    	}
    	else {
    		print "undef\t";
    	}
    	print $oIGSFeature->start,"\t",$oIGSFeature->end,"\t";
    	print $oIGSFeature->strand,"\t", $oIGSFeature->seq->seq,"\n";	
    }
	
}

=cut

package CLUSEAN::GetIGS;
use strict;
use vars ('$AUTOLOAD');
use base ("Bio::Root::Root");
use Bio::SeqFeature::Generic;
use Bio::Seq;

=head2

Title:      Autoload

Usage:      No direct execution

Function    This method is executed for every set_XXX or get_XXX method call that has no
            explicit method definition.
            
            You can store almost everything in via these methods but you have to
            care for yourself that you actually retrieve what you have stored...
            
Returns:    get_XXX : Value of XXX
            set_XXX : Defines XXX with value given as parameter            
            
            
=cut

# OK, let's start some perl magic with autoloading...
sub AUTOLOAD
{

	# For AUTOLOAD to work, we must not be too strict ;-)
	no strict "refs";
	my ( $self, $newval ) = @_;

	# Do not handle DESTROY method!!!
	return if $AUTOLOAD =~ /::DESTROY$/;

	# If it's a get_XXX  method return value
	if (    ( $AUTOLOAD =~ /.*::get([_-]\w+)/ )
		 && ( $self->_accessible($1) ) )
	{
		my $attr_name = $1;

		# Define method get_XXX to save time if the same method is called again
		*{$AUTOLOAD} = sub { return $_[0]->{$attr_name} };
		return $self->{$1};
	}

	# if it's a set_XXX  method set value
	if ( ( $AUTOLOAD =~ /.*::set(_\w+)/ ) )
	{
		my $attr_name = $1;
		*{$AUTOLOAD} = sub { $_[0]->{$attr_name} = $_[1]; return };
		$self->{$attr_name} = $newval;
		return;
	}

  # If we arrive here, it must have been an erroneous method call so bail out...
	$self->throw("Method $AUTOLOAD is not defined\n ");
}

=head2

Title:             _accessible

Usage:             $self->accessible('xyz')

Function:          Tests whether xyz is defined

Returns:           DEFINED/TRUE if xyz is defined

=cut

# To check whether method exists
sub _accessible
{
	my ( $self, $var ) = @_;
	exists $self->{$var};
}

=head2

Title:             new

Usage:             my $oIGS = CLUSEAN::GetIGS->new('-aoFeatures'   => \@aFeatures,
									               '-SeqObj'       => \$oSeq,
									               '-AnnoTag'      => 'misc_feature');
									               
Parameters (required):
                   -aoFeatures   =>  Reference to array of Bio::SeqFeature objects
                   -SeqObj       =>  Reference to Bio::Seq object
                                                       				               
Function:          Constructor for GetIGS objects

Parameters (optional):
                   -AnnoTag       =>  Primary tag used for Bio::SeqFeature   
                   -truncLength   =>  Truncate returned sequence to nn bases
                   -upstreamBases =>  Return nn upstream bases of each CDS

Returns:           GetIGS object

=cut									               

sub new
{
	my $invocant = shift;
	my $class    = ref($invocant) || $invocant;
	my $self     = {};
	bless( $self, $class );

	# Get parameters
	my %aParams = @_;
	$self->set_AnnoTag('misc_feature');

# Store values given in parameter hash
# By AUTOLOAD we instantly generate get_XXX Methods for each parameter without writing a single
# line of code :-)
	foreach my $HashKey ( keys %aParams )
	{
		my $sObjData = $HashKey;

		# Lets start the variables with an underscore '_' instead of '-'
		$sObjData =~ s/^-/_/;

		# $sMethodName = "set".$sMethodName;
		# print "$sObjData";
		$self->{$sObjData} = $aParams{$HashKey};
	}
	if ( !defined $self->{_aoFeatures} )
	{
		$self->throw("Parameter -aoFeatures required in constructur call");
	}

	# my $oEmptyFeature = Bio::SeqFeature::Generic->new ( '-start' =>  0,
	# 													'-end'   =>  0,
	# 													'-strand'=>  1);
	# $self->set_prevFeature( \$oEmptyFeature );
	$self->set_prevFeature(undef);
	$self->set_CDSFeature( $self->_getCDSFeat );
	$self->set_nextFeature( $self->_getCDSFeat );
	return $self;
}

=head2

Usage:           $oCDSFeature->_getCDSFeat

Function:        Get Bio::SeqFeature object from Feature stack,
				 internal use only
				 
Returns:		 Bio::SeqFeature object with primaryTag="CDS"

=cut

sub _getCDSFeat
{

	# we have to dereference here, so we can't use the AUTOLOAD get methods
	my $self = shift;
	return ( shift( @{ $self->{_aoFeatures} } ) );
}

=head2

Usage:          $self->_shift

Function:       Proceed to next feature of Feature Stack
                Internal use only!!!
                
Returns:        nothing

=cut

sub _shift
{
	my $self = shift;
	$self->set_prevFeature( $self->get_CDSFeature );
	$self->set_CDSFeature( $self->get_nextFeature );
	$self->set_nextFeature( $self->_getCDSFeat );
}

=head 2

Usage:         $oIGS->nextIGS;

Function:      Get intergenic sequence

Returns:       Bio::SegFeature::Generic object with coordinates of intergenic sequence
			   and annotation of current feature

=cut

sub nextIGS
{
	my $self = shift;

	# get previous, current and next Feature
	my $prevFeature = $self->get_prevFeature;
	my $CDSFeature  = $self->get_CDSFeature;

	# return undef if we are at the last Feature
	return if ( !defined $CDSFeature );
	my $nextFeature = $self->get_nextFeature;

	# get corresponding seq object
	my $oSeq = ${ $self->get_SeqObj };

	# Get coordinates of IGS
	my $IGSStart;
	my $IGSEnd;
	my $IGSStrand = $CDSFeature->strand;
	if ( $IGSStrand eq "1" )
	{
		if ( defined $prevFeature )
		{
			$IGSStart = $prevFeature->end;
		}
		else
		{
			$IGSStart = 1;
		}
		$IGSEnd = $CDSFeature->start;
	}
	else
	{
		$IGSStart = $CDSFeature->end;
		if ( defined $nextFeature )
		{
			$IGSEnd = $nextFeature->start;
		}
		else
		{
			$IGSEnd = $oSeq->length;
		}
	}

# If the parameter --upstreamBases nn is handed to to object, return nn upstream bases
# (irrespective wheter these bases are in an IGS or in a gene) instead of only IGSs

if ( defined $self->{_upstreamBases}) {
	if ( $IGSStrand == 1) {
		if ($IGSEnd - $self->get_upstreamBases > 1) {
			$IGSStart = $IGSEnd - $self->get_upstreamBases;
		}
		else {
			$IGSStart = 1
		}
	}
	else {
		if ($IGSStart + $self->get_upstreamBases < $oSeq->length ) {
			$IGSEnd = $IGSStart + $self->get_upstreamBases;
		}
		else {
			$IGSEnd = $oSeq->length
		}
	}	
	
	
}



# if there are overlapping start and stop codons, just return the base of the sequence start
# we have to return a oFeature object with coordinates (instead returning undef)
# as otherwise cycling through the results with the
# nextIGS method in while or foreach loop would not be possible
	if ( $IGSStart > $IGSEnd )
	{
		$IGSStart = $IGSEnd;
	}
	if ( defined $self->{_truncLength} )
	{
		if ( $IGSEnd - $IGSStart > $self->get_truncLength )
		{
			if ( $IGSStrand == "1" )
			{
				$IGSStart = $IGSEnd - $self->get_truncLength;
			}
			else
			{
				$IGSEnd = $IGSStart + $self->get_truncLength;
			}
		}
	}


# Setup new Bio::SeqFeature object with the coordinates of the IGS sequence and the annotation
# of the corresponding CDS feature
	my $oIGSFeature = Bio::SeqFeature::Generic->new(
											 '-start'      => $IGSStart,
											 '-end'        => $IGSEnd,
											 '-strand'     => $IGSStrand,
											 '-primaryTag' => $self->get_AnnoTag
	);
	if ( $CDSFeature->has_tag('gene') )
	{
		$oIGSFeature->add_tag_value(
									 'gene',
									 join( ",",
										   $CDSFeature->get_tag_values('gene') )
		);
	}
	if ( $CDSFeature->has_tag('note') )
	{
		$oIGSFeature->add_tag_value(
									 'note',
									 join( ",",
										   $CDSFeature->get_tag_values('note') )
		);
	}
	if ( $CDSFeature->has_tag('product') )
	{
		$oIGSFeature->add_tag_value(
									 'product',
									 join( ",",
										  $CDSFeature->get_tag_values('product')
									 )
		);
	}
	if ( $CDSFeature->has_tag('locus_tag') )
	{
		$oIGSFeature->add_tag_value(
									 'locus_tag',
									 join(
										   ",",
										   $CDSFeature->get_tag_values(
																	'locus_tag')
									 )
		);
	}
	if ( $CDSFeature->has_tag('function') )
	{
		$oIGSFeature->add_tag_value(
									 'function',
									 join(
										   ",",
										   $CDSFeature->get_tag_values(
																	 'function')
									 )
		);
	}
	if ( $CDSFeature->has_tag('label') )
	{
		$oIGSFeature->add_tag_value(
									 'label',
									 join(
										   ",",
										   $CDSFeature->get_tag_values(
																	 'label')
									 )
		);
	}

    # To allow use of the seq method, we have to attach the original $oSeq object
    # Again, this is very memory consuming but it works!
 	$oIGSFeature->attach_seq($oSeq);

	# Move to the next member of the @aoFeatures-Array
	$self->_shift;

	# return Bio::SeqFeature object of IGS
	return $oIGSFeature;
}
1;
