#!/usr/bin/env python
#
# Pipeline helpers for CLUSEAN
# Author: Kai Blin <kai.blin@biotech.uni-tuebingen.de>

class Pipeline:
    """An object collecting all function calls to the CLUSEAN modules"""

    def __init__(self):
        self.functions = []

    def register(self, func):
        """Register a function for the pipeline"""
        self.functions.append(func)

    def list_steps(self):
        """List available steps in the pipeline"""
        step = 1
        ret = "Available steps in the pipeline:\n"
        for func in self.functions:
            ret += "Step %s\t%35s:\t%s\n" % (step,
                                             func.func_name.replace('_', ' '),
                                             func.func_doc)
            step += 1

        return ret

    def count(self):
        """Get the number of steps in the pipeline"""
        return len(self.functions)

    def get_step(self, step):
        """Get a specified step"""
        return self.functions[step - 1]

