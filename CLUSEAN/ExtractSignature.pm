=head1 NAME

CLUSEAN::ExtractSignature - Extract a signature given a list of positions and an alignment

=head1 SYNOPSIS

=cut

package CLUSEAN::ExtractSignature;
require Exporter;
our @ISA      = qw(Exporter);
our @EXPORT   = qw(extractStachelhaus extract8Angstrom getModuleSequences);
our $VERSION  = 1.001;

use strict;

sub extractStachelhaus {
    my @stachelhaus_pos = ([46, 47, 50, 92, 124, 126, 153, 161, 162], [36]);
    return _extractSignatures(\@_, \@stachelhaus_pos);
}

sub extract8Angstrom {
    my @eightA_pos = ([12, 15, 16, 40, 45..51, 54, 92, 93, 124..128, 151..166], []);
    return _extractSignatures(\@_, \@eightA_pos);
}

sub _extractSignatures {
    my @seqs = @{shift()};
    my @pos = @{shift()};
    my @sigs;

    foreach my $seq (@seqs) {
        my ($amp, $lys, $name) = @{$seq};
        my $sig_string = _extractSignature($amp, $lys, @pos);
        push @sigs, [$sig_string, $name];
    }
    return @sigs;
}

sub _extractSignature {
    my ($amp, $lys, $amp_pos, $lys_pos) = @_;

    my $sig_string = _extractPositions($amp, $amp_pos);
    $sig_string .= _extractPositions($lys, $lys_pos);

    return $sig_string;
}

sub _extractPositions {
    my $string = shift;
    my @positions = @{shift()};
    my $result = "";

    foreach my $pos (@positions) {
        $result .= uc substr($string, $pos, 1);
    }
    return $result;
}

sub getModuleSequences {
    my $seq_obj = shift;

    my @hits;

    while(my $res = $seq_obj->next_result) {
        my @prot_hits;
        while(my $hit = $res->next_hit) {
            push @prot_hits, $hit;
        }
        push @hits, [$res->query_name, \@prot_hits];
    }

    my @seqs;

    foreach my $query (@hits) {
        my $name = $$query[0];
        my $hit_ref  = $$query[1];
        my @prot_hits = @{$hit_ref};
        my $idx = 0;

        for(my $i = 0; $i < $#prot_hits; $i += 2) {
            if ($#prot_hits % 2 == 0) {
                #print "Warning: $name has an odd number of hits, skipping\n";
                last;
            }
            my $hit_amp = $prot_hits[$i];
            my $hit_lys = $prot_hits[$i + 1];

            if ( ($hit_amp->name ne 'aa-activating-core.198-334') ||
                 ($hit_lys->name ne 'aroundLys517')){
                 #print "failed $name: amp name = " . $hit_amp->name . "; lys name = " . $hit_lys->name . "\n";
                 last;
            }

            my $amp = _removeGaps($hit_amp->next_hsp);
            my $lys = _removeGaps($hit_lys->next_hsp);

            $idx++;
            push @seqs, [$amp, $lys, "${name}_m$idx"];
        }
    }

    return @seqs;
}

sub _removeGaps {
    my $hsp = shift;
    my @hit_string = split //, $hsp->hit_string;
    my @query_string = split //, $hsp->query_string;

    for (my $i = 0; $i <= $#hit_string; $i++) {
        next unless ($hit_string[$i] eq ".");
        splice(@hit_string, $i, 1);
        splice(@query_string, $i, 1);
        $i -= 1;
    }
    return join("", @query_string);
}

1;
